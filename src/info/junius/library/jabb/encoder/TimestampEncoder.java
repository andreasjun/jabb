// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.encoder;

import java.sql.Timestamp;
import info.junius.library.jabb.dt.Encoder;
import info.junius.library.jabb.util.ArrayConcatenator;

/**
 * timestamp, gets encoded as unix time 8 bytes plus 4 bytes for the nanosecond fraction.
 * Created: 3 Aug 2015, 5:00:02 pm
 * Changed: 8 Mar 2018 - added nano second part
 * 
 * @author Andreas Junius
 */
public class TimestampEncoder implements Encoder {

	private static final ArrayConcatenator	ARRAY_CONCAT	= new ArrayConcatenator();

	/** long value */
	private LongEncoder						longEncoder		= new LongEncoder();

	/** int value */
	private IntEncoder						intEncoder		= new IntEncoder();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] encode ( Object value ) {
		byte[] result = null;
		if (value == null) {
			result = NULL;
		} else {
			// milliseconds
			byte[] milliseconds = this.longEncoder.encode( ((Timestamp)value).getTime());
			// nanoseconds
			byte[] nanoseconds = this.intEncoder.encode( ((Timestamp)value).getNanos());
			result = ARRAY_CONCAT.concatenate(milliseconds, nanoseconds);
		}
		return result;
	}
}