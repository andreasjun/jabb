// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import java.util.ArrayList;
import java.util.List;
import info.junius.library.jabb.ObjectID;

/**
 * Finds object id annotation.
 * Created: 8 Sep 2015, 10:24:38 pm
 * 
 * @author Andreas Junius
 */
public class ObjectIdAnnotationFinder {

	/**
	 * Returns the first ObjectID annotation found in the object hierarchy.
	 * 
	 * @param o target object
	 * @return object id or null
	 */
	public ObjectID get ( Object o ) {
		// get objectIds in hierarchy
		List<ObjectID> ids = this.findAnnotations(o.getClass(), new ArrayList<>());
		// missing if result is empty
		return (!ids.isEmpty()) ? ids.get(0) : null;
	}

	/**
	 * Returns the first ObjectID annotation found in the class hierarchy.
	 * 
	 * @param clazz target class
	 * @return object id or null
	 */
	public ObjectID get ( Class<?> clazz ) {
		// get objectIds in hierarchy
		List<ObjectID> ids = this.findAnnotations(clazz, new ArrayList<>());
		// missing if result is empty
		return (!ids.isEmpty()) ? ids.get(0) : null;
	}

	/**
	 * Finds ObjectID annotation in a hierarchy of classes using recursion.
	 * 
	 * @param clazz target class
	 * @param result result list
	 * @return result list used for input
	 */
	public List<ObjectID> findAnnotations ( Class<?> clazz, List<ObjectID> result ) {
		if (clazz != null) {
			ObjectID objectId = clazz.getAnnotation(ObjectID.class);
			if (objectId != null) {
				result.add(objectId);
			}
			Class<?>[] interfaces = clazz.getInterfaces();
			if (interfaces != null) {
				for (Class<?> i : interfaces) {
					result = this.findAnnotations(i, result);
				}
			}
			Class<?> abstr = clazz.getSuperclass();
			result = this.findAnnotations(abstr, result);
		}
		return result;
	}
}