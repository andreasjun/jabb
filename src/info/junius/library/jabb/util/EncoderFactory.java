// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import java.util.HashMap;
import java.util.Map;
import info.junius.library.jabb.constants.Type;
import info.junius.library.jabb.dt.Encoder;
import info.junius.library.jabb.encoder.BooleanArrayEncoder;
import info.junius.library.jabb.encoder.BooleanEncoder;
import info.junius.library.jabb.encoder.ByteArrayEncoder;
import info.junius.library.jabb.encoder.ByteEncoder;
import info.junius.library.jabb.encoder.CharArrayEncoder;
import info.junius.library.jabb.encoder.CharEncoder;
import info.junius.library.jabb.encoder.DateEncoder;
import info.junius.library.jabb.encoder.DoubleArrayEncoder;
import info.junius.library.jabb.encoder.DoubleEncoder;
import info.junius.library.jabb.encoder.EnumEncoder;
import info.junius.library.jabb.encoder.FloatArrayEncoder;
import info.junius.library.jabb.encoder.FloatEncoder;
import info.junius.library.jabb.encoder.IntArrayEncoder;
import info.junius.library.jabb.encoder.IntEncoder;
import info.junius.library.jabb.encoder.LongArrayEncoder;
import info.junius.library.jabb.encoder.LongEncoder;
import info.junius.library.jabb.encoder.NullEncoder;
import info.junius.library.jabb.encoder.ShortArrayEncoder;
import info.junius.library.jabb.encoder.ShortEncoder;
import info.junius.library.jabb.encoder.StringEncoder;
import info.junius.library.jabb.encoder.TimestampEncoder;

/**
 * Returns a suitable byte encoder for the given type.
 * Created: 3 Aug 2015, 12:05:43 pm
 * 
 * @author Andreas Junius
 */
public class EncoderFactory {

	/** All encoders */
	private static final Map<Type, Encoder> ENCODERS = new HashMap<>();

	/**
	 * Returns suitable encoder for the given type
	 * 
	 * @param value value
	 * @return encoder or null if there is none
	 */
	public Encoder getEncoder ( Object value ) {
		return this.fetchEncoder(Type.getByValue(value));
	}

	/**
	 * Returns an encoder; creates one if not yet done.
	 * 
	 * @param type type
	 * @return encoder or null
	 */
	private Encoder fetchEncoder ( Type type ) {
		Encoder encoder = (Encoder)ENCODERS.get(type);
		if (encoder == null) {
			switch (type) {
				case NULL:
					encoder = new NullEncoder();
					break;
				case BYTE:
					encoder = new ByteEncoder();
					break;
				case SHORT:
					encoder = new ShortEncoder();
					break;
				case INTEGER:
					encoder = new IntEncoder();
					break;
				case LONG:
					encoder = new LongEncoder();
					break;
				case FLOAT:
					encoder = new FloatEncoder();
					break;
				case DOUBLE:
					encoder = new DoubleEncoder();
					break;
				case BOOLEAN:
					encoder = new BooleanEncoder();
					break;
				case CHARACTER:
					encoder = new CharEncoder();
					break;
				case STRING:
					encoder = new StringEncoder();
					break;
				case DATE:
					encoder = new DateEncoder();
					break;
				case TIMESTAMP:
					encoder = new TimestampEncoder();
					break;
				case ENUM:
					encoder = new EnumEncoder();
					break;
				case BYTE_ARRAY:
					encoder = new ByteArrayEncoder();
					break;
				case SHORT_ARRAY:
					encoder = new ShortArrayEncoder();
					break;
				case INT_ARRAY:
					encoder = new IntArrayEncoder();
					break;
				case LONG_ARRAY:
					encoder = new LongArrayEncoder();
					break;
				case FLOAT_ARRAY:
					encoder = new FloatArrayEncoder();
					break;
				case DOUBLE_ARRAY:
					encoder = new DoubleArrayEncoder();
					break;
				case BOOLEAN_ARRAY:
					encoder = new BooleanArrayEncoder();
					break;
				case CHAR_ARRAY:
					encoder = new CharArrayEncoder();
					break;
				default:
					break;
			}
			if (encoder != null) {
				ENCODERS.put(type, encoder);
			}
		}
		return encoder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString () {
		return String.format("EncoderFactory []");
	}
}