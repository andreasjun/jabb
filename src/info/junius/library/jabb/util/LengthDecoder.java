// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import java.io.IOException;
import java.nio.ByteBuffer;
import info.junius.library.jabb.decoder.IntDecoder;
import info.junius.library.jabb.decoder.LongDecoder;
import info.junius.library.jabb.decoder.ShortDecoder;

/**
 * Decodes the length of a value to bytes using DER encoding rules.
 * Created: 1 Aug 2015, 12:52:20 pm
 * 
 * @author Andreas Junius
 */
public class LengthDecoder {

	// utility
	private ArrayConcatenator	arrayConcatenator	= new ArrayConcatenator();

	// decode for short value
	private ShortDecoder		shortDecoder		= new ShortDecoder();

	// decoder for integer value
	private IntDecoder			intDecoder			= new IntDecoder();

	// decoder for long value
	private LongDecoder			longDecoder			= new LongDecoder();

	/**
	 * Reads length from byte array.
	 * 
	 * @param byteBuffer byte array
	 * @return length of value
	 * @throws IOException invalid buffer length
	 */
	public long decode ( ByteBuffer byteBuffer ) throws IOException {
		long result = 0;
		if (byteBuffer != null && byteBuffer.remaining() > 0) {
			byte firstByte = byteBuffer.get();
			if (firstByte >= 0) {
				// first byte denotes length of value
				result = firstByte;
			} else {
				// first byte denotes length of length value
				int length = firstByte - Byte.MIN_VALUE;
				if (byteBuffer.remaining() < Byte.BYTES + length) {
					throw new IOException("Invalid length value, not enough bytes");
				}
				// byte value
				byte[] byteValue = new byte[length];
				// get the bytes
				for (int i = 0; i < length; i++) {
					byteValue[i] = byteBuffer.get();
				}
				if (length <= Short.BYTES) {
					result = this.shortDecoder.decode(this.getStandardByteArray(byteValue));
				} else if (length <= Integer.BYTES) {
					result = this.intDecoder.decode(this.getStandardByteArray(byteValue));
				} else {
					result = this.longDecoder.decode(this.getStandardByteArray(byteValue));
				}
			}
		}
		return result;
	}

	/**
	 * Reads length from byte array.
	 * 
	 * @param rawBytes byte array
	 * @return length of value
	 * @throws IOException invalid array length
	 */
	public long decode ( byte[] rawBytes ) throws IOException {
		long result = 0;
		if (rawBytes != null && rawBytes.length > 0) {
			byte firstByte = rawBytes[0];
			if (firstByte >= 0) {
				// first byte denotes length of value
				result = firstByte;
			} else {
				// first byte denotes length of length value
				int length = firstByte - Byte.MIN_VALUE;
				if (rawBytes.length < Byte.BYTES + length) {
					throw new IOException("Invalid length value, not enough bytes");
				}
				// byte value
				byte[] byteValue = new byte[length];
				// get the bytes
				for (int i = 0; i < length; i++) {
					byteValue[i] = rawBytes[i + 1];
				}
				if (length <= Short.BYTES) {
					result = this.shortDecoder.decode(this.getStandardByteArray(byteValue));
				} else if (length <= Integer.BYTES) {
					result = this.intDecoder.decode(this.getStandardByteArray(byteValue));
				} else {
					result = this.longDecoder.decode(this.getStandardByteArray(byteValue));
				}
			}
		}
		return result;
	}

	/**
	 * Returns the number of bytes that denote the length value
	 * 
	 * @param rawBytes bytes to read
	 * @return number of length value bytes
	 */
	public byte getLengthLength ( byte[] rawBytes ) {
		byte result = 0;
		if (rawBytes != null && rawBytes.length > 0) {
			byte firstByte = rawBytes[0];
			if (firstByte >= 0) {
				// one byte
				result = Byte.BYTES;
			} else {
				// leading byte plus value bytes
				result = (byte) ((byte)Byte.BYTES + (byte) (firstByte - Byte.MIN_VALUE));
			}
		}
		return result;
	}

	/**
	 * Fills the array if it has an unconventional length with leading 0 bytes
	 * 
	 * @param value value to test
	 * @return array of fixed length
	 */
	private byte[] getStandardByteArray ( byte[] value ) {
		byte[] prefix = null;
		byte[] result = value;
		if (value.length < Short.BYTES) {
			// this is a short
			prefix = new byte[Short.BYTES - value.length];
			result = this.arrayConcatenator.concatenate(prefix, value);
		} else if (value.length > Short.BYTES && value.length < Integer.BYTES) {
			// 3 bytes, integer
			prefix = new byte[Integer.BYTES - value.length];
			result = this.arrayConcatenator.concatenate(prefix, value);
		} else if (value.length > Integer.BYTES && value.length < Long.BYTES) {
			// 5 plus bytes, long
			prefix = new byte[Long.BYTES - value.length];
			result = this.arrayConcatenator.concatenate(prefix, value);
		}
		return result;
	}
}