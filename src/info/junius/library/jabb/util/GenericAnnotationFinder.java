// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import info.junius.library.jabb.Generic;

/**
 * The @Generic annotation could be on any method in a class hierarchy. This finder should find it.
 * Created: 22 Feb 2016, 8:08:06 pm
 * 
 * @author Andreas Junius
 */
public class GenericAnnotationFinder {

	/** constants and utilities */
	private final AnnotationReader	annotationReader	= new AnnotationReader();

	/** utility to find the concrete implementation for an interface or abstract class */
	private final InstanceFactory	instanceFactory		= new InstanceFactory();

	/**
	 * Returns the method that matches the given target method and that has a Generic annotation on it. Example:<br>
	 * getFoo() as and argument will return setFoo if setFoo has the Generic annotation<br>
	 * if getFoo() has the annotation it will be returned
	 * 
	 * @param target target method
	 * @return annotated method or null if none could be found
	 * @throws ClassNotFoundException if a concrete implementation of the declaring class can't be found
	 */
	public Method getAnnotatedMethod ( Method target ) throws ClassNotFoundException {
		Method result = null;
		// maybe the annotation is right there
		if (target.getAnnotation(Generic.class) != null) {
			result = target;
		}
		// if not
		if (result == null) {
			// search for other annotated method
			String lookupName = null;
			// fetch all methods on the class that owns the method given as argument
			List<Method> methods = this.annotationReader.findMethods(this.instanceFactory.getImplementationClazz(target.getDeclaringClass()), new ArrayList<>());
			if (methods != null) {
				if (target.getName().startsWith(AnnotationReader.SETTER_PREFIX)) {
					// search getter (is, get)
					for (Method method : methods) {
						// try get
						lookupName = AnnotationReader.GETTER_PREFIX
										+ target.getName().substring(AnnotationReader.GETTER_PREFIX.length());
						if (method.getName().equals(lookupName) && method.getAnnotation(Generic.class) != null) {
							result = method;
							break;
						}
						// try is
						lookupName = AnnotationReader.BOOLEAN_GETTER_PREFIX
										+ target.getName().substring(AnnotationReader.BOOLEAN_GETTER_PREFIX.length());
						if (method.getName().equals(lookupName) && method.getAnnotation(Generic.class) != null) {
							result = method;
							break;
						}
					}
				} else {
					// search setter (set)
					for (Method method : methods) {
						// try set
						lookupName = AnnotationReader.SETTER_PREFIX
										+ target.getName().substring(AnnotationReader.SETTER_PREFIX.length());
						if (method.getName().equals(lookupName) && method.getAnnotation(Generic.class) != null) {
							result = method;
							break;
						}
					}
				}
			}
		}
		return result;
	}
}