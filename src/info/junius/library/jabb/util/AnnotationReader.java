// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import info.junius.library.jabb.MemberID;
import info.junius.library.jabb.validator.MemberIdAnnotationValidator;

/**
 * Searches for annotated methods that use the MemberId annotation.
 * Created: 4 Aug 2015, 3:59:09 pm
 * 
 * @author Andreas Junius
 */
public class AnnotationReader {

	/** set */
	public static final String			SETTER_PREFIX			= "set";

	/** get */
	public static final String			GETTER_PREFIX			= "get";

	/** is */
	public static final String			BOOLEAN_GETTER_PREFIX	= "is";

	/** validates annotation use */
	private MemberIdAnnotationValidator	validator				= new MemberIdAnnotationValidator();

	/**
	 * Get all annotated getters (get, is).
	 * 
	 * @param object annotated object
	 * @return map of annotated getter methods, nerver null
	 */
	public Map<Method, MemberID> getGetter ( Object object ) {
		Map<Method, MemberID> result = new HashMap<>();
		// do a check first
		if (this.validator.isAmbiguous(object)) {
			throw new IllegalArgumentException("MemberID annotation is ambigous. Please ensure that IDs are unique within a class.");
		}
		// fetch all methods
		List<Method> methods = this.findMethods(object.getClass(), new ArrayList<>());
		// find all getters for the member annotation
		if (methods != null) {
			for (Method method : methods) {
				// see if it has a memberId annotation
				MemberID memberId = method.getAnnotation(MemberID.class);
				// if it has one, associate the getter method with it
				if (memberId != null) {
					if (method.getName().startsWith(SETTER_PREFIX)) {
						// found setter, try to find getter
						// throws a runtime exception
						result.put(this.findGetter(object, method), memberId);
					} else if (method.getName().startsWith(GETTER_PREFIX)) {
						// plain getter
						result.put(method, memberId);
					} else if (method.getName().startsWith(BOOLEAN_GETTER_PREFIX)) {
						// boolean getter
						result.put(method, memberId);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Get all annotated setters.
	 * 
	 * @param object annotated object
	 * @return map of annotated setter methods, never null
	 */
	public Map<Method, MemberID> getSetter ( Object object ) {
		Map<Method, MemberID> result = new HashMap<>();
		// do a check first
		if (this.validator.isAmbiguous(object)) {
			throw new IllegalArgumentException("MemberID annotation is ambigous. Please ensure that IDs are unique within a class.");
		}
		// fetch all methods
		List<Method> methods = this.findMethods(object.getClass(), new ArrayList<>());
		// find all getters for the member annotation
		if (methods != null) {
			for (Method method : methods) {
				// see if it has a memberId annotation
				MemberID memberId = method.getAnnotation(MemberID.class);
				// if it has one, associate the getter method with it
				if (memberId != null) {
					if (method.getName().startsWith(GETTER_PREFIX)
						|| method.getName().startsWith(BOOLEAN_GETTER_PREFIX)) {
						// found getter, try to find setter
						result.put(this.findSetter(object, method), memberId);
					} else if (method.getName().startsWith(SETTER_PREFIX)) {
						// plain setter
						result.put(method, memberId);
					}
				}
			}
		}
		return result;
	}

	/**
	 * Finding all methods in a class hierarchy using recursion
	 * 
	 * @param clazz target class
	 * @param result result list
	 * @return result list
	 */
	public List<Method> findMethods ( Class<?> clazz, List<Method> result ) {
		if (clazz != null) {
			Method[] methods = clazz.getMethods();
			for (Method method : methods) {
				result.add(method);
			}
			Class<?>[] interfaces = clazz.getInterfaces();
			if (interfaces != null) {
				for (Class<?> i : interfaces) {
					result = this.findMethods(i, result);
				}
			}
			Class<?> abstr = clazz.getSuperclass();
			result = this.findMethods(abstr, result);
		}
		return result;
	}

	/**
	 * Tries to find the getter to a given setter, i.e. a method with the same name that starts with is or get.
	 * 
	 * @param object
	 * @param setter
	 * @return getter method
	 */
	private Method findGetter ( Object object, Method setter ) {
		Method getter = null;
		List<String> getterMethodNames = new ArrayList<>();
		Class<?>[] parameterTypes = setter.getParameterTypes();
		if (parameterTypes.length != 1) {
			throw new IllegalArgumentException("Object has annotated setter without argument");
		}
		Class<?> parameterClazz = parameterTypes[0];
		// getter may start with "is"
		if (Boolean.TYPE.equals(parameterClazz) || Boolean.class.equals(parameterClazz)) {
			getterMethodNames.add(BOOLEAN_GETTER_PREFIX + setter.getName().substring(SETTER_PREFIX.length()));
		}
		// or "get"
		getterMethodNames.add(GETTER_PREFIX + setter.getName().substring(SETTER_PREFIX.length()));
		// try the names
		for (String name : getterMethodNames) {
			try {
				getter = object.getClass().getMethod(name);
				// done
				break;
			} catch (NoSuchMethodException | SecurityException e) {
				// nothing to do here
			}
		}
		// test the result
		if (getter == null) {
			// probably an object that doesn't obey the rules for EJB
			throw new IllegalArgumentException("Unable to find getter for " + setter);
		}
		return getter;
	}

	/**
	 * Tries to find the setter for the given getter, i.e. a method that takes a parameter matching the return type of
	 * the getter and a name starting with set
	 * 
	 * @param object
	 * @param getter
	 * @return setter method
	 */
	private Method findSetter ( Object object, Method getter ) {
		Method setter = null;
		String expectedSetterName = null;
		// get the setter name
		if (getter.getName().startsWith(BOOLEAN_GETTER_PREFIX)) {
			expectedSetterName = SETTER_PREFIX + getter.getName().substring(BOOLEAN_GETTER_PREFIX.length());
		} else {
			expectedSetterName = SETTER_PREFIX + getter.getName().substring(GETTER_PREFIX.length());
		}
		// get the parameter type
		Class<?> parameterType = getter.getReturnType();
		try {
			setter = object.getClass().getMethod(expectedSetterName, parameterType);
		} catch (NoSuchMethodException | SecurityException e) {
			// nothing to do here
		}
		// test the result
		if (setter == null) {
			// probably an object that doesn't obey the rules for EJB
			throw new IllegalArgumentException("Unable to find setter for " + getter);
		}
		return setter;
	}
}