// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import java.io.IOException;
import info.junius.library.jabb.encoder.IntEncoder;
import info.junius.library.jabb.encoder.LongEncoder;
import info.junius.library.jabb.encoder.ShortEncoder;

/**
 * Encodes the length of a value to bytes using DER encoding rules.
 * Created: 1 Aug 2015, 11:16:01 am
 * 
 * @author Andreas Junius
 */
public class LengthEncoder {

	// utility
	private ArrayConcatenator	arrayConcatenator	= new ArrayConcatenator();

	// encode for short value
	private ShortEncoder		shortEncoder		= new ShortEncoder();

	// encoder for integer value
	private IntEncoder			intEncoder			= new IntEncoder();

	// encoder for long value
	private LongEncoder			longEncoder			= new LongEncoder();

	/**
	 * Returns the length of the given value as a byte array of variable length.
	 * 
	 * @param value value
	 * @return byte array holding the length of the value. The length will be 0 if the value is null
	 */
	public byte[] encode ( byte[] value ) {
		long length = (value != null) ? (long)value.length : 0L;
		byte[] result = null;
		try {
			result = this.encode(length);
		} catch (IOException e) {
			throw new IllegalArgumentException("Unable to process value");
		}
		return result;
	}

	/**
	 * Encodes the given length value to bytes following the DER encoding rules for the length.
	 * 
	 * @param length length to encode
	 * @return byte array of variable length
	 * @throws IOException if length less than 0
	 */
	public byte[] encode ( long length ) throws IOException {
		byte[] result = null;
		if (length < 0) {
			throw new IOException("Illegal length value");
		}
		// create array of bytes expressing the length
		if (length <= Byte.MAX_VALUE) {
			// single byte sufficient
			result = new byte[] { (byte)length };
		} else if (length <= Short.MAX_VALUE) {
			// 2 bytes plus length byte
			result = arrayConcatenator.concatenate(new byte[] { Byte.MIN_VALUE
																+ Short.BYTES }, this.shortEncoder.encode((short)length));
		} else if (length <= Integer.MAX_VALUE) {
			// 4 bytes plus length byte
			result = arrayConcatenator.concatenate(new byte[] { Byte.MIN_VALUE
																+ Integer.BYTES }, this.intEncoder.encode((int)length));
		} else {
			// 8 bytes plus length byte
			result = arrayConcatenator.concatenate(new byte[] { Byte.MIN_VALUE
																+ Long.BYTES }, this.longEncoder.encode(length));
		}
		return result;
	}
}