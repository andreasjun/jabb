// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

/**
 * Array utility class that concatenates arrays.
 * Created: 28 Oct 2014, 11:26:16 am
 * 
 * @author Andreas Junius
 */
public class ArrayConcatenator {

	/**
	 * Concatenates the two given arrays. This method copes with null values by substituting null with an empty array.
	 * 
	 * @param a first array, may be null
	 * @param b second array, may be null
	 * @return concatenation of both arrays a, b
	 */
	public byte[] concatenate ( byte[] a, byte[] b ) {
		// validate input
		byte[] first = (a != null) ? a : new byte[0];
		byte[] second = (b != null) ? b : new byte[0];
		byte[] result = new byte[first.length + second.length];
		// copy the elements from the first array
		for (int i = 0; i < first.length; i++) {
			result[i] = first[i];
		}
		// copy all elements from the second array, considering offset for first array
		int offset = first.length;
		for (int i = 0; i < second.length; i++) {
			result[offset + i] = second[i];
		}
		return result;
	}

	/**
	 * Converts a to an array and concatenates both arrays.
	 * 
	 * @param a byte value
	 * @param b array, may be null
	 * @return concatenation of both arrays a, b
	 */
	public byte[] concatenate ( byte a, byte[] b ) {
		return this.concatenate(new byte[] { a }, b);
	}

	/**
	 * Converts b to an array and concatenates both arrays.
	 * 
	 * @param a array, may be null
	 * @param b byte value
	 * @return concatenation of both arrays a, b
	 */
	public byte[] concatenate ( byte a[], byte b ) {
		return this.concatenate(a, new byte[] { b });
	}

	/**
	 * Puts both values into an array
	 * 
	 * @param a first byte value
	 * @param b second byte value
	 * @return byte array
	 */
	public byte[] concatenate ( byte a, byte b ) {
		return new byte[] { a, b };
	}
}