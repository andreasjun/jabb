// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

/**
 * Utility class that tries to find a class by name, both simple name (in case it resides in either java.lang or
 * java.util) and a canonical name.
 * Created: 31 Oct 2015, 12:24:14 pm
 * 
 * @author Andreas Junius
 */
public class ClassFinder {

	/**
	 * Tries to get a class for a name
	 * 
	 * @param className simple name or canonical name
	 * @return class
	 * @throws ClassNotFoundException class not found or class name ambiguous
	 */
	public Class<?> getClassByName ( String className ) throws ClassNotFoundException {
		Class<?> clazz = null;
		// works in case this is a fully qualified name
		try {
			clazz = Class.forName(className);
		} catch (ClassNotFoundException e) {
			// nothing to do in this case
		}
		// simple name, may be in java.lang
		if (clazz == null) {
			try {
				clazz = Class.forName("java.lang." + className);
			} catch (ClassNotFoundException e) {
				// nothing to do in this case either
			}
		}
		// it's obviously not in java.lang, but may be in java.util
		if (clazz == null) {
			try {
				clazz = Class.forName("java.util." + className);
			} catch (ClassNotFoundException e) {
				// nothing to do in this case either
			}
		} else {
			// not very likely but it might happen at one time - a class exists in both packages
			Class<?> clazz2 = null;
			try {
				clazz2 = Class.forName("java.util." + className);
			} catch (ClassNotFoundException e) {
				// nothing to do here, didn't find a duplicate
			}
			// in case we found a duplicate
			if (clazz2 != null) {
				throw new ClassNotFoundException("Ambiguous class name " + className);
			}
		}
		// final check
		if (clazz == null) {
			throw new ClassNotFoundException("Class not found " + className);
		}
		// never return null
		return clazz;
	}
}