// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import info.junius.library.jabb.ObjectID;
import info.junius.library.jabb.dt.ObjectCreationException;
import info.junius.library.jabb.generics.GenericType;

/**
 * Utiltity class that tries to find an implementation to an interface or an abstract class and returns an instance of
 * the implementation class.
 * Created: 31 Oct 2015, 11:47:07 am
 * 
 * @author Andreas Junius
 */
public class InstanceFactory {

	/** utility */
	private static final ObjectIdAnnotationFinder	ANNOTATIONFINDER	= new ObjectIdAnnotationFinder();

	/** utility */
	private static final ClassFinder				CLASSFINDER			= new ClassFinder();

	/** known interfaces */
	private static final Map<Class<?>, Class<?>>	IMPLEMENTATIONS		= new HashMap<>();
	{
		// map types
		IMPLEMENTATIONS.put(SortedMap.class, TreeMap.class);
		IMPLEMENTATIONS.put(Map.class, HashMap.class);
		// set types
		IMPLEMENTATIONS.put(SortedSet.class, TreeSet.class);
		IMPLEMENTATIONS.put(Set.class, HashSet.class);
		// list types
		IMPLEMENTATIONS.put(List.class, ArrayList.class);
		IMPLEMENTATIONS.put(Collection.class, ArrayList.class);
		// queue types
		IMPLEMENTATIONS.put(Queue.class, LinkedList.class);
		// deque types
		IMPLEMENTATIONS.put(Deque.class, LinkedList.class);
	}

	/**
	 * Returns an instance of type T
	 * 
	 * @param type
	 *            type information
	 * @return instance
	 * @throws ObjectCreationException
	 *             unable to create instance
	 */
	public Object getInstance ( GenericType type ) throws ObjectCreationException {
		return this.getInstance(type.getTypeClass());
	}

	/**
	 * Tries to find the concrete implementation of a class (interface, abstract class, etc.).
	 * 
	 * @param clazz target class
	 * @return implementation
	 * @throws ClassNotFoundException if the actual implementation can't be found
	 */
	public Class<?> getImplementationClazz ( Class<?> clazz ) throws ClassNotFoundException {
		Class<?> result = null;
		// it's either and interface (or abstract class) or a concrete implementation
		if (clazz.isInterface() || Modifier.isAbstract(clazz.getModifiers())) {
			// check for maps or collections
			Class<?> composite = this.getImplementationOfCollectionOrMap(clazz);
			if (composite != null) {
				result = composite;
			} else {
				// it's not a map nor a collection
				// try to find annotation on the input class hierarchy...
				ObjectID objectId = ANNOTATIONFINDER.get(clazz);
				if (objectId != null && !objectId.className().isEmpty()) {
					// find implementation and create instance of it
					result = CLASSFINDER.getClassByName(objectId.className());
				}
			}
		} else {
			// already an implementation
			result = clazz;
		}
		// final check
		if (result == null) {
			throw new ClassNotFoundException("Unable to find implementation for type " + clazz);
		}
		return result;
	}

	/**
	 * Returns an instance of type T
	 * 
	 * @param clazz class, abstract class or interface
	 * @return instance
	 * @throws ObjectCreationException
	 *             if unable to instantiate a class or if there is no implementation class
	 */
	public Object getInstance ( Class<?> clazz ) throws ObjectCreationException {
		Object result = null;
		// it's either and interface (or abstract class) or a concrete implementation
		if (Enum.class.equals(clazz)) {
			throw new ObjectCreationException("Unable to instantiate enum without value. Use Enum.valueOf(...) instead");
		} else {
			try {
				Class<?> implementation = this.getImplementationClazz(clazz);
				result = implementation.newInstance();
			} catch (ClassNotFoundException e) {
				throw new ObjectCreationException("Unable to find implementation for type " + clazz, e);
			} catch (InstantiationException | IllegalAccessException e) {
				throw new ObjectCreationException("Unable to instantiate implementation for type " + clazz, e);
			}
		}
		return result;
	}

	/**
	 * Creates an instance of the given class using the string argument
	 * 
	 * @param clazz target class
	 * @param arg constructor argument
	 * @return instance
	 * @throws ObjectCreationException unable to create an instance of the given type
	 */
	public Object getInstance ( Class<?> clazz, String arg ) throws ObjectCreationException {
		Object result = null;
		try {
			Constructor<?> constructor = clazz.getConstructor(String.class);
			result = constructor.newInstance(new Object[] { arg });
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
						| IllegalArgumentException | InvocationTargetException e) {
			throw new ObjectCreationException("Unable to create object from string value. Missing String constructor", e);
		}
		return result;
	}

	/**
	 * Tests if the given class has a string argument constructor
	 * 
	 * @param clazz class to test
	 * @return true if instances of that class can be created using a string argument for its constructor
	 */
	public boolean hasStringArgConstructor ( Class<?> clazz ) {
		boolean result = false;
		try {
			clazz.getConstructor(String.class);
			result = true;
		} catch (NoSuchMethodException | SecurityException e) {
			result = false;
		}
		return result;
	}

	/**
	 * Tries to find the most suitable implementation for the given interface or abstract class of a Map or Collection
	 * 
	 * @param clazz
	 *            interface or abstract class
	 * @return implementation as defined in the map above or null
	 */
	private Class<?> getImplementationOfCollectionOrMap ( Class<?> clazz ) {
		Class<?> result = null;
		// sub interfaces tested first
		if (SortedMap.class.isAssignableFrom(clazz)) {
			result = IMPLEMENTATIONS.get(clazz);
		} else if (Map.class.isAssignableFrom(clazz)) {
			result = IMPLEMENTATIONS.get(clazz);
		} else if (SortedSet.class.isAssignableFrom(clazz)) {
			result = IMPLEMENTATIONS.get(clazz);
		} else if (Set.class.isAssignableFrom(clazz)) {
			result = IMPLEMENTATIONS.get(clazz);
		} else if (List.class.isAssignableFrom(clazz)) {
			result = IMPLEMENTATIONS.get(clazz);
		} else if (Collection.class.isAssignableFrom(clazz)) {
			result = IMPLEMENTATIONS.get(clazz);
		} else if (Deque.class.isAssignableFrom(clazz)) {
			result = IMPLEMENTATIONS.get(clazz);
		} else if (Queue.class.isAssignableFrom(clazz)) {
			result = IMPLEMENTATIONS.get(clazz);
		}
		return result;
	}
}