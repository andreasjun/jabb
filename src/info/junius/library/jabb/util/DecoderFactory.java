// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import java.util.HashMap;
import java.util.Map;
import info.junius.library.jabb.constants.Type;
import info.junius.library.jabb.decoder.BooleanArrayDecoder;
import info.junius.library.jabb.decoder.BooleanDecoder;
import info.junius.library.jabb.decoder.ByteArrayDecoder;
import info.junius.library.jabb.decoder.ByteDecoder;
import info.junius.library.jabb.decoder.CharArrayDecoder;
import info.junius.library.jabb.decoder.CharDecoder;
import info.junius.library.jabb.decoder.DateDecoder;
import info.junius.library.jabb.decoder.DoubleArrayDecoder;
import info.junius.library.jabb.decoder.DoubleDecoder;
import info.junius.library.jabb.decoder.FloatArrayDecoder;
import info.junius.library.jabb.decoder.FloatDecoder;
import info.junius.library.jabb.decoder.IntArrayDecoder;
import info.junius.library.jabb.decoder.IntDecoder;
import info.junius.library.jabb.decoder.LongArrayDecoder;
import info.junius.library.jabb.decoder.LongDecoder;
import info.junius.library.jabb.decoder.NullDecoder;
import info.junius.library.jabb.decoder.ShortArrayDecoder;
import info.junius.library.jabb.decoder.ShortDecoder;
import info.junius.library.jabb.decoder.StringDecoder;
import info.junius.library.jabb.decoder.TimestampDecoder;
import info.junius.library.jabb.dt.Decoder;

/**
 * Returns a suitable byte decoder for the given type.
 * Created: 3 Aug 2015, 12:05:43 pm
 * 
 * @author Andreas Junius
 */
public class DecoderFactory {

	/** All decoders */
	private static final Map<Type, Decoder<?>> DECODERS = new HashMap<>();

	/**
	 * Returns decoder for Type Type
	 * 
	 * @param <T> type of the desired decoder
	 * @param type type
	 * @return decoder or null if there is none for the given type
	 */
	public <T> Decoder<T> getDecoder ( Type type ) {
		return this.fetchDecoder(type);
	}

	/**
	 * Returns a decoder; creates one if not yet done.
	 * 
	 * @param type type
	 * @return decoder or null
	 */
	@SuppressWarnings ( "unchecked" )
	private <T> Decoder<T> fetchDecoder ( Type type ) {
		Decoder<T> decoder = (Decoder<T>)DECODERS.get(type);
		if (decoder == null) {
			switch (type) {
				case NULL:
					decoder = (Decoder<T>)new NullDecoder();
					break;
				case BYTE:
					decoder = (Decoder<T>)new ByteDecoder();
					break;
				case SHORT:
					decoder = (Decoder<T>)new ShortDecoder();
					break;
				case INTEGER:
					decoder = (Decoder<T>)new IntDecoder();
					break;
				case LONG:
					decoder = (Decoder<T>)new LongDecoder();
					break;
				case FLOAT:
					decoder = (Decoder<T>)new FloatDecoder();
					break;
				case DOUBLE:
					decoder = (Decoder<T>)new DoubleDecoder();
					break;
				case BOOLEAN:
					decoder = (Decoder<T>)new BooleanDecoder();
					break;
				case CHARACTER:
					decoder = (Decoder<T>)new CharDecoder();
					break;
				case STRING:
				case ENUM: // enum uses string decoder
					decoder = (Decoder<T>)new StringDecoder();
					break;
				case DATE:
					decoder = (Decoder<T>)new DateDecoder();
					break;
				case TIMESTAMP:
					decoder = (Decoder<T>)new TimestampDecoder();
					break;
				case BYTE_ARRAY:
					decoder = (Decoder<T>)new ByteArrayDecoder();
					break;
				case SHORT_ARRAY:
					decoder = (Decoder<T>)new ShortArrayDecoder();
					break;
				case INT_ARRAY:
					decoder = (Decoder<T>)new IntArrayDecoder();
					break;
				case LONG_ARRAY:
					decoder = (Decoder<T>)new LongArrayDecoder();
					break;
				case FLOAT_ARRAY:
					decoder = (Decoder<T>)new FloatArrayDecoder();
					break;
				case DOUBLE_ARRAY:
					decoder = (Decoder<T>)new DoubleArrayDecoder();
					break;
				case BOOLEAN_ARRAY:
					decoder = (Decoder<T>)new BooleanArrayDecoder();
					break;
				case CHAR_ARRAY:
					decoder = (Decoder<T>)new CharArrayDecoder();
					break;
				default:
					break;
			}
			if (decoder != null) {
				DECODERS.put(type, decoder);
			}
		}
		return decoder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString () {
		return String.format("DecoderFactory []");
	}
}