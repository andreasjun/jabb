// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.constants;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Maps data type and binary flag for this type.
 * Created: 29 Jul 2015, 1:36:14 pm
 * 
 * @author Andreas Junius
 */
public enum Type {
					/** null value */
					NULL ( null, (byte)0 ),
					/** byte */
					BYTE ( Byte.class, (byte)1 ),
					/** Short */
					SHORT ( Short.class, (byte)2 ),
					/** Integer */
					INTEGER ( Integer.class, (byte)3 ),
					/** Long */
					LONG ( Long.class, (byte)4 ),
					/** Float */
					FLOAT ( Float.class, (byte)5 ),
					/** Double */
					DOUBLE ( Double.class, (byte)6 ),
					/** Boolean */
					BOOLEAN ( Boolean.class, (byte)7 ),
					/** Character */
					CHARACTER ( Character.class, (byte)8 ),
					/** any object */
					OBJECT ( Object.class, (byte)9 ),
					/** string */
					STRING ( String.class, (byte)10 ),
					/** date */
					DATE ( Date.class, (byte)11 ),
					/** timestamp */
					TIMESTAMP ( Timestamp.class, (byte)12 ),
					/** enum */
					ENUM ( Enum.class, (byte)14 ),
					/** primitive byte array */
					BYTE_ARRAY ( Byte.TYPE, (byte)21 ),
					/** primitive short array */
					SHORT_ARRAY ( Short.TYPE, (byte)22 ),
					/** primitive int array */
					INT_ARRAY ( Integer.TYPE, (byte)23 ),
					/** primitive long array */
					LONG_ARRAY ( Long.TYPE, (byte)24 ),
					/** primitive float array */
					FLOAT_ARRAY ( Float.TYPE, (byte)25 ),
					/** primitive double array */
					DOUBLE_ARRAY ( Double.TYPE, (byte)26 ),
					/** primitive boolean array */
					BOOLEAN_ARRAY ( Boolean.TYPE, (byte)27 ),
					/** primitive char array */
					CHAR_ARRAY ( Character.TYPE, (byte)28 ),
					/** collection, like list or set */
					COLLECTION ( Collection.class, (byte)30 ),
					/** (any) object array */
					ARRAY ( Object[].class, (byte)40 ),
					/** a map holding key-value pairs */
					MAP ( Map.class, (byte)50 );

	private static final Type[]	ARRAYS				= new Type[] {	BYTE_ARRAY, SHORT_ARRAY, INT_ARRAY, LONG_ARRAY,
																	FLOAT_ARRAY, DOUBLE_ARRAY, BOOLEAN_ARRAY,
																	CHAR_ARRAY };

	private static final Type[]	PRIMITIVES			= new Type[] {	BYTE, SHORT, INTEGER, LONG, FLOAT, DOUBLE, BOOLEAN,
																	CHARACTER };

	private static final Type[]	OBJECTS				= new Type[] { TIMESTAMP, DATE, STRING, OBJECT };

	private static final String	WRAPPER_FIELD_TYPE	= "TYPE";

	/** class */
	private final Class<?>		clazz;

	/** id */
	private final byte			flag;

	/**
	 * Constructor
	 * 
	 * @param clazz
	 * @param flag
	 */
	private Type( Class<?> clazz, byte flag ) {
		this.clazz = clazz;
		this.flag = flag;
	}

	/**
	 * Returns the class associated with this type
	 * 
	 * @return the clazz, null in case of Type NULL
	 */
	public Class<?> getClazz () {
		return this.clazz;
	}

	/**
	 * Byte representation of this type
	 * 
	 * @return the value
	 */
	public byte getFlag () {
		return this.flag;
	}

	/**
	 * Returns type matching the bit pattern or null if not found.
	 * 
	 * @param value bit pattern
	 * @return type or null if not found
	 */
	public static Type getByFlag ( byte value ) {
		Type result = null;
		for (Type type : Type.values()) {
			if (type.getFlag() == value) {
				result = type;
				break;
			}
		}
		return result;
	}

	/**
	 * Returns type based on class
	 * 
	 * @param value a class, maybe a subclass or a concrete class
	 * @return type, NULL if not found
	 */
	public static Type getByClazz ( Class<?> value ) {
		Type type = Type.NULL;
		if (value != null) {
			if (Map.class.isAssignableFrom(value)) {
				type = Type.MAP;
			} else if (Collection.class.isAssignableFrom(value)) {
				type = Type.COLLECTION;
			} else if (value.isArray() && !value.getComponentType().isPrimitive()) {
				type = Type.ARRAY;
			} else if (Enum.class.equals(value) || value.isEnum()) {
				// enum values
				type = Type.ENUM;
			} else if (value.isPrimitive()) {
				// primitive values
				for (Type t : PRIMITIVES) {
					try {
						// the enum holds the wrapper types for its primitives
						if (t.getClazz().getField(WRAPPER_FIELD_TYPE).get(null).equals(value)) {
							type = t;
							break;
						}
					} catch (NoSuchFieldException | SecurityException | IllegalArgumentException
									| IllegalAccessException e) {
						throw new RuntimeException("Unable to get primitive type from wrapper type", e);
					}
				}
			} else if (value.isArray() && value.getComponentType().isPrimitive()) {
				// arrays of primitive values
				for (Type t : ARRAYS) {
					// the enum holds the wrapper types for its primitives
					if (t.getClazz().equals(value.getComponentType())) {
						type = t;
						break;
					}
				}
			} else {
				// primitive wrappers
				for (Type t : PRIMITIVES) {
					if (t.getClazz().equals(value)) {
						type = t;
						break;
					}
				}
				// all other objects if none has been found yet
				if (Type.NULL.equals(type)) {
					for (Type t : OBJECTS) {
						if (t.getClazz().isAssignableFrom(value)) {
							type = t;
							break;
						}
					}
				}
			}
		}
		return type;
	}

	/**
	 * Returns type matching the type of the given value.
	 * 
	 * @param value the value to examine
	 * @return type, never null
	 */
	public static Type getByValue ( Object value ) {
		// treat everything unknown as an object
		Type result = OBJECT;
		if (value == null) {
			// special case null
			result = NULL;
		} else {
			// get the class
			Class<?> clazz = value.getClass();
			// see if it is an array of primitive values
			if (clazz.isArray() && clazz.getComponentType().isPrimitive()) {
				// get component type
				Class<?> componentType = clazz.getComponentType();
				// find type
				for (Type type : Type.values()) {
					if (type.clazz != null && type.clazz.equals(componentType)) {
						result = type;
						break;
					}
				}
			} else if (clazz.isArray() && !clazz.getComponentType().isPrimitive()) {
				// an array of objects
				result = ARRAY;
			} else if (value instanceof Collection) {
				// this is a collection
				result = COLLECTION;
			} else if (value instanceof Map) {
				// a map
				result = MAP;
			} else if (clazz.isEnum()) {
				result = ENUM;
			} else {
				// anything else
				for (Type type : Type.values()) {
					if (type.clazz != null && type.clazz.equals(clazz)) {
						result = type;
						break;
					}
				}
			}
		}
		return result;
	}
}