// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.decoder;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import info.junius.library.jabb.dt.Decoder;

/**
 * Timestamp, gets decoded from unix time 8 bytes plus 4 bytes for the nanosecond fraction.
 * Created: 3 Aug 2015, 5:01:06 pm
 * Changed: 8 Mar 2018 - added nano second part
 * 
 * @author Andreas Junius
 */
public class TimestampDecoder implements Decoder<Date> {

	/** long value decoder */
	private LongDecoder	longDecoder	= new LongDecoder();

	/** integer value decoder */
	private IntDecoder	intDecoder	= new IntDecoder();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Timestamp decode ( byte[] value ) {
		Timestamp result = null;
		if (value != null && value.length >= (Long.BYTES + Integer.BYTES)) {
			long milliseconds = this.longDecoder.decode(value); // long decoder reads only 8 bytes
			int nanoseconds = this.intDecoder.decode(Arrays.copyOfRange(value, 8, 12)); // 4 bytes for the integer nanos
			result = new Timestamp(milliseconds);
			result.setNanos(nanoseconds);
		}
		return result;
	}
}