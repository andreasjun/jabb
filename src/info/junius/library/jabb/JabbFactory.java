// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Creates decoder and encoder.
 * Created: 16 Feb 2016, 8:35:26 pm
 * 
 * @author Andreas Junius
 */
public class JabbFactory {

	private static final Logger	LOGGER	= Logger.getLogger(JabbFactory.class.getCanonicalName());

	private final ObjectEncoder	encoder;

	private final ObjectDecoder	decoder;

	/**
	 * Default constructor, initialises factory with default implementations. Requires default implementation on the
	 * classpath.
	 */
	public JabbFactory() {
		this("info.junius.library.jabb.impl.DefaultObjectEncoder", "info.junius.library.jabb.impl.DefaultObjectDecoder");
	}

	/**
	 * Constructor, initialises factory with the given implementation classes.
	 * 
	 * @param encoder encoder implementation
	 * @param decoder decoder implementation
	 */
	public JabbFactory( String encoder, String decoder ) {
		super();
		ObjectEncoder tmpEncoder = null;
		ObjectDecoder tmpDecoder = null;
		try {
			tmpEncoder = (ObjectEncoder)Class.forName(encoder).getConstructor().newInstance();
			tmpDecoder = (ObjectDecoder)Class.forName(decoder).getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			LOGGER.log(Level.WARNING, "Unable to create instances from the implementation classes provided", e);
		} finally {
			this.encoder = tmpEncoder;
			this.decoder = tmpDecoder;
		}
	}

	/**
	 * Returns a JABB encoder
	 * 
	 * @return the encoder
	 */
	public ObjectEncoder getEncoder () {
		return this.encoder;
	}

	/**
	 * Returns a JABB decoder
	 * 
	 * @return the decoder
	 */
	public ObjectDecoder getDecoder () {
		return this.decoder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString () {
		return String.format("JabbFactory [encoder=%s, decoder=%s]", this.encoder, this.decoder);
	}
}