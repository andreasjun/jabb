// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.validator;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import info.junius.library.jabb.MemberID;
import info.junius.library.jabb.ObjectID;
import info.junius.library.jabb.util.AnnotationReader;
import info.junius.library.jabb.util.ObjectIdAnnotationFinder;

/**
 * Validates JABB object annotations.
 * Created: 17 Oct 2015, 1:13:42 pm
 * 
 * @author Andreas Junius
 */
public class JabbAnnotationValidator {

	/** annotation validator */
	protected static final ObjectIdAnnotationValidator	OBJECTVALIDATOR		= new ObjectIdAnnotationValidator();

	/** annotation validator */
	protected static final MemberIdAnnotationValidator	MEMBERVALIDATOR		= new MemberIdAnnotationValidator();

	/** reads annotated methods */
	private static final AnnotationReader				ANNOTATIONREADER	= new AnnotationReader();

	/** utility */
	private static final ObjectIdAnnotationFinder		ANNOTATIONFINDER	= new ObjectIdAnnotationFinder();

	/**
	 * Checks if the given class is a properly annotated JABB type
	 * 
	 * @param target target class
	 * @return true if the object is a properly annotated JABB type, false otherwise
	 * @throws IllegalArgumentException if type can't get created through reflection
	 */
	public boolean isValidJabbType ( Class<?> target ) {
		boolean validJabbObject = false;
		try {
			// holds implementation class
			Class<?> clazz = null;
			// check type of class
			if (target.isInterface() || Modifier.isAbstract(target.getModifiers())) {
				// find and read annotation
				ObjectID objectId = ANNOTATIONFINDER.get(target);
				// if unable to find an implementation class for the target, the target can't be a valid JABB class
				if (objectId != null && objectId.className() != null && !objectId.className().isEmpty()) {
					clazz = Class.forName(objectId.className());
				}
			} else {
				// target is an implementation class
				clazz = target;
			}
			// create an instance now
			if (clazz != null) {
				// do the final test
				validJabbObject = this.isValidJabbType(clazz.newInstance());
			}
		} catch (InstantiationException | IllegalAccessException e) {
			throw new IllegalArgumentException("Unable to create target object. Ensure it has a parameterless default constructor", e);
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("Unable to create target object. Can't find class.", e);
		}
		return validJabbObject;
	}

	/**
	 * Checks if the given object is a properly annotated JABB type
	 * 
	 * @param object target object
	 * @return true if the object is a properly annotated JABB type, false otherwise
	 */
	public boolean isValidJabbType ( Object object ) {
		// object id not missing, nor ambiguous and object has member id annotation(s) that are not ambiguous
		return !OBJECTVALIDATOR.isMissing(object)
				&& !OBJECTVALIDATOR.isAmbiguous(object)
				&& this.hasAnnotatedMethods(object)
				&& !MEMBERVALIDATOR.isAmbiguous(object);
	}

	/**
	 * Iterates the object to check if one of its methods has a memberId annotation.
	 * 
	 * @param object target object
	 * @return true if at least one method has been annotated
	 */
	private boolean hasAnnotatedMethods ( Object object ) {
		boolean result = false;
		// fetch all methods
		List<Method> methods = ANNOTATIONREADER.findMethods(object.getClass(), new ArrayList<>());
		// check if one of the methods is annotated
		if (methods != null) {
			for (Method method : methods) {
				// see if it has a memberId annotation
				if (method.getAnnotation(MemberID.class) != null) {
					result = true;
					break;
				}
			}
		}
		return result;
	}
}