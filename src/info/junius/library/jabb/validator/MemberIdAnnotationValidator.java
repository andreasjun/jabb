// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.validator;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import info.junius.library.jabb.MemberID;

/**
 * Validates MemberID annotation use.
 * Created: 2 Aug 2015, 10:54:48 am
 * 
 * @author Andreas Junius
 */
public class MemberIdAnnotationValidator {

	/**
	 * The memberID annotation has to be unique within a class.
	 * 
	 * @param o object to analyse
	 * @return true if ambigous, i.e. there are multiple member id annotations using the same ID
	 */
	public boolean isAmbiguous ( Object o ) {
		boolean result = false;
		List<MemberID> memberAnnotations = new ArrayList<>();
		Method[] methods = o.getClass().getMethods();
		// find all getters for the member annotation
		for (Method method : methods) {
			MemberID memberId = method.getAnnotation(MemberID.class);
			if (memberId != null) {
				memberAnnotations.add(memberId);
			}
		}
		// do the check
		for (MemberID memberId : memberAnnotations) {
			// check for every annotation if there is another one having the same value
			for (MemberID memberId2 : memberAnnotations) {
				// not the same object but the same value
				if (memberId != memberId2 && memberId.value() == memberId2.value()) {
					// found one
					result = true;
					break;
				}
			}
		}
		return result;
	}
}