// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.validator;

import java.util.ArrayList;
import java.util.List;
import info.junius.library.jabb.ObjectID;
import info.junius.library.jabb.util.ObjectIdAnnotationFinder;

/**
 * Checks if the given object is annotated with an object ID.
 * Created: 22 Aug 2015, 11:25:30 am
 * 
 * @author Andreas Junius
 */
public class ObjectIdAnnotationValidator {

	/** utility */
	private ObjectIdAnnotationFinder annotationFinder = new ObjectIdAnnotationFinder();

	/**
	 * Returns true if the given, annotated object is not annotated with an ObjectID annotation.
	 * 
	 * @param o object to test
	 * @return true if annotation is missing
	 */
	public boolean isMissing ( Object o ) {
		// get objectIds in hierarchy
		List<ObjectID> ids = this.annotationFinder.findAnnotations(o.getClass(), new ArrayList<>());
		// missing if result is empty
		return ids.isEmpty();
	}

	/**
	 * Checks if the object id is unique for the given object. An object might be ambiguously annotated if it subclasses
	 * an another, already annotated class or interface with different id values
	 * 
	 * @param o object to test
	 * @return true if the objectid annotation is clear and unique
	 */
	public boolean isAmbiguous ( Object o ) {
		boolean result = false;
		List<ObjectID> ids = this.annotationFinder.findAnnotations(o.getClass(), new ArrayList<>());
		for (ObjectID id : ids) {
			for (ObjectID id2 : ids) {
				if (id != id2 && id.value() != id2.value()) {
					// definitely not unique
					result = true;
					break;
				}
			}
		}
		return result;
	}
}