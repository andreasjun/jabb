// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.dt;

import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import info.junius.library.jabb.ParseException;
import info.junius.library.jabb.constants.Type;
import info.junius.library.jabb.util.LengthDecoder;

/**
 * Parses byte-array to RawBinary type.
 * Created: 11 Oct 2015, 11:30:23 am
 * 
 * @author Andreas Junius
 */
public class BinaryParser {

	/**
	 * 1 byte id, 1 byte type, 1 byte length, 0 bytes for null value denotes the minimum length of a JABB object array
	 */
	private static final int			MINIMUM_LENGTH	= 3;

	/** length decoder utility */
	private static final LengthDecoder	LENGTHDECODER	= new LengthDecoder();

	/**
	 * Parses octet-array and returns RawBinary instance
	 * 
	 * @param input binary message
	 * @param target target container
	 * @return fully initialised target container
	 * @throws ParseException if unable to parse the octet array
	 */
	public RawBinary parse ( ByteBuffer input, RawBinary target ) throws ParseException {
		// id/type/length/value
		if (target == null) {
			throw new ParseException("Missing target");
		}
		if (input == null) {
			throw new ParseException("Missing byte buffer");
		}
		if (input.remaining() < MINIMUM_LENGTH) {
			throw new ParseException("Not enough remaining elements");
		}
		// set id
		target.setId(input.get());
		// set type
		target.setType(input.get());
		// read length
		long length = 0;
		try {
			length = LENGTHDECODER.decode(input);
		} catch (IOException e) {
			throw new ParseException("Unable to read object length", e);
		}
		// convert type
		Type type = Type.getByFlag(target.getType());
		// parse type
		switch (type) {
			case NULL:
				target.setValue(Converter.NULL);
				break;
			case BYTE:
			case SHORT:
			case INTEGER:
			case LONG:
			case FLOAT:
			case DOUBLE:
			case BOOLEAN:
			case CHARACTER:
			case STRING:
			case DATE:
			case TIMESTAMP:
			case ENUM:
			case BYTE_ARRAY:
			case SHORT_ARRAY:
			case INT_ARRAY:
			case LONG_ARRAY:
			case FLOAT_ARRAY:
			case DOUBLE_ARRAY:
			case BOOLEAN_ARRAY:
			case CHAR_ARRAY:
				// values
				if (length > 0) {
					byte[] value = new byte[(int)length];
					try {
						input.get(value);
					} catch (BufferUnderflowException e) {
						throw new ParseException("Not enough bytes to read the value", e);
					}
					target.setValue(value);
				}
				break;
			case OBJECT:
				// container object
				if (length > 0) {
					byte[] value = new byte[(int)length];
					try {
						input.get(value);
					} catch (BufferUnderflowException e) {
						throw new ParseException("Not enough bytes to read the value", e);
					}
					ByteBuffer object = ByteBuffer.wrap(value);
					do {
						target.addChild(this.parse(object, new RawBinary()));
					} while (object.hasRemaining());
				}
				break;
			case COLLECTION:
				// get the value bytes, iterate the array to create elements...
				if (length > 0) {
					byte[] collection = new byte[(int)length];
					try {
						input.get(collection);
					} catch (BufferUnderflowException e) {
						throw new ParseException("Not enough bytes to read the collection", e);
					}
					ByteBuffer rawCollection = ByteBuffer.wrap(collection);
					do {
						target.addChild(this.parse(rawCollection, new RawBinary()));
					} while (rawCollection.hasRemaining());
				}
				break;
			case ARRAY:
				// get the value bytes, iterate the array to create elements...
				if (length > 0) {
					byte[] array = new byte[(int)length];
					try {
						input.get(array);
					} catch (BufferUnderflowException e) {
						throw new ParseException("Not enough bytes to read the array", e);
					}
					ByteBuffer rawArray = ByteBuffer.wrap(array);
					do {
						target.addChild(this.parse(rawArray, new RawBinary()));
					} while (rawArray.hasRemaining());
				}
				break;
			case MAP:
				// get the value bytes, iterate the array to create map entries...
				if (length > 0) {
					byte[] array = new byte[(int)length];
					try {
						input.get(array);
					} catch (BufferUnderflowException e) {
						throw new ParseException("Not enough bytes to read the map", e);
					}
					ByteBuffer rawArray = ByteBuffer.wrap(array);
					RawBinary rawBinary = null;
					RawBinary entry = null;
					int count = 0;
					do {
						// map consists of entry type containing key and value
						// map
						// ----entry
						// --------key
						// --------value
						// parse that chunk
						rawBinary = this.parse(rawArray, new RawBinary());
						// every third element is an entry element
						if (count % 3 == 0) {
							if (rawBinary.getValue() != null && rawBinary.getValue().length > 0) {
								throw new ParseException("Map entry contains unexpected value");
							}
							rawBinary.setValue(null);	// container
							target.addChild(rawBinary); // add entry
							entry = rawBinary;			// keep reference
						} else {
							// add key and child (same order as encountered)
							if (entry != null) {
								entry.addChild(rawBinary);
							}
						}
						// keep track of entries
						count++;
					} while (rawArray.hasRemaining());
				}
				break;
			default:
		}
		return target;
	}
}