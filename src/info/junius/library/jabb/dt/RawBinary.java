// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.dt;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import info.junius.library.jabb.ParseException;
import info.junius.library.jabb.util.ArrayConcatenator;
import info.junius.library.jabb.util.LengthEncoder;

/**
 * A RawBinary represents either a value or a container. If it is a container, it won't have a value but children. Using
 * the same node on multiple locations is not supported and will lead to unexpected results.<br>
 * This class implements equals and hashcode that does not consider the parent relationship, i.e. two nodes are equal if
 * they have the same children and values even if they have different parents. This avoids a stack overflow and allows
 * to compare different branches of a tree.
 * Created: 29 Jul 2015, 1:26:03 pm
 * 
 * @author Andreas Junius
 */
public class RawBinary implements Iterable<RawBinary> {

	/**
	 * the minimum lenght of the shortest possible rawBinary in bytes <br>
	 * is 3 (bytes: { 1, 0, 0 } based on: id, type, length, no value)
	 */
	public static final int					MIN_LENGTH_RAW		= 3;

	/** utility */
	private static final ArrayConcatenator	ARRAYCONCATENATOR	= new ArrayConcatenator();

	/** length encoder utility */
	private static final LengthEncoder		LENGTHENCODER		= new LengthEncoder();

	/** object ID or member ID */
	private byte							id					= 0;

	/** data type */
	private byte							type				= 0;

	/** value */
	private byte[]							value				= null;

	/** parent */
	private RawBinary						parent				= null;

	/** children */
	private List<RawBinary>					children			= null;

	/** internal comparator for equals and hashCode */
	private Comparator<RawBinary>			comparator			= new ChildComparator();

	/**
	 * Returns the byte representation of this object (-tree), i.e. id/type/length/value as set.
	 * 
	 * @return byte array representing this object (-tree)
	 */
	public byte[] getObjectValue () {
		// assembles octet string recursively
		byte[] result = null;
		if (this.children != null && !this.children.isEmpty()) {
			// this is a container
			byte[] value = null;
			for (RawBinary node : this.children) {
				value = ARRAYCONCATENATOR.concatenate(value, node.getObjectValue());
			}
			// calculate length
			byte[] length = LENGTHENCODER.encode(value);
			// assemble octet string
			result = ARRAYCONCATENATOR.concatenate(this.id, this.type);
			result = ARRAYCONCATENATOR.concatenate(result, length);
			result = ARRAYCONCATENATOR.concatenate(result, value);
		} else if (this.value != null) {
			// a value
			// calculate length
			byte[] length = LENGTHENCODER.encode(this.value);
			// assemble octet string
			result = ARRAYCONCATENATOR.concatenate(this.id, this.type);
			result = ARRAYCONCATENATOR.concatenate(result, length);
			result = ARRAYCONCATENATOR.concatenate(result, this.value);
		} else {
			// this is an empty object, assemble octet string
			result = ARRAYCONCATENATOR.concatenate(this.id, this.type);
			result = ARRAYCONCATENATOR.concatenate(result, new byte[] { 0 });
		}
		return result;
	}

	/**
	 * Parses the given octet array and returns a RawBinary structure
	 * 
	 * @param octetArray octet array
	 * @return RawBinary instance, never null
	 * @throws ParseException if parsing fails
	 */
	public static RawBinary parse ( byte[] octetArray ) throws ParseException {
		// create a parser
		BinaryParser binaryParser = new BinaryParser();
		// parse and return
		return binaryParser.parse(ByteBuffer.wrap(octetArray), new RawBinary());
	}

	/**
	 * @return the id
	 */
	public byte getId () {
		return this.id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId ( byte id ) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public byte getType () {
		return this.type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType ( byte type ) {
		this.type = type;
	}

	/**
	 * Returns the value which might be null
	 * 
	 * @return the value or null
	 */
	public byte[] getValue () {
		// null has a special meaning, don't return empty array (even if FindBugs claims otherwise)
		return (this.value != null) ? this.value.clone() : null;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue ( byte[] value ) {
		this.value = (value != null) ? value.clone() : null;
	}

	/**
	 * Returns a direct child of this element identified by its ID or null if there is no such an element
	 * 
	 * @param id target ID
	 * @return child or null
	 */
	public RawBinary getChild ( byte id ) {
		RawBinary result = null;
		if (this.children != null) {
			for (RawBinary child : this.children) {
				if (child.getId() == id) {
					result = child;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Returns the children of this element.
	 * 
	 * @return the children list, may be empty, never null
	 */
	public List<RawBinary> getChildren () {
		return (this.children != null) ? this.children : new ArrayList<>();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString () {
		return String.format("RawBinary [id=%s, type=%s, value=%s, children=%s]", this.id, this.type, Arrays.toString(this.value), this.children);
	}

	/**
	 * Adds the child to this node and sets its parent.
	 * 
	 * @param child child to add
	 */
	public void addChild ( RawBinary child ) {
		// add the child to this node and sets its parent
		if (this.children == null) {
			this.children = new ArrayList<>();
		}
		child.parent = this;
		this.children.add(child);
	}

	/**
	 * Returns the size of the tree, i.e. the number of nodes incl. this node
	 * 
	 * @return tree size
	 */
	public int size () {
		int size = 0;
		// counts all nodes in the object tree
		for (@SuppressWarnings ( "unused" ) RawBinary node : this) {
			size++;
		}
		return size;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode () {
		// generated code, intentionally not considering parent member
		final int prime = 31;
		int result = 1;
		if (this.children == null) {
			result = prime * result + 0;
		} else {
			// sort the list to calculate the hashcode (do this on a copy to avoid side effects)
			List<RawBinary> thisChildren = new ArrayList<>();
			thisChildren.addAll(this.children);
			Collections.sort(thisChildren, this.comparator);
			// System.out.println(thisChildren);
			// calculate hashcode
			result = prime * result + thisChildren.hashCode();
		}
		result = prime * result + this.id;
		result = prime * result + this.type;
		result = prime * result + Arrays.hashCode(this.value);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals ( Object obj ) {
		// generated code, intentionally not considering parent member
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (! (obj instanceof RawBinary)) {
			return false;
		}
		RawBinary other = (RawBinary)obj;
		if (this.children == null) {
			if (other.children != null) {
				return false;
			}
		} else {
			// this children != null here, but other children might be null
			if (other.children == null) {
				return false;
			}
			// sort both lists for comparison (do this on a copy to avoid side effects)
			List<RawBinary> thisChildren = new ArrayList<>();
			List<RawBinary> otherChildren = new ArrayList<>();
			otherChildren.addAll(other.children);
			thisChildren.addAll(this.children);
			Collections.sort(thisChildren, this.comparator);
			Collections.sort(otherChildren, this.comparator);
			// compare sorted lists
			if (!thisChildren.equals(otherChildren)) {
				return false;
			}
		}
		if (this.id != other.id) {
			return false;
		}
		if (this.type != other.type) {
			return false;
		}
		if (!Arrays.equals(this.value, other.value)) {
			return false;
		}
		return true;
	}

	/**
	 * Returns the parent of this node or null if this is a root node.
	 * 
	 * @return the parent
	 */
	protected RawBinary getParent () {
		return this.parent;
	}

	/**
	 * Tests if there a child nodes present
	 * 
	 * @return true if this node has at least one child node
	 */
	protected boolean hasChildren () {
		return (this.children != null && !this.children.isEmpty());
	}

	/**
	 * Returns first child or null
	 * 
	 * @return first child or null
	 */
	protected RawBinary getFirstChild () {
		return (this.children != null && !this.children.isEmpty()) ? this.children.get(0) : null;
	}

	/**
	 * Checks if this node has a next sibling
	 * 
	 * @return true if this node has a next sibling
	 */
	protected boolean hasNextSibling () {
		boolean result = false;
		if (this.parent != null && this.parent.children != null) {
			// find own index and see if there are more elements
			for (int index = 0; index < this.parent.children.size(); index++) {
				// check for instance equality
				if (this == this.parent.children.get(index)) {
					result = index < this.parent.children.size() - 1;
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Returns the next sibling to this node
	 * 
	 * @return next node
	 */
	protected RawBinary getNextSibling () {
		RawBinary result = null;
		if (this.parent != null && this.parent.children != null) {
			// find own index and see if there are more elements
			for (int index = 0; index < this.parent.children.size(); index++) {
				// check for instance equality
				if (this == this.parent.children.get(index) && index < this.parent.children.size() - 1) {
					// return element after this one
					result = this.parent.children.get(index + 1);
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Returns an iterator that iterates the tree in main order
	 * 
	 * @return iterator
	 */
	@Override
	public Iterator<RawBinary> iterator () {
		return new Iterator<RawBinary>() {

			/** root node */
			private RawBinary	rootNode	= RawBinary.this;

			/** current node */
			private RawBinary	currentNode	= RawBinary.this;

			/**
			 * {@inheritDoc}
			 */
			@Override
			public boolean hasNext () {
				return this.currentNode != null;
			}

			/**
			 * {@inheritDoc}
			 */
			// NoSuchElementException - if the iteration has no more elements
			@Override
			public RawBinary next () throws NoSuchElementException {
				// returns the current node
				RawBinary result = this.currentNode;
				// and moves on to the next one
				if (this.currentNode.hasChildren()) {
					// its first child
					this.currentNode = this.currentNode.getFirstChild();
				} else if (this.currentNode.hasNextSibling()) {
					// or its next sibling
					this.currentNode = this.currentNode.getNextSibling();
				} else {
					// or up the hierarchy until we reach either the root or a node that has a next sibling
					RawBinary node = this.currentNode;
					while (true) {
						// go one up
						node = node.getParent();
						// break if this is the root
						if (node == null || node == this.rootNode) {
							this.currentNode = null;
							break;
						}
						// not root, but there is a next sibling
						if (node.hasNextSibling()) {
							this.currentNode = node.getNextSibling();
							break;
						}
					}
				}
				return result;
			}
		};
	}

	/**
	 * Combines id, type and value for comparison
	 */
	private static class ChildComparator implements Comparator<RawBinary>, Serializable {

		/** serialVersionUID */
		private static final long serialVersionUID = 7199543552474318155L;

		@Override
		public int compare ( RawBinary rawBinary1, RawBinary rawBinary2 ) {
			// combine id, type and value
			long comparisonValue_1 = 64L * rawBinary1.getId()
										+ 32L * rawBinary1.getType()
										+ this.getSum(rawBinary1.getValue());
			long comparisonValue_2 = 64L * rawBinary2.getId()
										+ 32L * rawBinary2.getType()
										+ this.getSum(rawBinary2.getValue());
			return (int) (comparisonValue_1 - comparisonValue_2);
		}

		/**
		 * Sums up the value bytes
		 * 
		 * @param value object value
		 * @return sum of bytes
		 */
		private long getSum ( byte[] value ) {
			long result = 0;
			if (value != null && value.length > 0) {
				for (byte b : value) {
					result += b;
				}
			}
			return result;
		}
	};
}