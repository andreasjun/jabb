// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Map;
import info.junius.library.jabb.MemberID;
import info.junius.library.jabb.ObjectEncoder;
import info.junius.library.jabb.ObjectID;
import info.junius.library.jabb.constants.MapEntryId;
import info.junius.library.jabb.constants.Type;
import info.junius.library.jabb.dt.RawBinary;
import info.junius.library.jabb.util.AnnotationReader;
import info.junius.library.jabb.util.EncoderFactory;
import info.junius.library.jabb.util.InstanceFactory;
import info.junius.library.jabb.util.ObjectIdAnnotationFinder;
import info.junius.library.jabb.validator.JabbAnnotationValidator;

/**
 * Main class that encodes JABB objects
 * Created: 22 Aug 2015, 11:34:10 am
 * 
 * @author Andreas Junius
 */
public class DefaultObjectEncoder implements ObjectEncoder {

	/** encoder factory */
	private static final EncoderFactory		FACTORY					= new EncoderFactory();

	/** reads annotated methods */
	private static final AnnotationReader	ANNOTATIONREADER		= new AnnotationReader();

	/** utility, creates instance based on GenericType information */
	private static final InstanceFactory	INSTANCEFACTORY			= new InstanceFactory();

	/** utility */
	private ObjectIdAnnotationFinder		annotationFinder		= new ObjectIdAnnotationFinder();

	/** utility */
	private JabbAnnotationValidator			jabbAnnotationValidator	= new JabbAnnotationValidator();

	/**
	 * {@inheritDoc}
	 */
	@Override
	public byte[] encode ( Object object ) {
		// get annotation for the object id
		ObjectID objectId = this.annotationFinder.get(object);
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId( (objectId != null) ? objectId.value() : 0);
		return this.encode(object, rawBinary).getObjectValue();
	}

	/**
	 * Worker method that encodes its input
	 * 
	 * @param object object to encode
	 * @param rawBinary rawBinary matching the input object
	 * @return rawBinary
	 */
	protected RawBinary encode ( Object object, RawBinary rawBinary ) {
		// decide type of input
		Type inputType = Type.getByValue(object);
		// special case, the object has no annotations ==> treat as string value if it returns a string value, otherwise
		// as a null value
		if (Type.OBJECT.equals(inputType) && !this.jabbAnnotationValidator.isValidJabbType(object)) {
			// use toString to serialise that object if it has a string arg constructor
			// set to null otherwise
			if (INSTANCEFACTORY.hasStringArgConstructor(object.getClass())) {
				inputType = Type.STRING;
				object = object.toString();
			} else {
				inputType = Type.NULL;
			}
		}
		// set the detected type
		rawBinary.setType(inputType.getFlag());
		// encode if we got one
		switch (inputType) {
			case NULL:
				rawBinary.setValue(FACTORY.getEncoder(null).encode(object));
				break;
			case BYTE:
			case SHORT:
			case INTEGER:
			case LONG:
			case FLOAT:
			case DOUBLE:
			case BOOLEAN:
			case CHARACTER:
			case STRING:
			case DATE:
			case TIMESTAMP:
			case ENUM:
			case BYTE_ARRAY:
			case SHORT_ARRAY:
			case INT_ARRAY:
			case LONG_ARRAY:
			case FLOAT_ARRAY:
			case DOUBLE_ARRAY:
			case BOOLEAN_ARRAY:
			case CHAR_ARRAY:
				// get a suitable encoder and encode the atomic value
				rawBinary.setValue(FACTORY.getEncoder(object).encode(object));
				break;
			case OBJECT:
				// this might be another annotated object. If it is not annotated, nothing will be added to the tree
				Map<Method, MemberID> getter = ANNOTATIONREADER.getGetter(object);
				// iterate over those
				if (!getter.isEmpty()) {
					for (Map.Entry<Method, MemberID> entry : getter.entrySet()) {
						try {
							// get the value
							Object value = entry.getKey().invoke(object);
							// get the id
							MemberID memberId = entry.getValue();
							// no need to transfer null value for an object
							if (value != null && memberId != null) {
								// create raw type
								RawBinary child = new RawBinary();
								// add member id
								child.setId(memberId.value());
								// encode the value and add it to this object
								rawBinary.addChild(this.encode(entry.getKey().invoke(object), child));
							}
						} catch (IllegalAccessException | InvocationTargetException e) {
							throw new IllegalArgumentException("Unable to encode jabb object", e);
						}
					}
				}
				break;
			case COLLECTION:
				if (object != null) {
					// object is a collection
					Collection<?> c = (Collection<?>)object;
					if (!c.isEmpty()) {
						RawBinary child = null;
						for (Object o : c) {
							// create raw type
							child = new RawBinary();
							// add member id
							child.setId((byte)0);
							// encode the value and add it to this object
							// o could be null, but in case of a collection, null values should be transferred
							rawBinary.addChild(this.encode(o, child));
						}
					}
				}
				break;
			case ARRAY:
				// this is an array of objects (never primitives), see Type enum
				if (object != null) {
					Object[] array = (Object[])object;
					RawBinary child = null;
					for (Object o : array) {
						// create raw type
						child = new RawBinary();
						// add member id
						child.setId((byte)0);
						// encode the value and add it to this object
						// o could be null, but in case of an array, null values should be transferred
						rawBinary.addChild(this.encode(o, child));
					}
				}
				break;
			case MAP:
				if (object != null) {
					Map<?, ?> map = (Map<?, ?>)object;
					if (!map.isEmpty()) {
						RawBinary element = null;	// wraps key and value
						RawBinary key = null;		// key
						RawBinary value = null;		// value
						for (Map.Entry<?, ?> entry : map.entrySet()) {
							// create element
							element = new RawBinary();
							// add a key and a value
							key = new RawBinary();
							value = new RawBinary();
							element.setId((byte)0);
							key.setId(MapEntryId.KEY.getId());		// use id to indicate key
							value.setId(MapEntryId.VALUE.getId());	// use id to indicate value
							// encode key and value
							// o could be null, but in case of a map, null values should be transferred
							// some maps even allow for a null key
							key = this.encode(entry.getKey(), key);
							value = this.encode(entry.getValue(), value);
							// add key and value
							element.addChild(key);
							element.addChild(value);
							// add the entry to the map
							rawBinary.addChild(element);
						}
					}
				}
				break;
			default:
		}
		return rawBinary;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString () {
		return String.format("DefaultObjectEncoder []");
	}
}