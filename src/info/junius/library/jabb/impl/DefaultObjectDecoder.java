// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.impl;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import info.junius.library.jabb.Generic;
import info.junius.library.jabb.MemberID;
import info.junius.library.jabb.ObjectDecoder;
import info.junius.library.jabb.ParseException;
import info.junius.library.jabb.constants.MapEntryId;
import info.junius.library.jabb.constants.Type;
import info.junius.library.jabb.dt.Decoder;
import info.junius.library.jabb.dt.ObjectCreationException;
import info.junius.library.jabb.dt.RawBinary;
import info.junius.library.jabb.generics.GenericType;
import info.junius.library.jabb.generics.GenericsParser;
import info.junius.library.jabb.util.AnnotationReader;
import info.junius.library.jabb.util.DecoderFactory;
import info.junius.library.jabb.util.GenericAnnotationFinder;
import info.junius.library.jabb.util.InstanceFactory;
import info.junius.library.jabb.validator.JabbAnnotationValidator;

/**
 * Decodes JABB objects
 * Created: 31 Jan 2016, 11:32:24 am
 * 
 * @author Andreas Junius
 */
public class DefaultObjectDecoder implements ObjectDecoder {

	/** a list contains one element type. 'level' needs to be increased by one on every recursive call */
	private static final int						NUMBER_OF_ELEMENTTYPES_COLLECTION	= 1;

	/** an entry contains two elements, key and value. 'level' needs to be increased by two on every recursive call */
	private static final int						NUMBER_OF_ELEMENTTYPES_MAP			= 2;

	/** index added to the level for a key in a map entry */
	private static final int						MAP_KEY_INDEX						= 0;

	/** index added to the level for a value in a map entry */
	private static final int						MAP_VALUE_INDEX						= 1;

	/** decoder factory */
	private static final DecoderFactory				DECODERFACTORY						= new DecoderFactory();

	/** reads annotated methods */
	private static final AnnotationReader			ANNOTATIONREADER					= new AnnotationReader();

	/** parses generics annotation */
	private static final GenericsParser				GENERICSPARSER						= new GenericsParser();

	/** utility, creates instance based on GenericType information */
	private static final InstanceFactory			INSTANCEFACTORY						= new InstanceFactory();

	/** utility, tests if a class or object is a properly annotated JABB type */
	private static final JabbAnnotationValidator	JABB_ANNOTATION_VALIDATOR			= new JabbAnnotationValidator();

	/** utility that tries to find the @Generic annotation for a given method */
	private static final GenericAnnotationFinder	GENERIC_ANNOTATION_FINDER			= new GenericAnnotationFinder();

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings ( "unchecked" )
	public <T> T decode ( byte[] binary, Class<? extends T> rootClazz ) throws ParseException {
		// validate input
		if (binary == null) {
			throw new ParseException("binary must not be null");
		}
		if (binary.length < RawBinary.MIN_LENGTH_RAW) {
			throw new ParseException("binary must not be empty, nothing to parse. Got " + Arrays.toString(binary));
		}
		if (rootClazz == null) {
			throw new ParseException("rootType must not be null");
		}
		// parse input into a tree of RawBinary objects
		RawBinary rawBinary = RawBinary.parse(binary);
		// parse tree into type(s) using recursion and return
		return (T)this.parse(rawBinary, rootClazz, null, 0);
	}

	/**
	 * Parses the given rawBinary into the target type. Calls itself recursively depending on type
	 * 
	 * @param rawBinary raw binary to parse
	 * @param targetClazz target type
	 * @param genericInfo generic information if available
	 * @return parsed value
	 * @throws ParseException parsing errors
	 */
	@SuppressWarnings ( { "unchecked", "rawtypes" } )
	protected Object parse (	RawBinary rawBinary,
								Class<?> targetClazz,
								Generic genericInfo,
								int level ) throws ParseException {
		Object value = null;
		// try {
		// get source type (which may be null)
		Type sourceType = Type.getByFlag(rawBinary.getType());
		// determine target type based on class
		Type targetType = Type.getByClazz(targetClazz);
		// special case, source: STRING, target: OBJECT (e.g. File, URL, URI, etc..)
		if (Type.STRING.equals(sourceType) && Type.OBJECT.equals(targetType)) {
			// this kind of object is always a leaf, i.e. no further recursive calls from here
			value = this.parseStringObject(rawBinary, targetClazz);
		} else {
			// test if source and target types match
			if (!Type.NULL.equals(sourceType) && !sourceType.equals(targetType)) {
				throw new ParseException("Incompatible source type and target type, source: "
											+ sourceType
											+ ", target: "
											+ targetType);
			}
			// switch on type (source type and target type are equal at this position)
			switch (sourceType) {
				// all containers get analysed and trigger recursive calls
				case OBJECT:
					value = this.parseJabbObject(rawBinary, targetClazz);
					break;
				case ARRAY:
					value = this.parseObjectArray(rawBinary, targetClazz);
					break;
				case COLLECTION:
					value = this.parseCollection(rawBinary, targetClazz, genericInfo, level);
					break;
				case MAP:
					value = this.parseMap(rawBinary, targetClazz, genericInfo, level);
					break;
				// null, enum and primitive types return a result, i.e. these denote the termination condition
				case ENUM:
					// read enum name using suitable decoder
					String enumName = (String)this.parsePrimitive(rawBinary);
					if (enumName != null && targetClazz.isEnum()) {
						value = this.parseEnum(enumName, (Class<? extends Enum>)targetClazz);
					}
					break;
				default:
					// must be one of numerous primitive types
					value = this.parsePrimitive(rawBinary);
			}
		}
		return value;
	}

	/**
	 * Parses raw binary into an enum of the requested type.
	 * 
	 * @param rawBinary data
	 * @param targetClazz type derived from setter
	 * @return enum value
	 * @throws ParseException parsing problems
	 */
	@SuppressWarnings ( "unchecked" )
	protected Enum<?> parseEnum (	String enumName,
									@SuppressWarnings ( "rawtypes" ) Class<? extends Enum> targetClazz ) throws ParseException {
		Enum<?> enumValue = null;
		try {
			enumValue = Enum.valueOf(targetClazz, enumName);
		} catch (IllegalArgumentException | NullPointerException e) {
			throw new ParseException("Unable to parse enum", e);
		}
		return enumValue;
	}

	/**
	 * Decodes all primitives and primitive arrays, i.e.:<br>
	 * NULL, BYTE, SHORT, INTEGER, LONG, FLOAT, DOUBLE, BOOLEAN, CHARACTER, STRING, DATE, TIMESTAMP<br>
	 * BYTE_ARRAY, SHORT_ARRAY, INT_ARRAY, LONG_ARRAY, FLOAT_ARRAY, DOUBLE_ARRAY, BOOLEAN_ARRAY, CHAR_ARRAY
	 * 
	 * @param rawBinary rawBinary
	 * @return value derived from byte array
	 * @throws ParseException
	 */
	protected Object parsePrimitive ( RawBinary rawBinary ) throws ParseException {
		// decode depending on type
		Type targetType = Type.getByFlag(rawBinary.getType());
		Decoder<?> decoder = DECODERFACTORY.getDecoder(targetType);
		if (decoder == null) {
			throw new ParseException("No decoder for type " + targetType);
		}
		return decoder.decode(rawBinary.getValue());
	}

	/**
	 * Parses the raw value into a string and creates the target object using its string arg constructor.
	 * 
	 * @param rawBinary raw binary
	 * @param targetClazz target class, needs strign arg constructor
	 * @return value derived from byte array
	 * @throws ParseException missing string arg constructor or instantiation problems
	 */
	protected Object parseStringObject ( RawBinary rawBinary, Class<?> targetClazz ) throws ParseException {
		Object result = null;
		Decoder<String> decoder = DECODERFACTORY.getDecoder(Type.STRING);
		String value = decoder.decode(rawBinary.getValue());
		try {
			result = INSTANCEFACTORY.getInstance(targetClazz, value);
		} catch (ObjectCreationException e) {
			throw new ParseException("Unable to create object from string value. Missing String constructor", e);
		}
		return result;
	}

	/**
	 * Parses a rawBinary by doing recursive calls on the given JABB object
	 * 
	 * @param rawBinary data source
	 * @param targetClazz valid JABB object type
	 * @return object or null
	 * @throws ParseException
	 */
	protected Object parseJabbObject ( RawBinary rawBinary, Class<?> targetClazz ) throws ParseException {
		Object value = null;
		// is it a JABB object?
		if (JABB_ANNOTATION_VALIDATOR.isValidJabbType(targetClazz)) {
			// create desired object
			try {
				value = INSTANCEFACTORY.getInstance(targetClazz);
			} catch (ObjectCreationException e) {
				throw new ParseException("Unable to create instance of JABB object type", e);
			}
			// get all setters of the target
			Map<Method, MemberID> setters = ANNOTATIONREADER.getSetter(value);
			// iterate the setters and do the appropriate call depending on the object type
			for (Map.Entry<Method, MemberID> entry : setters.entrySet()) {
				Class<?> parameterType = null;
				RawBinary matchingChild = null;
				// get parameter type from method signature
				Method setter = entry.getKey();
				if (setter.getParameterCount() == 1) {
					Class<?>[] parameterTypes = setter.getParameterTypes();
					parameterType = (parameterTypes.length > 0) ? parameterTypes[0] : null;
				}
				// member id of this setter
				MemberID memberId = entry.getValue();
				// find the matching rawBinary
				matchingChild = rawBinary.getChild(memberId.value());
				// check if there is a generic type information, e.g. for collections or maps
				Generic generic = null;
				// finds the corresponding method that has the generics annotation on it if any
				Method annotatedWithGenerics = null;
				try {
					annotatedWithGenerics = GENERIC_ANNOTATION_FINDER.getAnnotatedMethod(setter);
				} catch (ClassNotFoundException e) {
					throw new ParseException("Unable to find @Generic annotation on method " + setter, e);
				}
				if (annotatedWithGenerics != null) {
					generic = annotatedWithGenerics.getAnnotation(Generic.class);
				}
				// if both values can be determined, do recursive call and assign returned value
				if (parameterType != null && matchingChild != null) {
					try {
						// do recursive call and assign returned value via setter method
						setter.invoke(value, new Object[] { this.parse(matchingChild, parameterType, generic, 0) });
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						throw new ParseException("Unable to assign value to JABB object type using setter "
													+ setter, e);
					}
				}
			}
		}
		// ELSE: not a JABB type, just return null
		return value;
	}

	/**
	 * Parses an array of objects
	 * 
	 * @param rawBinary raw binary representing the array
	 * @param targetClazz array type
	 * @return array
	 * @throws ParseException
	 */
	protected Object parseObjectArray ( RawBinary rawBinary, Class<?> targetClazz ) throws ParseException {
		Object array = null;
		if (!targetClazz.isArray()) {
			throw new ParseException("Wrong target type. Expected target type is of type array, got " + targetClazz);
		}
		List<Object> tmp = new ArrayList<>();
		// find component type of this array
		Class<?> elementType = targetClazz.getComponentType();
		// iterate the child nodes of the given array and do recursive call
		for (RawBinary element : rawBinary.getChildren()) {
			tmp.add(this.parse(element, elementType, null, 0));
		}
		// use reflection to create an array and to set its elements
		array = Array.newInstance(elementType, tmp.size());
		for (int i = 0; i < tmp.size(); i++) {
			Array.set(array, i, tmp.get(i));
		}
		return array;
	}

	/**
	 * Parses the collection of objects
	 * 
	 * @param rawBinary raw data
	 * @param targetClazz type of the collection
	 * @param genericInfo generic information of its elements
	 * @param
	 * @return collection
	 * @throws ParseException
	 */
	@SuppressWarnings ( "unchecked" )
	protected Object parseCollection (	RawBinary rawBinary,
										Class<?> targetClazz,
										Generic genericInfo,
										int level ) throws ParseException {
		Collection<Object> collection = null;
		GenericType type = null;
		if (genericInfo == null || genericInfo.value().isEmpty()) {
			throw new ParseException("Insufficient type information. Use the @Generic annotation to retain generic information during runtime. class "
										+ targetClazz
										+ ", raw binary ID "
										+ rawBinary.getId()
										+ ", raw binary type "
										+ rawBinary.getType());
		}
		try {
			type = GENERICSPARSER.parse(genericInfo.value());
		} catch (ClassNotFoundException e) {
			throw new ParseException("Unable to parse generic type");
		}
		if (type == null) {
			throw new ParseException("Unable to create collection without type information");
		}
		if (type.getParameterList().size() < 1) {
			throw new ParseException("Unable to create map without type information for elements");
		}
		Class<?> elementType = type.getParameterList().get(level).getTypeClass();
		// create a collection
		try {
			collection = (Collection<Object>)INSTANCEFACTORY.getInstance(targetClazz);
		} catch (ObjectCreationException e) {
			throw new ParseException("Unable to create instance of collection. Unsupported collection type "
										+ targetClazz);
		}
		// iterate the child nodes of the given array and do recursive call
		for (RawBinary element : rawBinary.getChildren()) {
			// a list contains one element type. Level needs to be increased by one on every recursive call
			collection.add(this.parse(element, elementType, genericInfo, level + NUMBER_OF_ELEMENTTYPES_COLLECTION));
		}
		return collection;
	}

	/**
	 * Parses map
	 * 
	 * @param rawBinary raw data
	 * @param targetClazz type of map
	 * @param genericInfo generic info (key and value types)
	 * @return map
	 * @throws ParseException
	 */
	@SuppressWarnings ( "unchecked" )
	protected Object parseMap (	RawBinary rawBinary,
								Class<?> targetClazz,
								Generic genericInfo,
								int level ) throws ParseException {
		Map<Object, Object> result = null;
		GenericType type = null;
		if (genericInfo == null || genericInfo.value().isEmpty()) {
			throw new ParseException("Insufficient type information. Use the @Generic annotation to retain generic information during runtime.");
		}
		try {
			type = GENERICSPARSER.parse(genericInfo.value());
		} catch (ClassNotFoundException e) {
			throw new ParseException("Unable to parse generic type");
		}
		if (type == null) {
			throw new ParseException("Unable to create map without type information");
		}
		if (type.getParameterList().size() < 2) {
			throw new ParseException("Unable to create map without type information for key and value");
		}
		try {
			result = (Map<Object, Object>)INSTANCEFACTORY.getInstance(type);
		} catch (ObjectCreationException e) {
			throw new ParseException("Unable to create instance of map " + type, e);
		}
		// iterate entries
		for (RawBinary entry : rawBinary.getChildren()) {
			RawBinary key = entry.getChild(MapEntryId.KEY.getId());
			RawBinary value = entry.getChild(MapEntryId.VALUE.getId());
			Object keyElement = null;
			Object valueElement = null;
			Class<?> keyType = type.getParameterList().get(level + MAP_KEY_INDEX).getTypeClass();
			Class<?> valueType = type.getParameterList().get(level + MAP_VALUE_INDEX).getTypeClass();
			// an entry contains two elements, key and value. Level needs to be increased by two on every recursive call
			keyElement = this.parse(key, keyType, genericInfo, level + NUMBER_OF_ELEMENTTYPES_MAP);
			valueElement = this.parse(value, valueType, genericInfo, level + NUMBER_OF_ELEMENTTYPES_MAP);
			// add pair to map
			if (keyElement != null) {
				result.put(keyElement, valueElement);
			}
		}
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString () {
		return String.format("DefaultObjectDecoder []");
	}
}