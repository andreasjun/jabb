// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.generics;

import info.junius.library.jabb.ParseException;
import info.junius.library.jabb.util.ClassFinder;

/**
 * Parses value of a generics annotation. Simple names will be recognised if they point to classes within java.lang.* or
 * java.util.*.
 * Created: 31 Oct 2015, 11:49:33 am
 * 
 * @author Andreas Junius
 */
public class GenericsParser {

	private static final String	OPEN	= "<";

	private static final String	CLOSE	= ">";

	private static final String	SEP		= ",";

	private static enum Operator {
									NONE,
									OPEN,
									CLOSE,
									NEXT;
	}

	/** utility */
	private static final ClassFinder CLASSFINDER = new ClassFinder();

	/**
	 * Parses the value and returns a tree of generic types
	 * 
	 * @param value generics string
	 * @return type tree
	 * @throws ClassNotFoundException if unable to parse the tree
	 * @throws IllegalArgumentException argument null or empty
	 * @throws ParseException if parentheses are not balanced
	 */
	public GenericType parse ( String value ) throws ClassNotFoundException, ParseException {
		// result
		GenericType type = null;
		// node element
		GenericType element = null;
		int currentIndex = 0;
		int bracketCount = 0;
		String name = null;
		// check input
		if (value == null || value.trim().isEmpty()) {
			throw new IllegalArgumentException("nothing to parse");
		}
		// start parsing
		while (currentIndex < value.length()) {
			name = null;
			int nextOperator = -1;
			int openingBracketIndex = value.indexOf(OPEN, currentIndex);
			int closingBracketIndex = value.indexOf(CLOSE, currentIndex);
			int separatorIndex = value.indexOf(SEP, currentIndex);
			// find minimum index --> i.e. the index of the next operator
			if (openingBracketIndex > -1) {
				nextOperator = openingBracketIndex;
			}
			if (nextOperator < 0 || closingBracketIndex > -1 && closingBracketIndex < nextOperator) {
				nextOperator = closingBracketIndex;
			}
			if (nextOperator < 0 || separatorIndex > -1 && separatorIndex < nextOperator) {
				nextOperator = separatorIndex;
			}
			// find operator type, assume none
			Operator operator = Operator.NONE;
			if (nextOperator > -1) {
				if (nextOperator == openingBracketIndex) {
					operator = Operator.OPEN;
				} else if (nextOperator == closingBracketIndex) {
					operator = Operator.CLOSE;
				} else if (nextOperator == separatorIndex) {
					operator = Operator.NEXT;
				}
			}
			// extract class name
			switch (operator) {
				case NONE:
					name = value;
					currentIndex = value.length();
					// single object
					element = this.getType(name);
					if (element != null) {
						type = this.getType(name);
					}
					break;
				case OPEN:
					name = value.substring(currentIndex, nextOperator);
					bracketCount++;
					currentIndex = nextOperator + 1;
					// create new element
					element = this.getType(name);
					if (element != null) {
						if (type == null) {
							// beginning of a type with parameters
							type = element;
						} else {
							// a parameter to add
							type.addParameter(element);
						}
					}
					break;
				case CLOSE:
					name = value.substring(currentIndex, nextOperator);
					bracketCount--;
					currentIndex = nextOperator + 1;
					element = this.getType(name);
					if (element != null) {
						if (type == null) {
							// TODO: not sure here...
							System.out.println("Found a type followed by close, but no type has been set so far! "
												+ element
												+ " name: "
												+ name);
							type = element;
						} else {
							// last parameter
							type.addParameter(element);
						}
					}
					break;
				case NEXT:
					name = value.substring(currentIndex, nextOperator);
					currentIndex = nextOperator + 1;
					// add new child
					element = this.getType(name);
					if (type != null && element != null) {
						// this is a parameter and there is one more to come
						type.addParameter(element);
					}
					break;
			}
		}
		if (bracketCount != 0) {
			throw new ParseException("Invalid syntax, unbalanced parentheses");
		}
		return type;
	}

	/**
	 * Tries to find class to the given class name (either a simple name or a canonical name)
	 * 
	 * @param className simple name or canonical name
	 * @return generic type or null
	 * @throws ClassNotFoundException
	 */
	private GenericType getType ( String className ) throws ClassNotFoundException {
		GenericType type = null;
		className = className.trim();
		if (!className.isEmpty()) {
			Class<?> clazz = CLASSFINDER.getClassByName(className);
			type = new DefaultGenericType(clazz);
		}
		return type;
	}
}