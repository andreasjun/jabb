// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.encoder;

import static org.junit.Assert.assertArrayEquals;
import info.junius.library.jabb.dt.Encoder;
import info.junius.library.jabb.util.EncoderFactory;
import org.junit.Test;

/**
 * Created: 12 Sep 2015, 1:58:03 pm
 * 
 * @author Andreas Junius
 */
public class TestIntEncoder {

	private EncoderFactory factory = new EncoderFactory();

	@Test
	public void testEncodeMinValue () {
		Encoder encoder = this.factory.getEncoder(Integer.MIN_VALUE);
		byte[] expected = new byte[] { -128, 0, 0, 0 };
		byte[] result = encoder.encode(Integer.MIN_VALUE);
		assertArrayEquals("primitive value min", expected, result);
	}

	@Test
	public void testEncode0Value () {
		Encoder encoder = this.factory.getEncoder((int)0);
		byte[] expected = new byte[] { 0, 0, 0, 0 };
		byte[] result = encoder.encode((int)0);
		assertArrayEquals("primitive value 0", expected, result);
	}

	@Test
	public void testEncodeMaxValue () {
		Encoder encoder = this.factory.getEncoder(Integer.MAX_VALUE);
		byte[] expected = new byte[] { 127, -1, -1, -1 };
		byte[] result = encoder.encode(Integer.MAX_VALUE);
		assertArrayEquals("primitive value max", expected, result);
	}

	@Test
	public void testEncodeMinValueObj () {
		Encoder encoder = this.factory.getEncoder(Integer.MIN_VALUE);
		byte[] expected = new byte[] { -128, 0, 0, 0 };
		byte[] result = encoder.encode(Integer.valueOf(Integer.MIN_VALUE));
		assertArrayEquals("object value min", expected, result);
	}

	@Test
	public void testEncode0ValueObj () {
		Encoder encoder = this.factory.getEncoder((int)0);
		byte[] expected = new byte[] { 0, 0, 0, 0 };
		byte[] result = encoder.encode(Integer.valueOf((int)0));
		assertArrayEquals("object value 0", expected, result);
	}

	@Test
	public void testEncodeMaxValueObj () {
		Encoder encoder = this.factory.getEncoder(Integer.MAX_VALUE);
		byte[] expected = new byte[] { 127, -1, -1, -1 };
		byte[] result = encoder.encode(Integer.valueOf(Integer.MAX_VALUE));
		assertArrayEquals("object value max", expected, result);
	}

	@Test
	public void testEncodeNull () {
		// uses IntEncoder
		Encoder encoder = this.factory.getEncoder((int)0);
		byte[] expected = new byte[] {};
		byte[] result = encoder.encode(null);
		assertArrayEquals("null value", expected, result);
	}

	@Test
	public void testEncodeNull2 () {
		// uses NullEncoder
		Encoder encoder = this.factory.getEncoder(null);
		byte[] expected = new byte[] {};
		byte[] result = encoder.encode(null);
		assertArrayEquals("null value", expected, result);
	}
}