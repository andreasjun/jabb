// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.encoder;

import static org.junit.Assert.assertArrayEquals;
import info.junius.library.jabb.dt.Encoder;
import info.junius.library.jabb.util.EncoderFactory;
import org.junit.Test;

/**
 * Created: 13 Sep 2015, 10:17:15 am
 * 
 * @author Andreas Junius
 */
public class TestCharEncoder {

	private EncoderFactory factory = new EncoderFactory();

	@Test
	public void testEncodeMinValue () {
		Encoder encoder = this.factory.getEncoder(Character.MIN_VALUE); // 0
		byte[] expected = new byte[] { 0, 0 };
		byte[] result = encoder.encode(Character.MIN_VALUE);
		assertArrayEquals("primitive value min", expected, result);
	}

	@Test
	public void testEncodeMaxValue () {
		Encoder encoder = this.factory.getEncoder(Character.MAX_VALUE);	// 65535
		byte[] expected = new byte[] { -1, -1 };
		byte[] result = encoder.encode(Character.MAX_VALUE);
		assertArrayEquals("primitive value max", expected, result);
	}

	@Test
	public void testEncodeMinValueObj () {
		Encoder encoder = this.factory.getEncoder(Character.MIN_VALUE);	// 0
		byte[] expected = new byte[] { 0, 0 };
		byte[] result = encoder.encode(Character.valueOf(Character.MIN_VALUE));
		assertArrayEquals("object value min", expected, result);
	}

	@Test
	public void testEncodeMaxValueObj () {
		Encoder encoder = this.factory.getEncoder(Character.MAX_VALUE);	// 65535
		byte[] expected = new byte[] { -1, -1 };
		byte[] result = encoder.encode(Character.valueOf(Character.MAX_VALUE));
		assertArrayEquals("object value max", expected, result);
	}

	@Test
	public void testEncodeNull () {
		// uses CharEncoder
		Encoder encoder = this.factory.getEncoder((char)0);
		byte[] expected = new byte[] {};
		byte[] result = encoder.encode(null);
		assertArrayEquals("null value", expected, result);
	}

	@Test
	public void testEncodeNull2 () {
		// uses NullEncoder
		Encoder encoder = this.factory.getEncoder(null);
		byte[] expected = new byte[] {};
		byte[] result = encoder.encode(null);
		assertArrayEquals("null value", expected, result);
	}
}