// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.encoder;

import static org.junit.Assert.assertArrayEquals;
import info.junius.library.jabb.dt.Encoder;
import info.junius.library.jabb.util.EncoderFactory;
import org.junit.Test;

/**
 * Created: 20 Sep 2015, 11:07:12 am
 * 
 * @author Andreas Junius
 */
public class TestDoubleArrayEncoder {

	private EncoderFactory factory = new EncoderFactory();

	// test array that's null
	// test array that's empty
	// test array that has elements
	@Test
	public void testEncodeNull () {
		// uses type encoder
		Encoder encoder = this.factory.getEncoder(new double[0]);
		byte[] expected = new byte[] {};
		byte[] result = encoder.encode(null);
		assertArrayEquals("null array", expected, result);
	}

	@Test
	public void testEncodeNull2 () {
		// uses NullEncoder
		Encoder encoder = this.factory.getEncoder(null);
		byte[] expected = new byte[] {};
		byte[] result = encoder.encode(null);
		assertArrayEquals("null value", expected, result);
	}

	@Test
	public void testEncodeEmpty () {
		double[] value = new double[0];
		Encoder encoder = this.factory.getEncoder(value);
		byte[] expected = new byte[] {};
		byte[] result = encoder.encode(value);
		assertArrayEquals("empty array", expected, result);
	}

	@Test
	public void testEncode () {
		double[] value = new double[] { Double.MIN_VALUE, 0, Double.MAX_VALUE };
		Encoder encoder = this.factory.getEncoder(value);
		byte[] expected = new byte[] {	0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0b01111111, (byte)0b11101111,
										(byte)0b11111111, (byte)0b11111111, (byte)0b11111111, (byte)0b11111111,
										(byte)0b11111111, (byte)0b11111111 };
		byte[] result = encoder.encode(value);
		assertArrayEquals("array", expected, result);
	}
}