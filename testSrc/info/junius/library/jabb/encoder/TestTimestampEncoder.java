// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.encoder;

import static org.junit.Assert.assertArrayEquals;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;
import info.junius.library.jabb.dt.Encoder;
import info.junius.library.jabb.util.ArrayConcatenator;
import info.junius.library.jabb.util.EncoderFactory;
import org.junit.Test;

/**
 * Created: 13 Sep 2015, 10:00:37 am
 * 
 * @author Andreas Junius
 */
public class TestTimestampEncoder {

	private EncoderFactory factory = new EncoderFactory();

	@Test
	public void testEncodeMinValue () {
		Date date = new Date(Long.MIN_VALUE);
		Timestamp timestamp = new Timestamp(date.getTime());
		Encoder encoder = this.factory.getEncoder(timestamp);		
		byte[] expectedNanos = this.factory.getEncoder(timestamp.getNanos()).encode(timestamp.getNanos());		
		byte[] expectedMillis = new byte[] { -128, 0, 0, 0, 0, 0, 0, 0  };		
		byte[] expected = new ArrayConcatenator().concatenate(expectedMillis, expectedNanos);		
		byte[] result = encoder.encode(timestamp);
		assertArrayEquals("primitive value min", expected, result);
	}

	@Test
	public void testEncode0Value () {
		Timestamp timestamp = Timestamp.from(Instant.ofEpochMilli(0));
		Encoder encoder = this.factory.getEncoder(timestamp);		
		byte[] expectedNanos = this.factory.getEncoder(timestamp.getNanos()).encode(timestamp.getNanos());		
		byte[] expectedMillis = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0  };		
		byte[] expected = new ArrayConcatenator().concatenate(expectedMillis, expectedNanos);
		byte[] result = encoder.encode(timestamp);
		assertArrayEquals("primitive value 0", expected, result);
	}

	@Test
	public void testEncodeMaxValue () {
		Date date = new Date(Long.MAX_VALUE);
		Timestamp timestamp = new Timestamp(date.getTime());
		Encoder encoder = this.factory.getEncoder(timestamp);
		byte[] expectedNanos = this.factory.getEncoder(timestamp.getNanos()).encode(timestamp.getNanos());		
		byte[] expectedMillis = new byte[] { 127, -1, -1, -1, -1, -1, -1, -1 };
		byte[] expected = new ArrayConcatenator().concatenate(expectedMillis, expectedNanos);
		byte[] result = encoder.encode(timestamp);
		assertArrayEquals("primitive value max", expected, result);
	}

	@Test
	public void testEncodeNull () {
		// uses LongEncoder
		Encoder encoder = this.factory.getEncoder(Timestamp.from(Instant.now()));
		byte[] expected = new byte[] {};
		byte[] result = encoder.encode(null);
		assertArrayEquals("null value", expected, result);
	}

	@Test
	public void testEncodeNull2 () {
		// uses NullEncoder
		Encoder encoder = this.factory.getEncoder(null);
		byte[] expected = new byte[] {};
		byte[] result = encoder.encode(null);
		assertArrayEquals("null value", expected, result);
	}
}