// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

import java.util.Arrays;

/**
 * Created: 10 Feb 2016, 8:09:47 pm
 * 
 * @author Andreas Junius
 */
@ObjectID ( 124 )
public class PrimNdimObjectArrays {

	private Byte[][][]		value0	= null;

	private Short[][]		value1	= null;

	private Integer[][]		value2	= null;

	private Long[][]		value3	= null;

	private Float[][]		value4	= null;

	private Double[][]		value5	= null;

	private Character[][]	value6	= null;

	private Boolean[][]		value7	= null;

	/**
	 * @return the value0
	 */
	@MemberID ( 0 )
	public Byte[][][] getValue0 () {
		return this.value0;
	}

	/**
	 * @param value0 the value0 to set
	 */
	public void setValue0 ( Byte[][][] value0 ) {
		this.value0 = value0;
	}

	/**
	 * @return the value1
	 */
	@MemberID ( 1 )
	public Short[][] getValue1 () {
		return this.value1;
	}

	/**
	 * @param value1 the value1 to set
	 */
	public void setValue1 ( Short[][] value1 ) {
		this.value1 = value1;
	}

	/**
	 * @return the value2
	 */
	@MemberID ( 2 )
	public Integer[][] getValue2 () {
		return this.value2;
	}

	/**
	 * @param value2 the value2 to set
	 */
	public void setValue2 ( Integer[][] value2 ) {
		this.value2 = value2;
	}

	/**
	 * @return the value3
	 */
	@MemberID ( 3 )
	public Long[][] getValue3 () {
		return this.value3;
	}

	/**
	 * @param value3 the value3 to set
	 */
	public void setValue3 ( Long[][] value3 ) {
		this.value3 = value3;
	}

	/**
	 * @return the value4
	 */
	@MemberID ( 4 )
	public Float[][] getValue4 () {
		return this.value4;
	}

	/**
	 * @param value4 the value4 to set
	 */
	public void setValue4 ( Float[][] value4 ) {
		this.value4 = value4;
	}

	/**
	 * @return the value5
	 */
	@MemberID ( 5 )
	public Double[][] getValue5 () {
		return this.value5;
	}

	/**
	 * @param value5 the value5 to set
	 */
	public void setValue5 ( Double[][] value5 ) {
		this.value5 = value5;
	}

	/**
	 * @return the value6
	 */
	@MemberID ( 6 )
	public Character[][] getValue6 () {
		return this.value6;
	}

	/**
	 * @param value6 the value6 to set
	 */
	public void setValue6 ( Character[][] value6 ) {
		this.value6 = value6;
	}

	/**
	 * @return the value7
	 */
	@MemberID ( 7 )
	public Boolean[][] getValue7 () {
		return this.value7;
	}

	/**
	 * @param value7 the value7 to set
	 */
	public void setValue7 ( Boolean[][] value7 ) {
		this.value7 = value7;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this.value0);
		result = prime * result + Arrays.hashCode(this.value1);
		result = prime * result + Arrays.hashCode(this.value2);
		result = prime * result + Arrays.hashCode(this.value3);
		result = prime * result + Arrays.hashCode(this.value4);
		result = prime * result + Arrays.hashCode(this.value5);
		result = prime * result + Arrays.hashCode(this.value6);
		result = prime * result + Arrays.hashCode(this.value7);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals ( Object obj ) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (! (obj instanceof PrimNdimObjectArrays)) {
			return false;
		}
		PrimNdimObjectArrays other = (PrimNdimObjectArrays)obj;
		if (!Arrays.deepEquals(this.value0, other.value0)) {
			return false;
		}
		if (!Arrays.deepEquals(this.value1, other.value1)) {
			return false;
		}
		if (!Arrays.deepEquals(this.value2, other.value2)) {
			return false;
		}
		if (!Arrays.deepEquals(this.value3, other.value3)) {
			return false;
		}
		if (!Arrays.deepEquals(this.value4, other.value4)) {
			return false;
		}
		if (!Arrays.deepEquals(this.value5, other.value5)) {
			return false;
		}
		if (!Arrays.deepEquals(this.value6, other.value6)) {
			return false;
		}
		if (!Arrays.deepEquals(this.value7, other.value7)) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString () {
		return String.format("PrimNdimObjectArrays [value0=%s, value1=%s, value2=%s, value3=%s, value4=%s, value5=%s, value6=%s, value7=%s]", Arrays.deepToString(this.value0), Arrays.deepToString(this.value1), Arrays.deepToString(this.value2), Arrays.deepToString(this.value3), Arrays.deepToString(this.value4), Arrays.deepToString(this.value5), Arrays.deepToString(this.value6), Arrays.deepToString(this.value7));
	}
}