// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.dt;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import info.junius.library.jabb.Element;
import info.junius.library.jabb.Maps;
import info.junius.library.jabb.ObjArrays;
import info.junius.library.jabb.ObjCollections;
import info.junius.library.jabb.ObjectEncoder;
import info.junius.library.jabb.ParseException;
import info.junius.library.jabb.PrimArrays;
import info.junius.library.jabb.Primitives;
import info.junius.library.jabb.TreeNode;
import info.junius.library.jabb.TypeImpl;
import info.junius.library.jabb.TypeInterface;
import info.junius.library.jabb.Wrapper;
import info.junius.library.jabb.impl.DefaultObjectEncoder;
import org.junit.Test;

/**
 * Tests the binary parser using a number of values. Semantics copied from ObjectEncoderTest
 * Created: 11 Oct 2015, 11:20:31 am
 * 
 * @author Andreas Junius
 */
public class RawBinaryDecodeTest {

	/**
	 * Single annotated element
	 */
	@Test
	public void test_1 () throws ParseException {
		RawBinary expected = RawBinary.parse(this.getExpected_1());
		RawBinary result = RawBinary.parse(expected.getObjectValue());
		// assert
		assertEquals("raw binary equality", expected, result);
		assertArrayEquals("raw binary equality", expected.getObjectValue(), result.getObjectValue());
	}

	/**
	 * Annotated object holding primitives
	 */
	@Test
	public void test_2 () throws ParseException {
		RawBinary expected = RawBinary.parse(this.getExpected_2());
		RawBinary result = RawBinary.parse(expected.getObjectValue());
		// assert
		assertEquals("raw binary equality", expected, result);
		assertArrayEquals("raw binary equality", expected.getObjectValue(), result.getObjectValue());
	}

	/**
	 * Annotated object holding primitive wrappers
	 */
	@Test
	public void test_3 () throws ParseException {
		RawBinary expected = RawBinary.parse(this.getExpected_3());
		RawBinary result = RawBinary.parse(expected.getObjectValue());
		// assert
		assertEquals("raw binary equality", expected, result);
		assertArrayEquals("raw binary equality", expected.getObjectValue(), result.getObjectValue());
	}

	/**
	 * Annotated object holding arrays of primitive values
	 */
	@Test
	public void test_4 () throws ParseException {
		RawBinary expected = RawBinary.parse(this.getExpected_4());
		RawBinary result = RawBinary.parse(expected.getObjectValue());
		// assert
		assertEquals("raw binary equality", expected, result);
		assertArrayEquals("raw binary equality", expected.getObjectValue(), result.getObjectValue());
	}

	/**
	 * Annotated object holding arrays of objects
	 */
	@Test
	public void test_5 () throws ParseException {
		RawBinary expected = RawBinary.parse(this.getExpected_5());
		RawBinary result = RawBinary.parse(expected.getObjectValue());
		// assert
		assertEquals("raw binary equality", expected, result);
		assertArrayEquals("raw binary equality", expected.getObjectValue(), result.getObjectValue());
	}

	/**
	 * Annotated object holding collections
	 */
	@Test
	public void test_6 () throws ParseException {
		RawBinary expected = RawBinary.parse(this.getExpected_6());
		RawBinary result = RawBinary.parse(expected.getObjectValue());
		// assert
		assertEquals("raw binary equality", expected, result);
		assertArrayEquals("raw binary equality", expected.getObjectValue(), result.getObjectValue());
	}

	/**
	 * Annotated object holding maps
	 */
	@Test
	public void test_7 () throws ParseException {
		RawBinary expected = RawBinary.parse(this.getExpected_7());
		RawBinary result = RawBinary.parse(expected.getObjectValue());
		// assert
		assertEquals("raw binary equality", expected, result);
		assertArrayEquals("raw binary equality", expected.getObjectValue(), result.getObjectValue());
	}

	/**
	 * Annotated object holding a tree structure
	 */
	@Test
	public void test_8 () throws ParseException {
		RawBinary expected = RawBinary.parse(this.getExpected_8());
		RawBinary result = RawBinary.parse(expected.getObjectValue());
		// assert
		assertEquals("raw binary equality", expected, result);
		assertArrayEquals("raw binary equality", expected.getObjectValue(), result.getObjectValue());
	}

	/**
	 * Object implementing an interface and extending an abstract class
	 */
	@Test
	public void test_9 () throws ParseException {
		RawBinary expected = RawBinary.parse(this.getExpected_9());
		RawBinary result = RawBinary.parse(expected.getObjectValue());
		// assert
		assertEquals("raw binary equality", expected, result);
		assertArrayEquals("raw binary equality", expected.getObjectValue(), result.getObjectValue());
	}

	private byte[] getExpected_1 () {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Element object = new Element();
		object.setValue0("Hello");
		object.setValue1(12345);
		// encode the object
		return encoder.encode(object);
	}

	private byte[] getExpected_2 () {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Primitives object = new Primitives();
		object.setValue0((byte)1);
		object.setValue1((short)1);
		object.setValue2(1);
		object.setValue3(1L);
		object.setValue4(1.0F);
		object.setValue5(1.0D);
		object.setValue6((char)0);
		object.setValue7(true);
		// encode the object
		return encoder.encode(object);
	}

	private byte[] getExpected_3 () {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Wrapper object = new Wrapper();
		object.setValue0(Byte.valueOf((byte)1));
		object.setValue1(Short.valueOf((short)1));
		object.setValue2(Integer.valueOf(1));
		object.setValue3(Long.valueOf(1L));
		object.setValue4(Float.valueOf(1.0F));
		object.setValue5(Double.valueOf(1.0D));
		object.setValue6(Character.valueOf((char)0));
		object.setValue7(Boolean.TRUE);
		// encode the object
		return encoder.encode(object);
	}

	private byte[] getExpected_4 () {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		PrimArrays object = new PrimArrays();
		object.setValue0(new byte[] { 1, 2 });
		object.setValue1(new short[] { 1, 2 });
		object.setValue2(new int[] { 1, 2 });
		object.setValue3(new long[] { 1L, 2L });
		object.setValue4(new float[] { 1.0F, 2.0F });
		object.setValue5(new double[] { 1.0, 2.0 });
		object.setValue6(new char[] { 'a', 'b' });
		object.setValue7(new boolean[] { true, false });
		// encode the object
		return encoder.encode(object);
	}

	private byte[] getExpected_5 () {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		ObjArrays object = new ObjArrays();
		object.setValue0(new Byte[] { 1, 2 });
		object.setValue1(new Short[] { 1, 2 });
		object.setValue2(new Integer[] { 1, 2 });
		object.setValue3(new Long[] { 1L, 2L });
		object.setValue4(new Float[] { 1.0F, 2.0F });
		object.setValue5(new Double[] { 1.0, 2.0 });
		object.setValue6(new Character[] { 'a', 'b' });
		object.setValue7(new Boolean[] { Boolean.TRUE, Boolean.FALSE });
		object.setValue8(new Object[] { new Object(), new Object() });
		object.setValue9(new Element[] { new Element("one", 1), new Element("two", 2) });
		// encode the object
		return encoder.encode(object);
	}

	private byte[] getExpected_6 () {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		ObjCollections object = new ObjCollections();
		// different types of collections
		List<String> list = Arrays.asList(new String[] { "hello", "world" });
		Set<String> set = new HashSet<>();
		set.addAll(list);
		Queue<String> queue = new PriorityQueue<>(list);
		// add them
		object.setValue0(list);
		object.setValue1(set);
		object.setValue2(queue);
		// expected size
		// encode the object
		return encoder.encode(object);
	}

	private byte[] getExpected_7 () {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Maps object = new Maps();
		// different types of maps
		Map<String, Long> value0 = new HashMap<>();
		// unknown object, decide how to deal with
		Map<Integer, Object> value1 = new HashMap<>();
		// known, annotated object
		Map<Integer, Element> value2 = new HashMap<>();
		value0.put("keyOne", 1L);
		value1.put(Integer.valueOf(1), new Object());
		value2.put(Integer.valueOf(1), new Element("hello", 1));
		// add them
		object.setValue0(value0);
		object.setValue1(value1);
		object.setValue2(value2);
		// encode the object
		return encoder.encode(object);
	}

	private byte[] getExpected_8 () {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		TreeNode a = new TreeNode();
		TreeNode b = new TreeNode();
		TreeNode c = new TreeNode();
		TreeNode d = new TreeNode();
		TreeNode e = new TreeNode();
		TreeNode f = new TreeNode();
		a.setChildren(new TreeNode[] { b, f });
		b.setChildren(new TreeNode[] { c, d });
		d.setChildren(new TreeNode[] { e });
		a.setValue(0);
		b.setValue(1);
		c.setValue(2);
		d.setValue(3);
		e.setValue(4);
		f.setValue(5);
		a.setParent(null);
		b.setParent(a);
		c.setParent(b);
		d.setParent(b);
		e.setParent(d);
		f.setParent(a);
		// encode the object
		return encoder.encode(a);
	}

	private byte[] getExpected_9 () {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// build object
		TypeInterface object = new TypeImpl();
		object.setValue0("Hello");
		object.setValue1(42);
		// encode the object
		return encoder.encode(object);
	}
}