// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.dt;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Tests all aspects of the tree structure of a RawBinary type.
 * Created: 8 Aug 2015, 12:05:10 pm
 * 
 * @author Andreas Junius
 */
public class RawBinaryTreeTest {

	/**
	 * Test method for {@link info.junius.library.jabb.dt.RawBinary#addChild(info.junius.library.jabb.dt.RawBinary)}.
	 */
	@Test
	public void testAddChild () {
		// create nodes
		RawBinary a = new RawBinary();
		a.setId((byte)0);
		RawBinary b = new RawBinary();
		b.setId((byte)1);
		RawBinary c = new RawBinary();
		c.setId((byte)2);
		RawBinary d = new RawBinary();
		d.setId((byte)3);
		RawBinary e = new RawBinary();
		e.setId((byte)4);
		RawBinary f = new RawBinary();
		f.setId((byte)5);
		// assemble
		a.addChild(b);
		a.addChild(e);
		b.addChild(c);
		b.addChild(d);
		e.addChild(f);
		// check assembly
		assertSame("b parent", a, b.getParent());
		assertSame("e parent", a, e.getParent());
		assertSame("c parent", b, c.getParent());
		assertSame("d parent", b, d.getParent());
		assertSame("f parent", e, f.getParent());
		// check testers
		assertTrue("a", a.hasChildren());
		assertFalse("a", a.hasNextSibling());
		assertTrue("b", b.hasChildren());
		assertTrue("b", b.hasNextSibling());
		assertFalse("c", c.hasChildren());
		assertTrue("c", c.hasNextSibling());
		assertFalse("d", d.hasChildren());
		assertFalse("d", d.hasNextSibling());
		assertTrue("e", e.hasChildren());
		assertFalse("e", e.hasNextSibling());
		assertFalse("f", f.hasChildren());
		assertFalse("f", f.hasNextSibling());
	}

	/**
	 * Test method for {@link info.junius.library.jabb.dt.RawBinary#iterator()}.
	 */
	@Test
	public void testIterator () {
		// create nodes
		RawBinary a = new RawBinary();
		a.setId((byte)0);
		RawBinary b = new RawBinary();
		b.setId((byte)1);
		RawBinary c = new RawBinary();
		c.setId((byte)2);
		RawBinary d = new RawBinary();
		d.setId((byte)3);
		RawBinary e = new RawBinary();
		e.setId((byte)4);
		RawBinary f = new RawBinary();
		f.setId((byte)5);
		// assemble
		a.addChild(b);
		a.addChild(e);
		b.addChild(c);
		b.addChild(d);
		e.addChild(f);
		// check iterator
		RawBinary[] expectedA = new RawBinary[] { a, b, c, d, e, f };
		RawBinary[] expectedB = new RawBinary[] { b, c, d };
		RawBinary[] expectedC = new RawBinary[] { e, f };
		RawBinary[] expectedD = new RawBinary[] { f };
		int index = 0;
		for (RawBinary node : a) {
			assertSame("expectedA " + index, node, expectedA[index]);
			index++;
		}
		index = 0;
		for (RawBinary node : b) {
			assertSame("expectedB " + index, node, expectedB[index]);
			index++;
		}
		index = 0;
		for (RawBinary node : e) {
			assertSame("expectedC " + index, node, expectedC[index]);
			index++;
		}
		index = 0;
		for (RawBinary node : f) {
			assertSame("expectedD " + index, node, expectedD[index]);
			index++;
		}
	}
}