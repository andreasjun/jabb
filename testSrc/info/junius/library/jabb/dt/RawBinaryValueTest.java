// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.dt;

import static org.junit.Assert.assertArrayEquals;
import java.util.ArrayList;
import java.util.List;
import info.junius.library.jabb.constants.Type;
import info.junius.library.jabb.util.EncoderFactory;
import org.junit.Test;

/**
 * Tests the getObjectValue() method for a number of use cases, i.e. single value, list, etc..
 * Created: 9 Aug 2015, 10:55:23 am
 * 
 * @author Andreas Junius
 */
public class RawBinaryValueTest {

	/** converter utility */
	private EncoderFactory factory = new EncoderFactory();

	/**
	 * Tests a null value
	 */
	@Test
	public void testNoValue () {
		// a single value, in this case a string
		byte[] expected = new byte[] { 1, 0, 0 };// id, type, length, nothing
		byte[] actual = null;
		String valueToEncode = null;
		byte[] value = this.factory.getEncoder(valueToEncode).encode(valueToEncode);
		RawBinary binary = new RawBinary();
		binary.setId((byte)1);
		binary.setType(Type.getByValue(valueToEncode).getFlag());
		binary.setValue(value);
		actual = binary.getObjectValue();
		assertArrayEquals("Octet string null value", expected, actual);
	}

	/**
	 * Tests a single value
	 */
	@Test
	public void testSingleValue () {
		// a single value, in this case a string
		byte[] expected = new byte[] { 1, 10, 12, 72, 101, 108, 108, 111, 32, 87, 111, 114, 108, 100, 33 };
		byte[] actual = null;
		String valueToEncode = "Hello World!";
		byte[] value = this.factory.getEncoder(valueToEncode).encode(valueToEncode);
		RawBinary binary = new RawBinary();
		binary.setId((byte)1);
		binary.setType(Type.getByValue(valueToEncode).getFlag());
		binary.setValue(value);
		actual = binary.getObjectValue();
		assertArrayEquals("Octet string single value", expected, actual);
	}

	/**
	 * Tests a container type, a flat list
	 */
	@Test
	public void testContainerValue () {
		// @formatter:off
		// values as a child of a container value
		byte[] expected = new byte[] { 127, // root id
		                               30, 	// collection type
		                               90, 	// container value length 90 bytes
		                               		1, 	// id of first string
		                               		10, // string type
		                               		24, // length of first string
		                               		89, 111, 117, 32, 119, 111, 110, 39, 116, 32, 98, 101, 108, 105, 101, 118, 101, 32, 105, 116, 32, 98, 117, 116, // string value
		                               		2, 	// id of second string
		                               		10, // string type
		                               		60, // length of second string
		                               		119, 105, 110, 110, 105, 110, 103, 32, 116, 104, 	// 60 bytes for the string value (10)
		                               		101, 32, 108, 111, 116, 116, 101, 114, 121, 32, 	// (20)
		                               		105, 115, 32, 97, 99, 116, 117, 97, 108, 108, 		// (30)
		                               		121, 32, 112, 111, 115, 115, 105, 98, 108, 101, 	// (40)
		                               		32, 97, 108, 98, 101, 105, 116, 32, 118, 101, 		// (50)
		                               		114, 121, 32, 114, 97, 114, 101, 108, 121, 46 };	// (60)
		// @formatter:on
		byte[] actual = null;
		// two string values
		String string1 = "You won't believe it but"; // 24
		String string2 = "winning the lottery is actually possible albeit very rarely."; // 60
		// encoded
		byte[] value1 = this.factory.getEncoder(string1).encode(string1);
		byte[] value2 = this.factory.getEncoder(string2).encode(string2);
		// a list type for the container
		List<String> myList = new ArrayList<>();
		// root node
		RawBinary root = new RawBinary();
		root.setId((byte)127);
		root.setType(Type.getByValue(myList).getFlag());
		// first element
		RawBinary binary = new RawBinary();
		binary.setId((byte)1);
		binary.setType(Type.getByValue(string1).getFlag());
		binary.setValue(value1);
		// second element
		RawBinary binary2 = new RawBinary();
		binary2.setId((byte)2);
		binary2.setType(Type.getByValue(string2).getFlag());
		binary2.setValue(value2);
		// assemble the raw object
		root.addChild(binary);
		root.addChild(binary2);
		actual = root.getObjectValue();
		assertArrayEquals("Octet string single value", expected, actual);
	}

	/**
	 * Tests a tree like structure
	 */
	@Test
	public void testTreeValue () {
		// @formatter:off
		// values as a child of a container value
		byte[] expected = new byte[] { 10, 				// container id 10
		                              	9, 				// type object
		                              	-126, 0, -124, 	// length of object
		                              		1, 	// id of first value
		                              		10,	// type string
		                              		24, // length of this string
		                              		89, 111, 117, 32, 119, 111, 110, 39, 116, 32, 98, 101, 108, 105, 101, 118, 101, 32, 105, 116, 32, 98, 117, 116, // string value
		                              		20, // container id 20
		                              		9, 	// type object
		                              		91, // object length 91
		                              			3,  // id of value
		                              			3,	// type int
		                              			4,  // 4 bytes length
		                              			0, 1, -30, 64, // value
		                              			4, 	// id of value
		                              			3,	// type int
		                              			4, 	// 4 bytes length
		                              			0, 9, -5, -15, // value
		                              			30, // container id 30
		                              			9,	// type object
		                              			74, // 74 bytes length
		                              				5,	// id of value
		                              				6, 	// type double
		                              				8, 	// 8 bytes length
		                              				64, -109, 74, 69, 109, 92, -6, -83, // value
		                              				2,	// id of value
		                              				10,	// type string
		                              				60, // 60 bytes length
		                              				119, 105, 110, 110, 105, 110, 103, 32, 116, 104, 	// (10) 60 bytes value
		                              				101, 32, 108, 111, 116, 116, 101, 114, 121, 32, 	// (20)
		                              				105, 115, 32, 97, 99, 116, 117, 97, 108, 108, 		// (30)
		                              				121, 32, 112, 111, 115, 115, 105, 98, 108, 101, 	// (40)
		                              				32, 97, 108, 98, 101, 105, 116, 32, 118, 101,		// (50)
		                              				114, 121, 32, 114, 97, 114, 101, 108, 121, 46,		// (60) 
		                              		6, 	// id of value
		                              		6, 	// type double
		                              		8,	// 8 bytes length
		                              		64, -74, 46, -23, -116, 126, 40, 36  // value  
		                              
		};
		// @formatter:on
		byte[] actual = null;
		// values
		String string1 = "You won't believe it but"; // 24
		String string2 = "winning the lottery is actually possible albeit very rarely."; // 60
		Object container = new Object();
		int int1 = 123456;
		int int2 = 654321;
		double double1 = 1234.5678D;
		double double2 = 5678.9123D;
		// binary values
		byte[] value1 = this.factory.getEncoder(string1).encode(string1);
		byte[] value2 = this.factory.getEncoder(string2).encode(string2);
		byte[] value3 = this.factory.getEncoder(int1).encode(int1);
		byte[] value4 = this.factory.getEncoder(int2).encode(int2);
		byte[] value5 = this.factory.getEncoder(double1).encode(double1);
		byte[] value6 = this.factory.getEncoder(double2).encode(double2);
		// object structure, nine nodes
		// @formatter:off
		// 	root container
		//	  |---	string1
		//	  |---	container
		//	  		    |--- int1
		//	  		    |--- int2
		//	  		    |--- container
		//	  			      |--- double1
		//	  				  |--- string2
		//	  |---	double2
		// @formatter:on
		// create value nodes
		RawBinary binary1 = new RawBinary();
		binary1.setId((byte)1);
		binary1.setType(Type.getByValue(string1).getFlag());
		binary1.setValue(value1);
		RawBinary binary2 = new RawBinary();
		binary2.setId((byte)2);
		binary2.setType(Type.getByValue(string2).getFlag());
		binary2.setValue(value2);
		RawBinary binary3 = new RawBinary();
		binary3.setId((byte)3);
		binary3.setType(Type.getByValue(int1).getFlag());
		binary3.setValue(value3);
		RawBinary binary4 = new RawBinary();
		binary4.setId((byte)4);
		binary4.setType(Type.getByValue(int2).getFlag());
		binary4.setValue(value4);
		RawBinary binary5 = new RawBinary();
		binary5.setId((byte)5);
		binary5.setType(Type.getByValue(double1).getFlag());
		binary5.setValue(value5);
		RawBinary binary6 = new RawBinary();
		binary6.setId((byte)6);
		binary6.setType(Type.getByValue(double2).getFlag());
		binary6.setValue(value6);
		// container nodes
		RawBinary container1 = new RawBinary();
		container1.setId((byte)10);
		container1.setType(Type.getByValue(container).getFlag());
		RawBinary container2 = new RawBinary();
		container2.setId((byte)20);
		container2.setType(Type.getByValue(container).getFlag());
		RawBinary container3 = new RawBinary();
		container3.setId((byte)30);
		container3.setType(Type.getByValue(container).getFlag());
		// assemble tree structure as outlined above
		container1.addChild(binary1);
		container1.addChild(container2);
		container1.addChild(binary6);
		container2.addChild(binary3);
		container2.addChild(binary4);
		container2.addChild(container3);
		container3.addChild(binary5);
		container3.addChild(binary2);
		// get the value
		actual = container1.getObjectValue();
		// and check
		assertArrayEquals("Octet string single value", expected, actual);
	}
}