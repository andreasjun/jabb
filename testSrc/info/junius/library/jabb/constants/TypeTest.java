// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.constants;

import static org.junit.Assert.assertEquals;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import info.junius.library.jabb.EnumTypes.Color;
import org.junit.Test;

/**
 * Tests type identification method and byte value getter
 * Created: 3 Aug 2015, 1:32:35 pm
 * 
 * @author Andreas Junius
 */
public class TypeTest {

	// primitives
	@Test
	public void testGetByType_null_ () {
		assertEquals("Class null", Type.NULL, Type.getByClazz(null));
	}

	@Test
	public void testGetByType_byte () {
		assertEquals("Class byte", Type.BYTE, Type.getByClazz(Byte.TYPE));
	}

	@Test
	public void testGetByType_short () {
		assertEquals("Class short", Type.SHORT, Type.getByClazz(Short.TYPE));
	}

	@Test
	public void testGetByType_int () {
		assertEquals("Class int", Type.INTEGER, Type.getByClazz(Integer.TYPE));
	}

	@Test
	public void testGetByType_long () {
		assertEquals("Class long", Type.LONG, Type.getByClazz(Long.TYPE));
	}

	@Test
	public void testGetByType_float () {
		assertEquals("Class float", Type.FLOAT, Type.getByClazz(Float.TYPE));
	}

	@Test
	public void testGetByType_double () {
		assertEquals("Class double", Type.DOUBLE, Type.getByClazz(Double.TYPE));
	}

	@Test
	public void testGetByType_boolean () {
		assertEquals("Class boolean", Type.BOOLEAN, Type.getByClazz(Boolean.TYPE));
	}

	@Test
	public void testGetByType_char () {
		assertEquals("Class char", Type.CHARACTER, Type.getByClazz(Character.TYPE));
	}

	// enum
	@Test
	public void testGetByType_enum () {
		assertEquals("Class enum", Type.ENUM, Type.getByClazz(Color.GREEN.getClass()));
	}

	@Test
	public void testGetByType_Enum () {
		assertEquals("Class Enum", Type.ENUM, Type.getByClazz(Enum.class));
	}

	// wrapper types
	@Test
	public void testGetByType_Byte () {
		assertEquals("Class Byte", Type.BYTE, Type.getByClazz(Byte.class));
	}

	@Test
	public void testGetByType_Short () {
		assertEquals("Class Short", Type.SHORT, Type.getByClazz(Short.class));
	}

	@Test
	public void testGetByType_Integer () {
		assertEquals("Class Integer", Type.INTEGER, Type.getByClazz(Integer.class));
	}

	@Test
	public void testGetByType_Long () {
		assertEquals("Class Long", Type.LONG, Type.getByClazz(Long.class));
	}

	@Test
	public void testGetByType_Float () {
		assertEquals("Class Float", Type.FLOAT, Type.getByClazz(Float.class));
	}

	@Test
	public void testGetByType_Double () {
		assertEquals("Class Double", Type.DOUBLE, Type.getByClazz(Double.class));
	}

	@Test
	public void testGetByType_Boolean () {
		assertEquals("Class Boolean", Type.BOOLEAN, Type.getByClazz(Boolean.class));
	}

	@Test
	public void testGetByType_Character () {
		assertEquals("Class Character", Type.CHARACTER, Type.getByClazz(Character.class));
	}

	// arrays of primitve values
	@Test
	public void testGetByType_byteArray () {
		assertEquals("Class byteArray", Type.BYTE_ARRAY, Type.getByClazz(byte[].class));
	}

	@Test
	public void testGetByType_shortArray () {
		assertEquals("Class shortArray", Type.SHORT_ARRAY, Type.getByClazz(short[].class));
	}

	@Test
	public void testGetByType_intArray () {
		assertEquals("Class intArray", Type.INT_ARRAY, Type.getByClazz(int[].class));
	}

	@Test
	public void testGetByType_longArray () {
		assertEquals("Class longArray", Type.LONG_ARRAY, Type.getByClazz(long[].class));
	}

	@Test
	public void testGetByType_floatArray () {
		assertEquals("Class floatArray", Type.FLOAT_ARRAY, Type.getByClazz(float[].class));
	}

	@Test
	public void testGetByType_doubleArray () {
		assertEquals("Class doubleArray", Type.DOUBLE_ARRAY, Type.getByClazz(double[].class));
	}

	@Test
	public void testGetByType_booleanArray () {
		assertEquals("Class booleanArray", Type.BOOLEAN_ARRAY, Type.getByClazz(boolean[].class));
	}

	@Test
	public void testGetByType_charArray () {
		assertEquals("Class charArray", Type.CHAR_ARRAY, Type.getByClazz(char[].class));
	}

	// other object types
	@Test
	public void testGetByType_Object () {
		assertEquals("Class Object", Type.OBJECT, Type.getByClazz(Object.class));
	}

	@Test
	public void testGetByType_String () {
		assertEquals("Class String", Type.STRING, Type.getByClazz(String.class));
	}

	@Test
	public void testGetByType_Date () {
		assertEquals("Class Date", Type.DATE, Type.getByClazz(Date.class));
	}

	@Test
	public void testGetByType_Timestamp () {
		assertEquals("Class Timestamp", Type.TIMESTAMP, Type.getByClazz(Timestamp.class));
	}

	@Test
	public void testGetByType_Collection () {
		assertEquals("Class Collection, List", Type.COLLECTION, Type.getByClazz(List.class));
		assertEquals("Class Collection, Set", Type.COLLECTION, Type.getByClazz(Set.class));
		assertEquals("Class Collection, Queue", Type.COLLECTION, Type.getByClazz(Queue.class));
		assertEquals("Class Collection, SortedSet", Type.COLLECTION, Type.getByClazz(SortedSet.class));
		assertEquals("Class Collection, Deque", Type.COLLECTION, Type.getByClazz(Deque.class));
	}

	@Test
	public void testGetByType_Array () {
		assertEquals("Class Array", Type.ARRAY, Type.getByClazz(Object[].class));
	}

	@Test
	public void testGetByType_Map () {
		assertEquals("Class Map", Type.MAP, Type.getByClazz(Map.class));
	}

	@Test
	public void testGetByType_HashMap () {
		assertEquals("Class HashMap", Type.MAP, Type.getByClazz(HashMap.class));
	}

	// value checks
	@Test
	public void testGetByValue_ByNullValue () {
		assertEquals("Null value getter", Type.NULL, Type.getByValue(null));
	}

	@Test
	public void testGetByValue_ByByteValue () {
		assertEquals("byte value getter", Type.BYTE, Type.getByValue((byte)127));
		assertEquals("Byte value getter", Type.BYTE, Type.getByValue(Byte.valueOf((byte)127)));
	}

	@Test
	public void testGetByValue_ByShortValue () {
		assertEquals("short value getter", Type.SHORT, Type.getByValue((short)5620));
		assertEquals("Short value getter", Type.SHORT, Type.getByValue(Short.valueOf((short)5243)));
	}

	@Test
	public void testGetByValue_ByIntValue () {
		assertEquals("int value getter", Type.INTEGER, Type.getByValue(18000000));
		assertEquals("Integer value getter", Type.INTEGER, Type.getByValue(Integer.valueOf(520)));
	}

	@Test
	public void testGetByValue_ByLongValue () {
		assertEquals("long value getter", Type.LONG, Type.getByValue(4528L));
		assertEquals("Long value getter", Type.LONG, Type.getByValue(Long.valueOf(123L)));
	}

	@Test
	public void testGetByValue_ByFloatValue () {
		assertEquals("float value getter", Type.FLOAT, Type.getByValue(1.235F));
		assertEquals("Float value getter", Type.FLOAT, Type.getByValue(1.235F));
	}

	@Test
	public void testGetByValue_ByDoubleValue () {
		assertEquals("double value getter", Type.DOUBLE, Type.getByValue(25.23654D));
		assertEquals("Double value getter", Type.DOUBLE, Type.getByValue(Double.valueOf(25.23654D)));
	}

	@Test
	public void testGetByValue_ByBooleanValue () {
		assertEquals("boolean value getter", Type.BOOLEAN, Type.getByValue(true));
		assertEquals("boolean value getter", Type.BOOLEAN, Type.getByValue(false));
		assertEquals("Boolean value getter", Type.BOOLEAN, Type.getByValue(Boolean.TRUE));
		assertEquals("Boolean value getter", Type.BOOLEAN, Type.getByValue(Boolean.FALSE));
	}

	@Test
	public void testGetByValue_ByCharValue () {
		assertEquals("char value getter", Type.CHARACTER, Type.getByValue('a'));
		assertEquals("Character value getter", Type.CHARACTER, Type.getByValue(Character.valueOf('a')));
	}

	@Test
	public void testGetByValue_ByObjectValue () {
		assertEquals("Object value getter", Type.OBJECT, Type.getByValue(new Object()));
	}

	@Test
	public void testGetByValue_ByStringValue () {
		assertEquals("String value getter", Type.STRING, Type.getByValue(""));
		assertEquals("String value getter", Type.STRING, Type.getByValue("Hello world!"));
	}

	@Test
	public void testGetByValue_ByDateValue () {
		assertEquals("Date value getter", Type.DATE, Type.getByValue(new Date()));
	}

	@Test
	public void testGetByValue_ByTimestampValue () {
		assertEquals("Timestamp value getter", Type.TIMESTAMP, Type.getByValue(Timestamp.from(Instant.now())));
	}

	@Test
	public void testGetByValue_ByByteArrayValue () {
		assertEquals("byte array value getter", Type.BYTE_ARRAY, Type.getByValue(new byte[] { 2, 5, 4, -5 }));
	}

	@Test
	public void testGetByValue_ByShortArrayValue () {
		assertEquals("short array value getter", Type.SHORT_ARRAY, Type.getByValue(new short[] { 2, 5, 4, -5 }));
	}

	@Test
	public void testGetByValue_ByIntArrayValue () {
		assertEquals("int array value getter", Type.INT_ARRAY, Type.getByValue(new int[] { 2, 5, 4, -5 }));
	}

	@Test
	public void testGetByValue_ByLongArrayValue () {
		assertEquals("long array value getter", Type.LONG_ARRAY, Type.getByValue(new long[] { 2, 5, 4, -5 }));
	}

	@Test
	public void testGetByValue_ByFloatArrayValue () {
		assertEquals("float array value getter", Type.FLOAT_ARRAY, Type.getByValue(new float[] {	2.0F, 5.0F, 4.0F,
																									-5.0F }));
	}

	@Test
	public void testGetByValue_ByDoubleArrayValue () {
		assertEquals("double array value getter", Type.DOUBLE_ARRAY, Type.getByValue(new double[] {	2.0, 5.0, 4.0,
																									-5.0 }));
	}

	@Test
	public void testGetByValue_ByBooleanArrayValue () {
		assertEquals("boolean array value getter", Type.BOOLEAN_ARRAY, Type.getByValue(new boolean[] { true, false }));
	}

	@Test
	public void testGetByValue_ByCharArrayValue () {
		assertEquals("char array value getter", Type.CHAR_ARRAY, Type.getByValue(new char[] { 'a', 'b' }));
	}

	@Test
	public void testGetByValue_ByByteArrayWrapperValue () {
		assertEquals("byte wrapper array value getter", Type.ARRAY, Type.getByValue(new Byte[] { 2, 5, 4, -5 }));
	}

	@Test
	public void testGetByValue_ByShortArrayWrapperValue () {
		assertEquals("short wrapper array value getter", Type.ARRAY, Type.getByValue(new Short[] { 2, 5, 4, -5 }));
	}

	@Test
	public void testGetByValue_ByIntArrayWrapperValue () {
		assertEquals("int wrapper array value getter", Type.ARRAY, Type.getByValue(new Integer[] { 2, 5, 4, -5 }));
	}

	@Test
	public void testGetByValue_ByLongArrayWrapperValue () {
		assertEquals("long wrapper array value getter", Type.ARRAY, Type.getByValue(new Long[] { 2L, 5L, -2L }));
	}

	@Test
	public void testGetByValue_ByFloatArrayWrapperValue () {
		assertEquals("float wrapper array value getter", Type.ARRAY, Type.getByValue(new Float[] {	2.0F, 5.0F, 4.0F,
																									-5.0F }));
	}

	@Test
	public void testGetByValue_ByDoubleArrayWrapperValue () {
		assertEquals("double wrapper array value getter", Type.ARRAY, Type.getByValue(new Double[] {	2.0, 5.0, 4.0,
																										-5.0 }));
	}

	@Test
	public void testGetByValue_ByBooleanArrayWrapperValue () {
		assertEquals("boolean wrapper array value getter", Type.ARRAY, Type.getByValue(new Boolean[] {	Boolean.TRUE,
																										Boolean.FALSE }));
	}

	@Test
	public void testGetByValue_ByCharArrayWrapperValue () {
		assertEquals("char wrapper array value getter", Type.ARRAY, Type.getByValue(new Character[] { 'a', 'b' }));
	}

	@Test
	public void testGetByValue_ByMapValue () {
		Map<Integer, String> aMap = new HashMap<>();
		aMap.put(2, "Hello world!");
		assertEquals("map value getter", Type.MAP, Type.getByValue(aMap));
	}
}