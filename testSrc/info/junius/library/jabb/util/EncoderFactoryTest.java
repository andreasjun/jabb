// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import static org.junit.Assert.assertEquals;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import info.junius.library.jabb.encoder.BooleanArrayEncoder;
import info.junius.library.jabb.encoder.BooleanEncoder;
import info.junius.library.jabb.encoder.ByteArrayEncoder;
import info.junius.library.jabb.encoder.ByteEncoder;
import info.junius.library.jabb.encoder.CharArrayEncoder;
import info.junius.library.jabb.encoder.CharEncoder;
import info.junius.library.jabb.encoder.DateEncoder;
import info.junius.library.jabb.encoder.DoubleArrayEncoder;
import info.junius.library.jabb.encoder.DoubleEncoder;
import info.junius.library.jabb.encoder.FloatArrayEncoder;
import info.junius.library.jabb.encoder.FloatEncoder;
import info.junius.library.jabb.encoder.IntArrayEncoder;
import info.junius.library.jabb.encoder.IntEncoder;
import info.junius.library.jabb.encoder.LongArrayEncoder;
import info.junius.library.jabb.encoder.LongEncoder;
import info.junius.library.jabb.encoder.NullEncoder;
import info.junius.library.jabb.encoder.ShortArrayEncoder;
import info.junius.library.jabb.encoder.ShortEncoder;
import info.junius.library.jabb.encoder.StringEncoder;
import info.junius.library.jabb.encoder.TimestampEncoder;
import org.junit.Test;

public class EncoderFactoryTest {

	private EncoderFactory factory = new EncoderFactory();

	@Test
	public void testFactory_0 () {
		assertEquals("NullEncoder", NullEncoder.class, this.factory.getEncoder(null).getClass());
	}

	@Test
	public void testFactory_1 () {
		assertEquals("ByteEncoder", ByteEncoder.class, this.factory.getEncoder((byte)1).getClass());
	}

	@Test
	public void testFactory_2 () {
		assertEquals("ShortEncoder", ShortEncoder.class, this.factory.getEncoder((short)1).getClass());
	}

	@Test
	public void testFactory_3 () {
		assertEquals("IntEncoder", IntEncoder.class, this.factory.getEncoder(1).getClass());
	}

	@Test
	public void testFactory_4 () {
		assertEquals("LongEncoder", LongEncoder.class, this.factory.getEncoder(1L).getClass());
	}

	@Test
	public void testFactory_5 () {
		assertEquals("FloatEncoder", FloatEncoder.class, this.factory.getEncoder(1.0F).getClass());
	}

	@Test
	public void testFactory_6 () {
		assertEquals("DoubleEncoder", DoubleEncoder.class, this.factory.getEncoder(1.0D).getClass());
	}

	@Test
	public void testFactory_7 () {
		assertEquals("BooleanEncoder", BooleanEncoder.class, this.factory.getEncoder(true).getClass());
	}

	@Test
	public void testFactory_8 () {
		assertEquals("CharEncoder", CharEncoder.class, this.factory.getEncoder('a').getClass());
	}

	@Test
	public void testFactory_9 () {
		assertEquals("StringEncoder", StringEncoder.class, this.factory.getEncoder("Hello").getClass());
	}

	@Test
	public void testFactory_10 () {
		assertEquals("DateEncoder", DateEncoder.class, this.factory.getEncoder(new Date()).getClass());
	}

	@Test
	public void testFactory_11 () {
		assertEquals("TimestampEncoder", TimestampEncoder.class, this.factory.getEncoder(new Timestamp(new Date().getTime())).getClass());
	}

	@Test
	public void testFactory_12 () {
		assertEquals("ByteArrayEncoder", ByteArrayEncoder.class, this.factory.getEncoder(new byte[] {	1, 2,
																										3 }).getClass());
	}

	@Test
	public void testFactory_13 () {
		assertEquals("ShortArrayEncoder", ShortArrayEncoder.class, this.factory.getEncoder(new short[] {	1, 2,
																											3 }).getClass());
	}

	@Test
	public void testFactory_14 () {
		assertEquals("IntArrayEncoder", IntArrayEncoder.class, this.factory.getEncoder(new int[] {	1, 2,
																									3 }).getClass());
	}

	@Test
	public void testFactory_15 () {
		assertEquals("LongArrayEncoder", LongArrayEncoder.class, this.factory.getEncoder(new long[] {	1, 2,
																										3 }).getClass());
	}

	@Test
	public void testFactory_16 () {
		assertEquals("FloatArrayEncoder", FloatArrayEncoder.class, this.factory.getEncoder(new float[] {	(float)1.0,
																											(float)2.0,
																											(float)3.0 }).getClass());
	}

	@Test
	public void testFactory_17 () {
		assertEquals("DoubleArrayEncoder", DoubleArrayEncoder.class, this.factory.getEncoder(new double[] {	1.0, 2.0,
																											3.0 }).getClass());
	}

	@Test
	public void testFactory_18 () {
		assertEquals("BooleanArrayEncoder", BooleanArrayEncoder.class, this.factory.getEncoder(new boolean[] {	true,
																												false,
																												true }).getClass());
	}

	@Test
	public void testFactory_19 () {
		assertEquals("CharArrayEncoder", CharArrayEncoder.class, this.factory.getEncoder(new char[] {	'a', 'b',
																										'c' }).getClass());
	}

	@Test
	public void testFactory_20 () {
		assertEquals("ObjectEncoder", null, this.factory.getEncoder(new Object()));
	}

	@Test
	public void testFactory_21 () {
		assertEquals("collection", null, this.factory.getEncoder(new ArrayList<Integer>()));
	}

	@Test
	public void testFactory_22 () {
		assertEquals("object array", null, this.factory.getEncoder(new Object[] { new Object(), new Object() }));
	}

	@Test
	public void testFactory_23 () {
		assertEquals("map", null, this.factory.getEncoder(new HashMap<Integer, String>()));
	}
}