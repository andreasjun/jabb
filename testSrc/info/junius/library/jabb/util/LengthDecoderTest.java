// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import static org.junit.Assert.assertEquals;
import java.io.IOException;
import org.junit.Test;

/**
 * The length decoder decodes byte arrays from 0 to nine elements to length values given as a long.
 * Created: 10 Aug 2015, 9:39:52 am
 * 
 * @author Andreas Junius
 */
public class LengthDecoderTest {

	/** decoder under test */
	private LengthDecoder	decoder			= new LengthDecoder();

	// input
	private byte[]			nullByte		= null;																									// 0

	private byte[]			emptyByte		= new byte[0];																						// 0

	private byte[]			minByte			= new byte[] { 0 };																			// 0

	private byte[]			maxByte			= new byte[] { Byte.MAX_VALUE };												// 127

	private byte[]			minShort		= new byte[] { -126, 0, -128 };													// 128

	private byte[]			maxShort		= new byte[] { -126, 127, -1 };													// 32767

	private byte[]			minInt			= new byte[] { -124, 0, 0, -128, 0 };										// 32768

	private byte[]			maxInt			= new byte[] { -124, 127, -1, -1, -1 };									// 2147483647

	private byte[]			minLong			= new byte[] { -120, 0, 0, 0, 0, -128, 0, 0, 0 };				// 2147483648

	private byte[]			maxLong			= new byte[] { -120, 127, -1, -1, -1, -1, -1, -1, -1 };		// 9223372036854775807

	// values with unconventional length
	// 1, 3, 5, 9 --> conventional, byte, short, int, long
	// 2, 4, 6, 7, 8 --> unconventional, byte, int. long, long, long
	private byte[]			twoDigits		= new byte[] { -127, 127 };															// 127

	private byte[]			fourDigits		= new byte[] { -125, 127, -1, -1 };											// 8388607

	private byte[]			sixDigits		= new byte[] { -123, 127, -1, -1, -1, -1 };							// 549755813887

	private byte[]			sevenDigits		= new byte[] { -122, 127, -1, -1, -1, -1, -1 };					// 140737488355327

	private byte[]			eightDigits		= new byte[] { -121, 127, -1, -1, -1, -1, -1, -1 };			// 36028797018963967

	// expected results length
	private byte			length_zero		= 0;

	private byte			length_one		= 1;

	private byte			length_three	= 3;

	private byte			length_five		= 5;

	private byte			length_nine		= 9;

	private byte			length_two		= 2;

	private byte			length_four		= 4;

	private byte			length_six		= 6;

	private byte			length_seven	= 7;

	private byte			length_eight	= 8;

	@Test
	public void testDecode_null () throws IOException {
		byte length = this.decoder.getLengthLength(this.nullByte);
		long value = this.decoder.decode(this.nullByte);
		assertEquals("length", this.length_zero, length);
		assertEquals("value", 0, value);
	}

	@Test
	public void testDecode_empty () throws IOException {
		byte length = this.decoder.getLengthLength(this.emptyByte);
		long value = this.decoder.decode(this.emptyByte);
		assertEquals("length", this.length_zero, length);
		assertEquals("value", 0, value);
	}

	@Test
	public void testDecode_minByte () throws IOException {
		byte length = this.decoder.getLengthLength(this.minByte);
		long value = this.decoder.decode(this.minByte);
		assertEquals("length", this.length_one, length);
		assertEquals("value", 0, value);
	}

	@Test
	public void testDecode_maxByte () throws IOException {
		byte length = this.decoder.getLengthLength(this.maxByte);
		long value = this.decoder.decode(this.maxByte);
		assertEquals("length", this.length_one, length);
		assertEquals("value", 127, value);
	}

	@Test
	public void testDecode_minShort () throws IOException {
		byte length = this.decoder.getLengthLength(this.minShort);
		long value = this.decoder.decode(this.minShort);
		assertEquals("length", this.length_three, length);
		assertEquals("value", 128, value);
	}

	@Test
	public void testDecode_maxShort () throws IOException {
		byte length = this.decoder.getLengthLength(this.maxShort);
		long value = this.decoder.decode(this.maxShort);
		assertEquals("length", this.length_three, length);
		assertEquals("value", 32767, value);
	}

	@Test
	public void testDecode_minInt () throws IOException {
		byte length = this.decoder.getLengthLength(this.minInt);
		long value = this.decoder.decode(this.minInt);
		assertEquals("length", this.length_five, length);
		assertEquals("value", 32768, value);
	}

	@Test
	public void testDecode_maxInt () throws IOException {
		byte length = this.decoder.getLengthLength(this.maxInt);
		long value = this.decoder.decode(this.maxInt);
		assertEquals("length", this.length_five, length);
		assertEquals("value", 2147483647, value);
	}

	@Test
	public void testDecode_minLong () throws IOException {
		byte length = this.decoder.getLengthLength(this.minLong);
		long value = this.decoder.decode(this.minLong);
		assertEquals("length", this.length_nine, length);
		assertEquals("value", 2147483648L, value);
	}

	@Test
	public void testDecode_maxLong () throws IOException {
		byte length = this.decoder.getLengthLength(this.maxLong);
		long value = this.decoder.decode(this.maxLong);
		assertEquals("length", this.length_nine, length);
		assertEquals("value", 9223372036854775807L, value);
	}

	@Test
	public void testDecode_twoDigits () throws IOException {
		byte length = this.decoder.getLengthLength(this.twoDigits);
		long value = this.decoder.decode(this.twoDigits);
		assertEquals("length", this.length_two, length);
		assertEquals("value", 127, value);
	}

	@Test
	public void testDecode_fourDigits () throws IOException {
		byte length = this.decoder.getLengthLength(this.fourDigits);
		long value = this.decoder.decode(this.fourDigits);
		assertEquals("length", this.length_four, length);
		assertEquals("value", 8388607, value);
	}

	@Test
	public void testDecode_sixDigits () throws IOException {
		byte length = this.decoder.getLengthLength(this.sixDigits);
		long value = this.decoder.decode(this.sixDigits);
		assertEquals("length", this.length_six, length);
		assertEquals("value", 549755813887L, value);
	}

	@Test
	public void testDecode_sevenDigits () throws IOException {
		byte length = this.decoder.getLengthLength(this.sevenDigits);
		long value = this.decoder.decode(this.sevenDigits);
		assertEquals("length", this.length_seven, length);
		assertEquals("value", 140737488355327L, value);
	}

	@Test
	public void testDecode_eightDigits () throws IOException {
		byte length = this.decoder.getLengthLength(this.eightDigits);
		long value = this.decoder.decode(this.eightDigits);
		assertEquals("length", this.length_eight, length);
		assertEquals("value", 36028797018963967L, value);
	}
}