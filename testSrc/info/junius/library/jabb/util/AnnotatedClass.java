// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import info.junius.library.jabb.MemberID;
import info.junius.library.jabb.ObjectID;

/**
 * Used by AnnotationReaderTest.
 * Created: 6 Aug 2015, 11:28:49 am
 * 
 * @author Andreas Junius
 */
@ObjectID ( 55 )
public class AnnotatedClass {

	private byte	aByte		= 0;

	private int		anInt		= 0;

	private boolean	aBool		= false;

	private Boolean	aBoolean	= null;

	private Boolean	bBoolean	= null;

	private String	aString		= null;

	private Object	anObject	= null;

	/**
	 * @return the aByte
	 */
	@MemberID ( 1 )
	public byte getaByte () {
		return this.aByte;
	}

	/**
	 * @param aByte the aByte to set
	 */
	public void setaByte ( byte aByte ) {
		this.aByte = aByte;
	}

	/**
	 * @return the anInt
	 */
	public int getAnInt () {
		return this.anInt;
	}

	/**
	 * @param anInt the anInt to set
	 */
	@MemberID ( 2 )
	public void setAnInt ( int anInt ) {
		this.anInt = anInt;
	}

	/**
	 * @return the aBool
	 */
	@MemberID ( 3 )
	public boolean isaBool () {
		return this.aBool;
	}

	/**
	 * @param aBool the aBool to set
	 */
	public void setaBool ( boolean aBool ) {
		this.aBool = aBool;
	}

	/**
	 * @return the aBoolean
	 */
	public Boolean getaBoolean () {
		return this.aBoolean;
	}

	/**
	 * @param aBoolean the aBoolean to set
	 */
	@MemberID ( 4 )
	public void setaBoolean ( Boolean aBoolean ) {
		this.aBoolean = aBoolean;
	}

	/**
	 * @return the aString
	 */
	@MemberID ( 5 )
	public String getaString () {
		return this.aString;
	}

	/**
	 * @param aString the aString to set
	 */
	public void setaString ( String aString ) {
		this.aString = aString;
	}

	/**
	 * @return the anObject
	 */
	public Object getAnObject () {
		return this.anObject;
	}

	/**
	 * @param anObject the anObject to set
	 */
	@MemberID ( 6 )
	public void setAnObject ( Object anObject ) {
		this.anObject = anObject;
	}

	/**
	 * This might not following the convention
	 * 
	 * @return the bBoolean
	 */
	public Boolean isbBoolean () {
		return this.bBoolean;
	}

	/**
	 * @param bBoolean the bBoolean to set
	 */
	@MemberID ( 7 )
	public void setbBoolean ( Boolean bBoolean ) {
		this.bBoolean = bBoolean;
	}
}