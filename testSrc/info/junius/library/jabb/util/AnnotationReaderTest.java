// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import static org.junit.Assert.*;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import info.junius.library.jabb.MemberID;
import org.junit.Test;

/**
 * Uses AnnotatedClass to test the AnnotationReader.
 * Created: 6 Aug 2015, 11:28:24 am
 * 
 * @author Andreas Junius
 */
public class AnnotationReaderTest {

	private static final int	NUMBEROFMETHODS		= 7;

	private AnnotationReader	annotationReader	= new AnnotationReader();

	private AnnotatedClass		annotatedClass		= new AnnotatedClass();

	/**
	 * Test method for {@link info.junius.library.jabb.util.AnnotationReader#getGetter(java.lang.Object)}.
	 */
	@Test
	public void testGetter () {
		Map<Method, MemberID> getters = this.annotationReader.getGetter(this.annotatedClass);
		Set<Byte> memberIds = new HashSet<>();
		for (MemberID memberId : getters.values()) {
			memberIds.add(memberId.value());
		}
		assertEquals("number of methods", NUMBEROFMETHODS, getters.size());
		assertEquals("number of distinct member ids", NUMBEROFMETHODS, memberIds.size());
		for (Method method : getters.keySet()) {
			if (!method.getName().startsWith("get") && !method.getName().startsWith("is")) {
				fail("getter method does not start with either get or is");
			}
		}
	}

	/**
	 * Test method for {@link info.junius.library.jabb.util.AnnotationReader#getSetter(java.lang.Object)}.
	 */
	@Test
	public void testSetter () {
		Map<Method, MemberID> setters = this.annotationReader.getSetter(this.annotatedClass);
		Set<Byte> memberIds = new HashSet<>();
		for (MemberID memberId : setters.values()) {
			memberIds.add(memberId.value());
		}
		assertEquals("number of methods", NUMBEROFMETHODS, setters.size());
		assertEquals("number of distinct member ids", NUMBEROFMETHODS, memberIds.size());
		for (Method method : setters.keySet()) {
			if (!method.getName().startsWith("set")) {
				fail("setter method does not start with set");
			}
		}
	}
}