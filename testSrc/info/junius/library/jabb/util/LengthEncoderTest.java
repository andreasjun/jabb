// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb.util;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;
import java.io.IOException;
import org.junit.Test;

/**
 * Tests the length encoder
 * Created: 10 Aug 2015, 9:40:09 am
 * 
 * @author Andreas Junius
 */
public class LengthEncoderTest {

	private LengthEncoder encoder = new LengthEncoder();

	// no negative length values
	@Test ( expected = java.io.IOException.class )
	public void testEncodeNegativeValue () throws IOException {
		this.encoder.encode(-1);
	}

	// 0 length
	@Test
	public void testEncodeBoundary_0 () {
		byte value = 0;
		byte[] expected = new byte[] { 0 };
		try {
			byte[] result = this.encoder.encode(value);
			assertArrayEquals("value 0", expected, result);
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	// max byte value
	@Test
	public void testEncodeBoundary_127 () {
		byte value = Byte.MAX_VALUE;
		byte[] expected = new byte[] { Byte.MAX_VALUE };
		try {
			byte[] result = this.encoder.encode(value);
			assertArrayEquals("value 127", expected, result);
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	// min short value
	@Test
	public void testEncodeBoundary_128 () {
		short value = Byte.MAX_VALUE + 1;
		byte[] expected = new byte[] { -126, 0, -128 };
		try {
			byte[] result = this.encoder.encode(value);
			assertArrayEquals("value 128", expected, result);
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	// max short value
	@Test
	public void testEncodeBoundary_32767 () {
		short value = Short.MAX_VALUE;
		byte[] expected = new byte[] { -126, 127, -1 };
		try {
			byte[] result = this.encoder.encode(value);
			assertArrayEquals("value 32767", expected, result);
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	// min int value
	@Test
	public void testEncodeBoundary_32768 () {
		int value = Short.MAX_VALUE + 1;
		byte[] expected = new byte[] { -124, 0, 0, -128, 0 };
		try {
			byte[] result = this.encoder.encode(value);
			assertArrayEquals("value 32768", expected, result);
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	// max integer value
	@Test
	public void testEncodeBoundary_2147483647 () {
		int value = Integer.MAX_VALUE;
		byte[] expected = new byte[] { -124, 127, -1, -1, -1 };
		try {
			byte[] result = this.encoder.encode(value);
			assertArrayEquals("value 2147483647", expected, result);
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	// max integer value
	@Test
	public void testEncodeBoundary_2147483648 () {
		long value = Integer.MAX_VALUE + 1L;
		byte[] expected = new byte[] { -120, 0, 0, 0, 0, -128, 0, 0, 0 };
		try {
			byte[] result = this.encoder.encode(value);
			assertArrayEquals("value 2147483648", expected, result);
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	// max long value
	@Test
	public void testEncodeBoundary_9223372036854775807 () {
		long value = Long.MAX_VALUE;
		byte[] expected = new byte[] { -120, 127, -1, -1, -1, -1, -1, -1, -1 };
		try {
			byte[] result = this.encoder.encode(value);
			assertArrayEquals("byte value 9223372036854775807", expected, result);
		} catch (IOException e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
}