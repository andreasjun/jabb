// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

import static org.junit.Assert.assertEquals;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import org.junit.Test;
import info.junius.library.jabb.EnumTypes.Color;
import info.junius.library.jabb.constants.MapEntryId;
import info.junius.library.jabb.constants.Type;
import info.junius.library.jabb.dt.RawBinary;
import info.junius.library.jabb.impl.DefaultObjectEncoder;

/**
 * Created: 15 Aug 2015, 2:36:54 pm
 * 
 * @author Andreas Junius
 */
public class ObjectEncoderTest {

	/**
	 * Empty JABB object
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testEmptyElement () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		StringArgTypes object = new StringArgTypes();
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		expected.setType(Type.OBJECT.getFlag());
		expected.setId((byte)12);
		expected.setValue(null);
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * Single annotated element
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testElement () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Element object = new Element();
		object.setValue0("Hello");
		object.setValue1(12345);
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		RawBinary child0 = new RawBinary();
		RawBinary child1 = new RawBinary();
		expected.addChild(child0);
		expected.addChild(child1);
		expected.setType(Type.OBJECT.getFlag());
		child0.setType(Type.STRING.getFlag());
		child1.setType(Type.INTEGER.getFlag());
		expected.setId((byte)-1);
		child0.setId((byte)0);
		child1.setId((byte)1);
		child0.setValue(new byte[] { 72, 101, 108, 108, 111 });
		child1.setValue(new byte[] { 0, 0, 48, 57 });
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * String arg types
	 * 
	 * @throws URISyntaxException
	 * @throws MalformedURLException
	 * @throws ParseException
	 */
	@Test
	public void testStringArgTypes () throws URISyntaxException, MalformedURLException, ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		StringArgTypes object = new StringArgTypes();
		File file = new File("home/andy/test.txt");
		object.setFile(file);
		object.setUri(new URI("file:///home/andy/test.txt"));
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		RawBinary child0 = new RawBinary();
		RawBinary child1 = new RawBinary();
		expected.addChild(child0);
		expected.addChild(child1);
		expected.setType(Type.OBJECT.getFlag());
		child0.setType(Type.STRING.getFlag());
		child1.setType(Type.STRING.getFlag());
		expected.setId((byte)12);
		child0.setId((byte)1);
		child1.setId((byte)2);
		// @formatter:off
		// file representation depends on OS; can't be hard-coded
		byte[] fileRepresentation = file.toString().getBytes(StandardCharsets.UTF_8);
		child0.setValue(fileRepresentation);
		child1.setValue(new byte[] { 102, 105, 108, 101, 58, 47, 47, 47, 104, 111, 109, 101, 47, 97, 110, 100, 121, 47, 116, 101, 115, 116, 46, 116, 120, 116 });
		// @formatter:on 
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * Annotated object holding primitives
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testPrimitives () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Primitives object = new Primitives();
		object.setValue0((byte)1);
		object.setValue1((short)1);
		object.setValue2(1);
		object.setValue3(1L);
		object.setValue4(1.0F);
		object.setValue5(1.0D);
		object.setValue6((char)0);
		object.setValue7(true);
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		RawBinary child0 = new RawBinary();
		RawBinary child1 = new RawBinary();
		RawBinary child2 = new RawBinary();
		RawBinary child3 = new RawBinary();
		RawBinary child4 = new RawBinary();
		RawBinary child5 = new RawBinary();
		RawBinary child6 = new RawBinary();
		RawBinary child7 = new RawBinary();
		expected.addChild(child0);
		expected.addChild(child1);
		expected.addChild(child2);
		expected.addChild(child3);
		expected.addChild(child4);
		expected.addChild(child5);
		expected.addChild(child6);
		expected.addChild(child7);
		expected.setType(Type.OBJECT.getFlag());
		child0.setType(Type.BYTE.getFlag());
		child1.setType(Type.SHORT.getFlag());
		child2.setType(Type.INTEGER.getFlag());
		child3.setType(Type.LONG.getFlag());
		child4.setType(Type.FLOAT.getFlag());
		child5.setType(Type.DOUBLE.getFlag());
		child6.setType(Type.CHARACTER.getFlag());
		child7.setType(Type.BOOLEAN.getFlag());
		// set ids
		expected.setId((byte)11);
		child0.setId((byte)0);
		child1.setId((byte)1);
		child2.setId((byte)2);
		child3.setId((byte)3);
		child4.setId((byte)4);
		child5.setId((byte)5);
		child6.setId((byte)6);
		child7.setId((byte)7);
		// set values
		child0.setValue(new byte[] { 1 });
		child1.setValue(new byte[] { 0, 1 });
		child2.setValue(new byte[] { 0, 0, 0, 1 });
		child3.setValue(new byte[] { 0, 0, 0, 0, 0, 0, 0, 1 });
		child4.setValue(new byte[] { 63, -128, 0, 0 });
		child5.setValue(new byte[] { 63, -16, 0, 0, 0, 0, 0, 0 });
		child6.setValue(new byte[] { 0, 0 });
		child7.setValue(new byte[] { 127 });
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * Annotated object holding primitive wrappers
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testWrapper () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Wrapper object = new Wrapper();
		object.setValue0(Byte.valueOf((byte)1));
		object.setValue1(Short.valueOf((short)1));
		object.setValue2(Integer.valueOf(1));
		object.setValue3(Long.valueOf(1L));
		object.setValue4(Float.valueOf(1.0F));
		object.setValue5(Double.valueOf(1.0D));
		object.setValue6(Character.valueOf((char)0));
		object.setValue7(Boolean.TRUE);
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		RawBinary child0 = new RawBinary();
		RawBinary child1 = new RawBinary();
		RawBinary child2 = new RawBinary();
		RawBinary child3 = new RawBinary();
		RawBinary child4 = new RawBinary();
		RawBinary child5 = new RawBinary();
		RawBinary child6 = new RawBinary();
		RawBinary child7 = new RawBinary();
		expected.addChild(child0);
		expected.addChild(child1);
		expected.addChild(child2);
		expected.addChild(child3);
		expected.addChild(child4);
		expected.addChild(child5);
		expected.addChild(child6);
		expected.addChild(child7);
		expected.setType(Type.OBJECT.getFlag());
		child0.setType(Type.BYTE.getFlag());
		child1.setType(Type.SHORT.getFlag());
		child2.setType(Type.INTEGER.getFlag());
		child3.setType(Type.LONG.getFlag());
		child4.setType(Type.FLOAT.getFlag());
		child5.setType(Type.DOUBLE.getFlag());
		child6.setType(Type.CHARACTER.getFlag());
		child7.setType(Type.BOOLEAN.getFlag());
		// set ids
		expected.setId((byte)17);
		child0.setId((byte)0);
		child1.setId((byte)1);
		child2.setId((byte)2);
		child3.setId((byte)3);
		child4.setId((byte)4);
		child5.setId((byte)5);
		child6.setId((byte)6);
		child7.setId((byte)7);
		// set values
		child0.setValue(new byte[] { 1 });
		child1.setValue(new byte[] { 0, 1 });
		child2.setValue(new byte[] { 0, 0, 0, 1 });
		child3.setValue(new byte[] { 0, 0, 0, 0, 0, 0, 0, 1 });
		child4.setValue(new byte[] { 63, -128, 0, 0 });
		child5.setValue(new byte[] { 63, -16, 0, 0, 0, 0, 0, 0 });
		child6.setValue(new byte[] { 0, 0 });
		child7.setValue(new byte[] { 127 });
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * Annotated object holding arrays of primitive values
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testPrimitiveArrays () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		PrimArrays object = new PrimArrays();
		object.setValue0(new byte[] { 1, 2 });
		object.setValue1(new short[] { 1, 2 });
		object.setValue2(new int[] { 1, 2 });
		object.setValue3(new long[] { 1L, 2L });
		object.setValue4(new float[] { 1.0F, 2.0F });
		object.setValue5(new double[] { 1.0, 2.0 });
		object.setValue6(new char[] { 'a', 'b' });
		object.setValue7(new boolean[] { true, false });
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		RawBinary child0 = new RawBinary();
		RawBinary child1 = new RawBinary();
		RawBinary child2 = new RawBinary();
		RawBinary child3 = new RawBinary();
		RawBinary child4 = new RawBinary();
		RawBinary child5 = new RawBinary();
		RawBinary child6 = new RawBinary();
		RawBinary child7 = new RawBinary();
		expected.addChild(child0);
		expected.addChild(child1);
		expected.addChild(child2);
		expected.addChild(child3);
		expected.addChild(child4);
		expected.addChild(child5);
		expected.addChild(child6);
		expected.addChild(child7);
		expected.setType(Type.OBJECT.getFlag());
		child0.setType(Type.BYTE_ARRAY.getFlag());
		child1.setType(Type.SHORT_ARRAY.getFlag());
		child2.setType(Type.INT_ARRAY.getFlag());
		child3.setType(Type.LONG_ARRAY.getFlag());
		child4.setType(Type.FLOAT_ARRAY.getFlag());
		child5.setType(Type.DOUBLE_ARRAY.getFlag());
		child6.setType(Type.CHAR_ARRAY.getFlag());
		child7.setType(Type.BOOLEAN_ARRAY.getFlag());
		// set ids
		expected.setId((byte)19);
		child0.setId((byte)0);
		child1.setId((byte)1);
		child2.setId((byte)2);
		child3.setId((byte)3);
		child4.setId((byte)4);
		child5.setId((byte)5);
		child6.setId((byte)6);
		child7.setId((byte)7);
		// set values
		child0.setValue(new byte[] { 1, 2 });
		child1.setValue(new byte[] { 0, 1, 0, 2 });
		child2.setValue(new byte[] { 0, 0, 0, 1, 0, 0, 0, 2 });
		child3.setValue(new byte[] { 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 2 });
		child4.setValue(new byte[] { 63, -128, 0, 0, 64, 0, 0, 0 });
		child5.setValue(new byte[] { 63, -16, 0, 0, 0, 0, 0, 0, 64, 0, 0, 0, 0, 0, 0, 0 });
		child6.setValue(new byte[] { 0, 97, 0, 98 });
		child7.setValue(new byte[] { 127, 0 });
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * Annotated object holding arrays of objects
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testObjectArrays () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		ObjArrays object = new ObjArrays();
		object.setValue0(new Byte[] { 1, 2 });
		object.setValue1(new Short[] { 1, 2 });
		object.setValue2(new Integer[] { 1, 2 });
		object.setValue3(new Long[] { 1L, 2L });
		object.setValue4(new Float[] { 1.0F, 2.0F });
		object.setValue5(new Double[] { 1.0, 2.0 });
		object.setValue6(new Character[] { 'a', 'b' });
		object.setValue7(new Boolean[] { Boolean.TRUE, Boolean.FALSE });
		object.setValue8(new Object[] { new Object(), new Object() });
		object.setValue9(new Element[] { new Element("one", 1), new Element("two", 2) });
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		// arrays
		RawBinary child0 = new RawBinary();
		RawBinary child1 = new RawBinary();
		RawBinary child2 = new RawBinary();
		RawBinary child3 = new RawBinary();
		RawBinary child4 = new RawBinary();
		RawBinary child5 = new RawBinary();
		RawBinary child6 = new RawBinary();
		RawBinary child7 = new RawBinary();
		RawBinary child8 = new RawBinary();
		RawBinary child9 = new RawBinary();
		// elements
		RawBinary elem0 = new RawBinary();
		RawBinary elem1 = new RawBinary();
		RawBinary elem2 = new RawBinary();
		RawBinary elem3 = new RawBinary();
		RawBinary elem4 = new RawBinary();
		RawBinary elem5 = new RawBinary();
		RawBinary elem6 = new RawBinary();
		RawBinary elem7 = new RawBinary();
		RawBinary elem8 = new RawBinary();
		RawBinary elem9 = new RawBinary();
		RawBinary elem10 = new RawBinary();
		RawBinary elem11 = new RawBinary();
		RawBinary elem12 = new RawBinary();
		RawBinary elem13 = new RawBinary();
		RawBinary elem14 = new RawBinary();
		RawBinary elem15 = new RawBinary();
		RawBinary elem16 = new RawBinary();
		RawBinary elem17 = new RawBinary();
		// element objects
		RawBinary elem18 = new RawBinary();
		RawBinary elem19 = new RawBinary();
		RawBinary elem20 = new RawBinary();
		RawBinary elem21 = new RawBinary();
		RawBinary elem22 = new RawBinary();
		RawBinary elem23 = new RawBinary();
		// add array nodes
		expected.addChild(child0);
		expected.addChild(child1);
		expected.addChild(child2);
		expected.addChild(child3);
		expected.addChild(child4);
		expected.addChild(child5);
		expected.addChild(child6);
		expected.addChild(child7);
		expected.addChild(child8);
		expected.addChild(child9);
		// add object nodes to array nodes
		child0.addChild(elem0);
		child0.addChild(elem1);
		child1.addChild(elem2);
		child1.addChild(elem3);
		child2.addChild(elem4);
		child2.addChild(elem5);
		child3.addChild(elem6);
		child3.addChild(elem7);
		child4.addChild(elem8);
		child4.addChild(elem9);
		child5.addChild(elem10);
		child5.addChild(elem11);
		child6.addChild(elem12);
		child6.addChild(elem13);
		child7.addChild(elem14);
		child7.addChild(elem15);
		child8.addChild(elem16);
		child8.addChild(elem17);
		child9.addChild(elem18);
		child9.addChild(elem21);
		// assemble element object types
		elem18.addChild(elem19);
		elem18.addChild(elem20);
		elem21.addChild(elem22);
		elem21.addChild(elem23);
		expected.setType(Type.OBJECT.getFlag());
		child0.setType(Type.ARRAY.getFlag());
		child1.setType(Type.ARRAY.getFlag());
		child2.setType(Type.ARRAY.getFlag());
		child3.setType(Type.ARRAY.getFlag());
		child4.setType(Type.ARRAY.getFlag());
		child5.setType(Type.ARRAY.getFlag());
		child6.setType(Type.ARRAY.getFlag());
		child7.setType(Type.ARRAY.getFlag());
		child8.setType(Type.ARRAY.getFlag());
		child9.setType(Type.ARRAY.getFlag());
		elem0.setType(Type.BYTE.getFlag());
		elem1.setType(Type.BYTE.getFlag());
		elem2.setType(Type.SHORT.getFlag());
		elem3.setType(Type.SHORT.getFlag());
		elem4.setType(Type.INTEGER.getFlag());
		elem5.setType(Type.INTEGER.getFlag());
		elem6.setType(Type.LONG.getFlag());
		elem7.setType(Type.LONG.getFlag());
		elem8.setType(Type.FLOAT.getFlag());
		elem9.setType(Type.FLOAT.getFlag());
		elem10.setType(Type.DOUBLE.getFlag());
		elem11.setType(Type.DOUBLE.getFlag());
		elem12.setType(Type.CHARACTER.getFlag());
		elem13.setType(Type.CHARACTER.getFlag());
		elem14.setType(Type.BOOLEAN.getFlag());
		elem15.setType(Type.BOOLEAN.getFlag());
		elem16.setType(Type.NULL.getFlag());
		elem17.setType(Type.NULL.getFlag());
		elem18.setType(Type.OBJECT.getFlag());
		elem21.setType(Type.OBJECT.getFlag());
		elem19.setType(Type.STRING.getFlag());
		elem20.setType(Type.INTEGER.getFlag());
		elem22.setType(Type.STRING.getFlag());
		elem23.setType(Type.INTEGER.getFlag());
		// set ids
		expected.setId((byte)13);
		child0.setId((byte)0);
		child1.setId((byte)1);
		child2.setId((byte)2);
		child3.setId((byte)3);
		child4.setId((byte)4);
		child5.setId((byte)5);
		child6.setId((byte)6);
		child7.setId((byte)7);
		child8.setId((byte)8);
		child9.setId((byte)9);
		// remaining ones all have id = 0, except for the complext types
		elem19.setId((byte)0);
		elem20.setId((byte)1);
		elem22.setId((byte)0);
		elem23.setId((byte)1);
		// set values
		elem0.setValue(new byte[] { 1 });
		elem1.setValue(new byte[] { 2 });
		elem2.setValue(new byte[] { 0, 1 });
		elem3.setValue(new byte[] { 0, 2 });
		elem4.setValue(new byte[] { 0, 0, 0, 1 });
		elem5.setValue(new byte[] { 0, 0, 0, 2 });
		elem6.setValue(new byte[] { 0, 0, 0, 0, 0, 0, 0, 1 });
		elem7.setValue(new byte[] { 0, 0, 0, 0, 0, 0, 0, 2 });
		elem8.setValue(new byte[] { 63, -128, 0, 0 });
		elem9.setValue(new byte[] { 64, 0, 0, 0 });
		elem10.setValue(new byte[] { 63, -16, 0, 0, 0, 0, 0, 0 });
		elem11.setValue(new byte[] { 64, 0, 0, 0, 0, 0, 0, 0 });
		elem12.setValue(new byte[] { 0, 97 });
		elem13.setValue(new byte[] { 0, 98 });
		elem14.setValue(new byte[] { 127 });
		elem15.setValue(new byte[] { 0 });
		elem16.setValue(new byte[] {});
		elem17.setValue(new byte[] {});
		elem19.setValue(new byte[] { 111, 110, 101 });
		elem20.setValue(new byte[] { 0, 0, 0, 1 });
		elem22.setValue(new byte[] { 116, 119, 111 });
		elem23.setValue(new byte[] { 0, 0, 0, 2 });
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * Annotated object holding collections
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testCollections () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		ObjCollections object = new ObjCollections();
		// different types of collections
		List<String> list = Arrays.asList(new String[] { "hello", "world" });
		Set<String> set = new HashSet<>();
		set.addAll(list);
		Queue<String> queue = new PriorityQueue<>(list);
		// add them
		object.setValue0(list);
		object.setValue1(set);
		object.setValue2(queue);
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		RawBinary child0 = new RawBinary();
		RawBinary child1 = new RawBinary();
		RawBinary child2 = new RawBinary();
		RawBinary listElement0 = new RawBinary();
		RawBinary listElement1 = new RawBinary();
		RawBinary setElement0 = new RawBinary();
		RawBinary setElement1 = new RawBinary();
		RawBinary queueElement0 = new RawBinary();
		RawBinary queueElement1 = new RawBinary();
		expected.addChild(child0);
		expected.addChild(child1);
		expected.addChild(child2);
		child0.addChild(listElement0);
		child0.addChild(listElement1);
		child1.addChild(setElement0);
		child1.addChild(setElement1);
		child2.addChild(queueElement0);
		child2.addChild(queueElement1);
		expected.setType(Type.OBJECT.getFlag());
		child0.setType(Type.COLLECTION.getFlag());
		child1.setType(Type.COLLECTION.getFlag());
		child2.setType(Type.COLLECTION.getFlag());
		listElement0.setType(Type.STRING.getFlag());
		listElement1.setType(Type.STRING.getFlag());
		setElement0.setType(Type.STRING.getFlag());
		setElement1.setType(Type.STRING.getFlag());
		queueElement0.setType(Type.STRING.getFlag());
		queueElement1.setType(Type.STRING.getFlag());
		expected.setId((byte)23);
		child0.setId((byte)0);
		child1.setId((byte)1);
		child2.setId((byte)2);
		listElement0.setValue(new byte[] { 104, 101, 108, 108, 111 });
		listElement1.setValue(new byte[] { 119, 111, 114, 108, 100 });
		setElement0.setValue(new byte[] { 104, 101, 108, 108, 111 });
		setElement1.setValue(new byte[] { 119, 111, 114, 108, 100 });
		queueElement0.setValue(new byte[] { 104, 101, 108, 108, 111 });
		queueElement1.setValue(new byte[] { 119, 111, 114, 108, 100 });
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * Annotated object holding maps
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testMaps () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Maps object = new Maps();
		// different types of maps
		Map<String, Long> value0 = new HashMap<>();
		// unknown object, treat as null value and type
		Map<Integer, Object> value1 = new HashMap<>();
		// known, annotated object
		Map<Integer, Element> value2 = new HashMap<>();
		value0.put("keyOne", 1L);
		value1.put(Integer.valueOf(1), new Object());
		value2.put(Integer.valueOf(1), new Element("hello", 1));
		// add them
		object.setValue0(value0);
		object.setValue1(value1);
		object.setValue2(value2);
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		// 3 maps
		RawBinary map0 = new RawBinary();
		RawBinary map1 = new RawBinary();
		RawBinary map2 = new RawBinary();
		// one entry for each map
		RawBinary entry0 = new RawBinary();
		RawBinary entry1 = new RawBinary();
		RawBinary entry2 = new RawBinary();
		RawBinary entry0key = new RawBinary();
		RawBinary entry0value = new RawBinary();
		RawBinary entry1key = new RawBinary();
		RawBinary entry1value = new RawBinary();
		RawBinary entry2key = new RawBinary();
		RawBinary entry2value = new RawBinary();
		RawBinary entry2value_el0 = new RawBinary();
		RawBinary entry2value_el1 = new RawBinary();
		expected.addChild(map0);
		expected.addChild(map1);
		expected.addChild(map2);
		map0.addChild(entry0);
		map1.addChild(entry1);
		map2.addChild(entry2);
		entry0.addChild(entry0key);
		entry0.addChild(entry0value);
		entry1.addChild(entry1key);
		entry1.addChild(entry1value);
		entry2.addChild(entry2key);
		entry2.addChild(entry2value);
		entry2value.addChild(entry2value_el0);
		entry2value.addChild(entry2value_el1);
		expected.setType(Type.OBJECT.getFlag());
		map0.setType(Type.MAP.getFlag());
		map1.setType(Type.MAP.getFlag());
		map2.setType(Type.MAP.getFlag());
		entry0key.setType(Type.STRING.getFlag());
		entry0value.setType(Type.LONG.getFlag());
		entry1key.setType(Type.INTEGER.getFlag());
		entry1value.setType(Type.NULL.getFlag());
		entry2key.setType(Type.INTEGER.getFlag());
		entry2value.setType(Type.OBJECT.getFlag());
		entry2value_el0.setType(Type.STRING.getFlag());
		entry2value_el1.setType(Type.INTEGER.getFlag());
		expected.setId((byte)27);
		map0.setId((byte)0);
		map1.setId((byte)1);
		map2.setId((byte)2);
		entry0key.setId(MapEntryId.KEY.getId());
		entry0value.setId(MapEntryId.VALUE.getId());
		entry1key.setId(MapEntryId.KEY.getId());
		entry1value.setId(MapEntryId.VALUE.getId());
		entry2key.setId(MapEntryId.KEY.getId());
		entry2value.setId(MapEntryId.VALUE.getId());
		entry2value_el0.setId((byte)0);
		entry2value_el1.setId((byte)1);
		entry0key.setValue(new byte[] { 107, 101, 121, 79, 110, 101 });
		entry0value.setValue(new byte[] { 0, 0, 0, 0, 0, 0, 0, 1 });
		entry1key.setValue(new byte[] { 0, 0, 0, 1 });
		entry1value.setValue(new byte[] {});
		entry2key.setValue(new byte[] { 0, 0, 0, 1 });
		entry2value.setValue(null);
		entry2value_el0.setValue(new byte[] { 104, 101, 108, 108, 111 });
		entry2value_el1.setValue(new byte[] { 0, 0, 0, 1 });
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * Annotated object holding a tree structure
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testTreeStructure () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		TreeNode a = new TreeNode();
		TreeNode b = new TreeNode();
		TreeNode c = new TreeNode();
		TreeNode d = new TreeNode();
		TreeNode e = new TreeNode();
		TreeNode f = new TreeNode();
		a.setChildren(new TreeNode[] { b, f });
		b.setChildren(new TreeNode[] { c, d });
		d.setChildren(new TreeNode[] { e });
		a.setValue(0);
		b.setValue(1);
		c.setValue(2);
		d.setValue(3);
		e.setValue(4);
		f.setValue(5);
		a.setParent(null);
		b.setParent(a);
		c.setParent(b);
		d.setParent(b);
		e.setParent(d);
		f.setParent(a);
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		RawBinary child_b = new RawBinary();
		RawBinary child_c = new RawBinary();
		RawBinary child_d = new RawBinary();
		RawBinary child_e = new RawBinary();
		RawBinary child_f = new RawBinary();
		RawBinary int_a = new RawBinary();
		RawBinary int_b = new RawBinary();
		RawBinary int_c = new RawBinary();
		RawBinary int_d = new RawBinary();
		RawBinary int_e = new RawBinary();
		RawBinary int_f = new RawBinary();
		RawBinary array_a = new RawBinary();
		RawBinary array_b = new RawBinary();
		RawBinary array_d = new RawBinary();
		expected.addChild(array_a);
		expected.addChild(int_a);
		child_b.addChild(array_b);
		child_b.addChild(int_b);
		child_c.addChild(int_c);
		child_d.addChild(array_d);
		child_d.addChild(int_d);
		child_e.addChild(int_e);
		child_f.addChild(int_f);
		array_a.addChild(child_b);
		array_a.addChild(child_f);
		array_b.addChild(child_c);
		array_b.addChild(child_d);
		array_d.addChild(child_e);
		expected.setType(Type.OBJECT.getFlag());
		child_b.setType(Type.OBJECT.getFlag());
		child_c.setType(Type.OBJECT.getFlag());
		child_d.setType(Type.OBJECT.getFlag());
		child_e.setType(Type.OBJECT.getFlag());
		child_f.setType(Type.OBJECT.getFlag());
		int_a.setType(Type.INTEGER.getFlag());
		int_b.setType(Type.INTEGER.getFlag());
		int_c.setType(Type.INTEGER.getFlag());
		int_d.setType(Type.INTEGER.getFlag());
		int_e.setType(Type.INTEGER.getFlag());
		int_f.setType(Type.INTEGER.getFlag());
		array_a.setType(Type.ARRAY.getFlag());
		array_b.setType(Type.ARRAY.getFlag());
		array_d.setType(Type.ARRAY.getFlag());
		expected.setId((byte)-2);
		array_a.setId((byte)1);
		array_b.setId((byte)1);
		array_d.setId((byte)1);
		int_a.setValue(new byte[] { 0, 0, 0, 0 });
		int_b.setValue(new byte[] { 0, 0, 0, 1 });
		int_c.setValue(new byte[] { 0, 0, 0, 2 });
		int_d.setValue(new byte[] { 0, 0, 0, 3 });
		int_e.setValue(new byte[] { 0, 0, 0, 4 });
		int_f.setValue(new byte[] { 0, 0, 0, 5 });
		// encode the object
		byte[] result = encoder.encode(a);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * Object implementing an interface and extending an abstract class
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testInheritance () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// build object
		TypeInterface object = new TypeImpl();
		object.setValue0("Hello");
		object.setValue1(42);
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		// 3 maps
		RawBinary element0 = new RawBinary();
		RawBinary element1 = new RawBinary();
		expected.addChild(element0);
		expected.addChild(element1);
		expected.setId((byte)-3);
		element0.setId((byte)0);
		element1.setId((byte)1);
		expected.setType(Type.OBJECT.getFlag());
		element0.setType(Type.STRING.getFlag());
		element1.setType(Type.INTEGER.getFlag());
		element0.setValue(new byte[] { 72, 101, 108, 108, 111 });
		element1.setValue(new byte[] { 0, 0, 0, 42 });
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	/**
	 * Object that has enum type members
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testEnumTypes () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// build object
		EnumTypes object = new EnumTypes();
		object.setColor(Color.GREEN);
		object.setBackground(Color.RED);
		object.setNix(null);
		// create a tree that matches what is expected as an outcome
		RawBinary expected = new RawBinary();
		// 3 maps
		RawBinary element0 = new RawBinary();
		RawBinary element1 = new RawBinary();
		expected.addChild(element0);
		expected.addChild(element1);
		expected.setId((byte)27);
		element0.setId((byte)0);
		element1.setId((byte)1);
		expected.setType(Type.OBJECT.getFlag());
		element0.setType(Type.ENUM.getFlag());
		element1.setType(Type.ENUM.getFlag());
		element0.setValue(new byte[] { 71, 82, 69, 69, 78 });
		element1.setValue(new byte[] { 82, 69, 68 });
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}

	// some special cases, here nested maps
	@Test
	public void testNestedMaps () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		NestedMaps object = new NestedMaps();
		// create the two maps
		Map<Integer, List<String>> value0 = new HashMap<>();
		Map<Integer, Map<Integer, String>> value1 = new HashMap<>();
		// put values into the first one (a list)
		value0.put(1, Arrays.asList("eins", "zwei", "drei"));
		value0.put(2, Arrays.asList("vier", "fuenf", "sechs"));
		// and put values into the second one (a nested map)
		Map<Integer, String> v1 = new HashMap<>();
		Map<Integer, String> v2 = new HashMap<>();
		v1.put(1, "first inner map - one");
		v1.put(2, "first inner map - two");
		v2.put(1, "second inner map - one");
		v2.put(2, "second inner map - two");
		value1.put(1, v1);
		value1.put(2, v2);
		// assign
		object.setValue0(value0);
		object.setValue1(value1);
		// @formatter:off
		/*
		 * expected tree structure
		 * 
		 * expectation																				das gesamte object
		 * 	 |--- firstMap																			die erste map
		 * 	 |       |--- firstMapEntry_0															map entry 1
		 * 	 |       |          |--- firstMapEntry_0_key 											integer as a key
		 * 	 |       |          |--- firstMapEntry_0_value 											list as value
		 * 	 |       |                      |--- element_0											erstes element
		 * 	 |       |                      |--- element_1											zweites element
		 * 	 |       |                      |--- element_2											drittes element
		 * 	 |       |--- firstMapEntry_1															map entry 2
		 * 	 |                  |--- firstMapEntry_1_key 											integer as a key
		 * 	 |                  |--- firstMapEntry_1_value											list as value
		 * 	 |                              |--- element_3											erstes element
		 * 	 |                              |--- element_4											zweites element
		 * 	 |                              |--- element_5											drittes element
		 * 	 | 
		 * 	 |--- secondMap																			die zweite map
		 *           |--- secondMapEntry_0															map entry 1
		 *           |          |--- secondMapEntry_0_key											integer as a key
		 *           |          |--- secondMapEntry_0_value											nested map as value
		 *           |                      |--- secondMap_0_SubEntry_0								map entry 1
		 *           |                                      |--- secondMap_0_SubEntry_0_key			key integer
		 *           |                                      |--- secondMap_0_SubEntry_0_value		value string
		 *           |                      |--- secondMap_0_SubEntry_1 							map entry 2
		 *           |                                      |--- secondMap_0_SubEntry_1_key 		key integer
		 *           |                                      |--- secondMap_0_SubEntry_1_value		value string
		 *           |
		 *           |--- secondMapEntry_1															map entry 2
		 *                      |--- secondMapEntry_1_key											integer as a key
		 *                      |--- secondMapEntry_1_value											nested map as value
		 *                                  |--- secondMap_1_SubEntry_0					    		map entry 1
		 *                                                 |--- secondMap_1_SubEntry_0_key			key integer
		 *                                                 |--- secondMap_1_SubEntry_0_value		value string
		 *                                  |--- secondMap_1_SubEntry_1								map entry 1
		 *                                                 |--- secondMap_1_SubEntry_1_key			key integer
		 *                                                 |--- secondMap_1_SubEntry_1_value		value string
		 * 
		 */
		// @formatter:on
		// create nodes
		RawBinary expected = new RawBinary();
		RawBinary firstMap = new RawBinary();
		RawBinary firstMapEntry_0 = new RawBinary();
		RawBinary firstMapEntry_0_key = new RawBinary();
		RawBinary firstMapEntry_0_value = new RawBinary();
		RawBinary element_0 = new RawBinary();
		RawBinary element_1 = new RawBinary();
		RawBinary element_2 = new RawBinary();
		RawBinary firstMapEntry_1 = new RawBinary();
		RawBinary firstMapEntry_1_key = new RawBinary();
		RawBinary firstMapEntry_1_value = new RawBinary();
		RawBinary element_3 = new RawBinary();
		RawBinary element_4 = new RawBinary();
		RawBinary element_5 = new RawBinary();
		RawBinary secondMap = new RawBinary();
		RawBinary secondMapEntry_0 = new RawBinary();
		RawBinary secondMapEntry_0_key = new RawBinary();
		RawBinary secondMapEntry_0_value = new RawBinary();
		RawBinary secondMap_0_SubEntry_0 = new RawBinary();
		RawBinary secondMap_0_SubEntry_0_key = new RawBinary();
		RawBinary secondMap_0_SubEntry_0_value = new RawBinary();
		RawBinary secondMap_0_SubEntry_1 = new RawBinary();
		RawBinary secondMap_0_SubEntry_1_key = new RawBinary();
		RawBinary secondMap_0_SubEntry_1_value = new RawBinary();
		RawBinary secondMapEntry_1 = new RawBinary();
		RawBinary secondMapEntry_1_key = new RawBinary();
		RawBinary secondMapEntry_1_value = new RawBinary();
		RawBinary secondMap_1_SubEntry_0 = new RawBinary();
		RawBinary secondMap_1_SubEntry_0_key = new RawBinary();
		RawBinary secondMap_1_SubEntry_0_value = new RawBinary();
		RawBinary secondMap_1_SubEntry_1 = new RawBinary();
		RawBinary secondMap_1_SubEntry_1_key = new RawBinary();
		RawBinary secondMap_1_SubEntry_1_value = new RawBinary();
		// assemble tree
		expected.addChild(firstMap);
		expected.addChild(secondMap);
		firstMap.addChild(firstMapEntry_0);
		firstMap.addChild(firstMapEntry_1);
		secondMap.addChild(secondMapEntry_0);
		secondMap.addChild(secondMapEntry_1);
		firstMapEntry_0.addChild(firstMapEntry_0_key);
		firstMapEntry_0.addChild(firstMapEntry_0_value);
		firstMapEntry_0_value.addChild(element_0);
		firstMapEntry_0_value.addChild(element_1);
		firstMapEntry_0_value.addChild(element_2);
		firstMapEntry_1.addChild(firstMapEntry_1_key);
		firstMapEntry_1.addChild(firstMapEntry_1_value);
		firstMapEntry_1_value.addChild(element_3);
		firstMapEntry_1_value.addChild(element_4);
		firstMapEntry_1_value.addChild(element_5);
		secondMapEntry_0.addChild(secondMapEntry_0_key);
		secondMapEntry_0.addChild(secondMapEntry_0_value);
		secondMapEntry_0_value.addChild(secondMap_0_SubEntry_0);
		secondMapEntry_0_value.addChild(secondMap_0_SubEntry_1);
		secondMap_0_SubEntry_0.addChild(secondMap_0_SubEntry_0_key);
		secondMap_0_SubEntry_0.addChild(secondMap_0_SubEntry_0_value);
		secondMap_0_SubEntry_1.addChild(secondMap_0_SubEntry_1_key);
		secondMap_0_SubEntry_1.addChild(secondMap_0_SubEntry_1_value);
		secondMapEntry_1.addChild(secondMapEntry_1_key);
		secondMapEntry_1.addChild(secondMapEntry_1_value);
		secondMapEntry_1_value.addChild(secondMap_1_SubEntry_0);
		secondMapEntry_1_value.addChild(secondMap_1_SubEntry_1);
		secondMap_1_SubEntry_0.addChild(secondMap_1_SubEntry_0_key);
		secondMap_1_SubEntry_0.addChild(secondMap_1_SubEntry_0_value);
		secondMap_1_SubEntry_1.addChild(secondMap_1_SubEntry_1_key);
		secondMap_1_SubEntry_1.addChild(secondMap_1_SubEntry_1_value);
		// set ids
		expected.setId((byte)113);
		firstMap.setId((byte)0);
		secondMap.setId((byte)1);
		firstMapEntry_0_key.setId(MapEntryId.KEY.getId());
		firstMapEntry_0_value.setId(MapEntryId.VALUE.getId());
		firstMapEntry_1_key.setId(MapEntryId.KEY.getId());
		firstMapEntry_1_value.setId(MapEntryId.VALUE.getId());
		secondMapEntry_0_key.setId(MapEntryId.KEY.getId());
		secondMapEntry_0_value.setId(MapEntryId.VALUE.getId());
		secondMapEntry_1_key.setId(MapEntryId.KEY.getId());
		secondMapEntry_1_value.setId(MapEntryId.VALUE.getId());
		secondMap_0_SubEntry_0_key.setId(MapEntryId.KEY.getId());
		secondMap_0_SubEntry_0_value.setId(MapEntryId.VALUE.getId());
		secondMap_0_SubEntry_1_key.setId(MapEntryId.KEY.getId());
		secondMap_0_SubEntry_1_value.setId(MapEntryId.VALUE.getId());
		secondMap_1_SubEntry_0_key.setId(MapEntryId.KEY.getId());
		secondMap_1_SubEntry_0_value.setId(MapEntryId.VALUE.getId());
		secondMap_1_SubEntry_1_key.setId(MapEntryId.KEY.getId());
		secondMap_1_SubEntry_1_value.setId(MapEntryId.VALUE.getId());
		// set types
		expected.setType(Type.OBJECT.getFlag());
		firstMap.setType(Type.MAP.getFlag());
		firstMapEntry_0_key.setType(Type.INTEGER.getFlag());
		firstMapEntry_0_value.setType(Type.COLLECTION.getFlag());
		element_0.setType(Type.STRING.getFlag());
		element_1.setType(Type.STRING.getFlag());
		element_2.setType(Type.STRING.getFlag());
		firstMapEntry_1_key.setType(Type.INTEGER.getFlag());
		firstMapEntry_1_value.setType(Type.COLLECTION.getFlag());
		element_3.setType(Type.STRING.getFlag());
		element_4.setType(Type.STRING.getFlag());
		element_5.setType(Type.STRING.getFlag());
		secondMap.setType(Type.MAP.getFlag());
		secondMapEntry_0_key.setType(Type.INTEGER.getFlag());
		secondMapEntry_0_value.setType(Type.MAP.getFlag());
		secondMap_0_SubEntry_0_key.setType(Type.INTEGER.getFlag());
		secondMap_0_SubEntry_0_value.setType(Type.STRING.getFlag());
		secondMap_0_SubEntry_1_key.setType(Type.INTEGER.getFlag());
		secondMap_0_SubEntry_1_value.setType(Type.STRING.getFlag());
		secondMapEntry_1_key.setType(Type.INTEGER.getFlag());
		secondMapEntry_1_value.setType(Type.MAP.getFlag());
		secondMap_1_SubEntry_0_key.setType(Type.INTEGER.getFlag());
		secondMap_1_SubEntry_0_value.setType(Type.STRING.getFlag());
		secondMap_1_SubEntry_1_key.setType(Type.INTEGER.getFlag());
		secondMap_1_SubEntry_1_value.setType(Type.STRING.getFlag());
		// set values if any
		firstMapEntry_0_key.setValue(new byte[] { 0, 0, 0, 1 });	// 1
		element_0.setValue(new byte[] { 101, 105, 110, 115 }); // eins
		element_1.setValue(new byte[] { 122, 119, 101, 105 }); // zwei
		element_2.setValue(new byte[] { 100, 114, 101, 105 }); // drei
		firstMapEntry_1_key.setValue(new byte[] { 0, 0, 0, 2 }); // 2
		element_3.setValue(new byte[] { 118, 105, 101, 114 }); // vier
		element_4.setValue(new byte[] { 102, 117, 101, 110, 102 }); // fuenf
		element_5.setValue(new byte[] { 115, 101, 99, 104, 115 }); // sechs
		secondMapEntry_0_key.setValue(new byte[] { 0, 0, 0, 1 });	// 1
		// @formatter:off
		secondMap_0_SubEntry_0_key.setValue(new byte[]{0, 0, 0, 1});	// 1
		secondMap_0_SubEntry_0_value.setValue(new byte[]{102, 105, 114, 115, 116, 32, 105, 110, 110, 101, 114, 32, 109, 97, 112, 32, 45, 32, 111, 110, 101}); // first inner map - one
		secondMap_0_SubEntry_1_key.setValue(new byte[]{0, 0, 0, 2});	// 2
		secondMap_0_SubEntry_1_value.setValue(new byte[]{102, 105, 114, 115, 116, 32, 105, 110, 110, 101, 114, 32, 109, 97, 112, 32, 45, 32, 116, 119, 111}); // first inner map - two
		
		secondMapEntry_1_key.setValue(new byte[]{0, 0, 0, 2});	// 2
			
		secondMap_1_SubEntry_0_key.setValue(new byte[]{0, 0, 0, 1});	// 1
		secondMap_1_SubEntry_0_value.setValue(new byte[]{115, 101, 99, 111, 110, 100, 32, 105, 110, 110, 101, 114, 32, 109, 97, 112, 32, 45, 32, 111, 110, 101}); // second inner map - one
		secondMap_1_SubEntry_1_key.setValue(new byte[]{0, 0, 0, 2});	// 2
		secondMap_1_SubEntry_1_value.setValue(new byte[]{115, 101, 99, 111, 110, 100, 32, 105, 110, 110, 101, 114, 32, 109, 97, 112, 32, 45, 32, 116, 119, 111}); // second inner map - two
		// @formatter:on
		// @formatter:off
		/*
		 * Result:
		 * RawBinary [id=113, type=9, value=null, children=[
		 * 
		 * 	RawBinary [id=0, type=50, value=null, children=[
		 * 		RawBinary [id=0, type=0, value=null, children=[
		 * 			RawBinary [id=0, type=3, value=[0, 0, 0, 1], children=null], 
		 * 			RawBinary [id=1, type=30, value=null, children=[
		 * 				RawBinary [id=0, type=10, value=[101, 105, 110, 115], children=null], 
		 * 				RawBinary [id=0, type=10, value=[122, 119, 101, 105], children=null], 
		 * 				RawBinary [id=0, type=10, value=[100, 114, 101, 105], children=null]]]]], 
		 * 		RawBinary [id=0, type=0, value=null, children=[
		 * 			RawBinary [id=0, type=3, value=[0, 0, 0, 2], children=null], 
		 * 			RawBinary [id=1, type=30, value=null, children=[
		 * 				RawBinary [id=0, type=10, value=[118, 105, 101, 114], children=null], 
		 * 				RawBinary [id=0, type=10, value=[102, 117, 101, 110, 102], children=null], 
		 * 				RawBinary [id=0, type=10, value=[115, 101, 99, 104, 115], children=null]]]]]]], 
		 * 	
		 * 	RawBinary [id=1, type=50, value=null, children=[
		 * 		RawBinary [id=0, type=0, value=null, children=[
		 * 			RawBinary [id=0, type=3, value=[0, 0, 0, 1], children=null], 
		 * 			RawBinary [id=1, type=50, value=null, children=[
		 * 				RawBinary [id=0, type=0, value=null, children=[
		 * 					RawBinary [id=0, type=3, value=[0, 0, 0, 1], children=null], 
		 * 					RawBinary [id=1, type=10, value=[102, 105, 114, 115, 116, 32, 105, 110, 110, 101, 114, 32, 109, 97, 112, 32, 45, 32, 111, 110, 101], children=null]]], 
		 * 				RawBinary [id=0, type=0, value=null, children=[
		 * 					RawBinary [id=0, type=3, value=[0, 0, 0, 2], children=null], 
		 * 					RawBinary [id=1, type=10, value=[102, 105, 114, 115, 116, 32, 105, 110, 110, 101, 114, 32, 109, 97, 112, 32, 45, 32, 116, 119, 111], children=null]]]]]]], 
		 * 		RawBinary [id=0, type=0, value=null, children=[
		 * 			RawBinary [id=0, type=3, value=[0, 0, 0, 2], children=null], 
		 * 			RawBinary [id=1, type=50, value=null, children=[
		 * 				RawBinary [id=0, type=0, value=null, children=[
		 * 					RawBinary [id=0, type=3, value=[0, 0, 0, 1], children=null], 
		 * 					RawBinary [id=1, type=10, value=[115, 101, 99, 111, 110, 100, 32, 105, 110, 110, 101, 114, 32, 109, 97, 112, 32, 45, 32, 111, 110, 101], children=null]]], 
		 * 				RawBinary [id=0, type=0, value=null, children=[
		 * 					RawBinary [id=0, type=3, value=[0, 0, 0, 2], children=null], 
		 * 					RawBinary [id=1, type=10, value=[115, 101, 99, 111, 110, 100, 32, 105, 110, 110, 101, 114, 32, 109, 97, 112, 32, 45, 32, 116, 119, 111], children=null]]]]]]]]]]]
		 * 
		 */
		// @formatter:on
		// encode the object
		byte[] result = encoder.encode(object);
		// assert
		assertEquals("empty element", expected, RawBinary.parse(result));
	}
}