// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

@ObjectID ( 27 )
public class EnumTypes {

	public static enum Color {
								BLUE,
								RED,
								GREEN,
								YELLOW;
	}

	private Color	color		= null;

	private Color	background	= null;

	private Color	nix			= null;

	/**
	 * @return the color
	 */
	public Color getColor () {
		return this.color;
	}

	/**
	 * @param color the color to set
	 */
	@MemberID ( 0 )
	public void setColor ( Color color ) {
		this.color = color;
	}

	/**
	 * @return the background
	 */
	@MemberID ( 1 )
	public Color getBackground () {
		return this.background;
	}

	/**
	 * @param background the background to set
	 */
	public void setBackground ( Color background ) {
		this.background = background;
	}

	/**
	 * @return the nix
	 */
	@MemberID ( 2 )
	public Color getNix () {
		return this.nix;
	}

	/**
	 * @param nix the nix to set
	 */
	public void setNix ( Color nix ) {
		this.nix = nix;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( (this.background == null) ? 0 : this.background.hashCode());
		result = prime * result + ( (this.color == null) ? 0 : this.color.hashCode());
		result = prime * result + ( (this.nix == null) ? 0 : this.nix.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals ( Object obj ) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (! (obj instanceof EnumTypes)) {
			return false;
		}
		EnumTypes other = (EnumTypes)obj;
		if (this.background != other.background) {
			return false;
		}
		if (this.color != other.color) {
			return false;
		}
		if (this.nix != other.nix) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString () {
		return String.format("EnumTypes [color=%s, Background=%s, nix=%s]", this.color, this.background, this.nix);
	}
}