// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

import java.util.Arrays;

/**
 * Created: 26 Aug 2015, 8:45:39 pm
 * 
 * @author Andreas Junius
 */
@ObjectID ( -2 )
public class TreeNode {

	private int			value		= 0;

	private TreeNode	parent		= null;

	private TreeNode[]	children	= null;

	/**
	 * @return the value
	 */
	@MemberID ( 0 )
	public int getValue () {
		return this.value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue ( int value ) {
		this.value = value;
	}

	/**
	 * @return the parent
	 */
	// @MemberID ( 1 )
	// FIXME: document the fact that a link to parent elements leads to
	// a stack overflow exception.
	// a parent relationship can be set when a node gets added to a parent,
	// therefore this doesn't denote a loss of information
	public TreeNode getParent () {
		return this.parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent ( TreeNode parent ) {
		this.parent = parent;
	}

	/**
	 * @return the children
	 */
	@MemberID ( 1 )
	public TreeNode[] getChildren () {
		return this.children;
	}

	/**
	 * @param children
	 *            the children to set
	 */
	public void setChildren ( TreeNode[] children ) {
		this.children = children;
		if (this.children != null) {
			for (TreeNode node : this.children) {
				node.setParent(this);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return String.format("TreeNode [value=%s, parent=%s, children=%s]", this.value, ( (this.parent != null) ? "parent "
																													+ this.parent.value : "null"), Arrays.toString(this.children));
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this.children);
		result = prime * result + this.value;
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals ( Object obj ) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (! (obj instanceof TreeNode)) {
			return false;
		}
		TreeNode other = (TreeNode)obj;
		if (!Arrays.equals(this.children, other.children)) {
			return false;
		}
		if (this.value != other.value) {
			return false;
		}
		return true;
	}
}