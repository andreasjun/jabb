// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

/**
 * Created: 26 Aug 2015, 8:44:25 pm
 * 
 * @author Andreas Junius
 */
@ObjectID ( 17 )
public class Wrapper {

	private Byte		value0	= null;

	private Short		value1	= null;

	private Integer		value2	= null;

	private Long		value3	= null;

	private Float		value4	= null;

	private Double		value5	= null;

	private Character	value6	= null;

	private Boolean		value7	= null;

	/**
	 * @return the value0
	 */
	@MemberID ( 0 )
	public Byte getValue0 () {
		return this.value0;
	}

	/**
	 * @param value0
	 *            the value0 to set
	 */
	public void setValue0 ( Byte value0 ) {
		this.value0 = value0;
	}

	/**
	 * @return the value1
	 */
	@MemberID ( 1 )
	public Short getValue1 () {
		return this.value1;
	}

	/**
	 * @param value1
	 *            the value1 to set
	 */
	public void setValue1 ( Short value1 ) {
		this.value1 = value1;
	}

	/**
	 * @return the value2
	 */
	@MemberID ( 2 )
	public Integer getValue2 () {
		return this.value2;
	}

	/**
	 * @param value2
	 *            the value2 to set
	 */
	public void setValue2 ( Integer value2 ) {
		this.value2 = value2;
	}

	/**
	 * @return the value3
	 */
	@MemberID ( 3 )
	public Long getValue3 () {
		return this.value3;
	}

	/**
	 * @param value3
	 *            the value3 to set
	 */
	public void setValue3 ( Long value3 ) {
		this.value3 = value3;
	}

	/**
	 * @return the value4
	 */
	@MemberID ( 4 )
	public Float getValue4 () {
		return this.value4;
	}

	/**
	 * @param value4
	 *            the value4 to set
	 */
	public void setValue4 ( Float value4 ) {
		this.value4 = value4;
	}

	/**
	 * @return the value5
	 */
	@MemberID ( 5 )
	public Double getValue5 () {
		return this.value5;
	}

	/**
	 * @param value5
	 *            the value5 to set
	 */
	public void setValue5 ( Double value5 ) {
		this.value5 = value5;
	}

	/**
	 * @return the value6
	 */
	@MemberID ( 6 )
	public Character getValue6 () {
		return this.value6;
	}

	/**
	 * @param value6
	 *            the value6 to set
	 */
	public void setValue6 ( Character value6 ) {
		this.value6 = value6;
	}

	/**
	 * @return the value7
	 */
	@MemberID ( 7 )
	public Boolean getValue7 () {
		return this.value7;
	}

	/**
	 * @param value7
	 *            the value7 to set
	 */
	public void setValue7 ( Boolean value7 ) {
		this.value7 = value7;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return String.format("Wrapper [value0=%s, value1=%s, value2=%s, value3=%s, value4=%s, value5=%s, value6=%s, value7=%s]", this.value0, this.value1, this.value2, this.value3, this.value4, this.value5, this.value6, this.value7);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( (this.value0 == null) ? 0 : this.value0.hashCode());
		result = prime * result + ( (this.value1 == null) ? 0 : this.value1.hashCode());
		result = prime * result + ( (this.value2 == null) ? 0 : this.value2.hashCode());
		result = prime * result + ( (this.value3 == null) ? 0 : this.value3.hashCode());
		result = prime * result + ( (this.value4 == null) ? 0 : this.value4.hashCode());
		result = prime * result + ( (this.value5 == null) ? 0 : this.value5.hashCode());
		result = prime * result + ( (this.value6 == null) ? 0 : this.value6.hashCode());
		result = prime * result + ( (this.value7 == null) ? 0 : this.value7.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals ( Object obj ) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Wrapper other = (Wrapper)obj;
		if (this.value0 == null) {
			if (other.value0 != null) {
				return false;
			}
		} else if (!this.value0.equals(other.value0)) {
			return false;
		}
		if (this.value1 == null) {
			if (other.value1 != null) {
				return false;
			}
		} else if (!this.value1.equals(other.value1)) {
			return false;
		}
		if (this.value2 == null) {
			if (other.value2 != null) {
				return false;
			}
		} else if (!this.value2.equals(other.value2)) {
			return false;
		}
		if (this.value3 == null) {
			if (other.value3 != null) {
				return false;
			}
		} else if (!this.value3.equals(other.value3)) {
			return false;
		}
		if (this.value4 == null) {
			if (other.value4 != null) {
				return false;
			}
		} else if (!this.value4.equals(other.value4)) {
			return false;
		}
		if (this.value5 == null) {
			if (other.value5 != null) {
				return false;
			}
		} else if (!this.value5.equals(other.value5)) {
			return false;
		}
		if (this.value6 == null) {
			if (other.value6 != null) {
				return false;
			}
		} else if (!this.value6.equals(other.value6)) {
			return false;
		}
		if (this.value7 == null) {
			if (other.value7 != null) {
				return false;
			}
		} else if (!this.value7.equals(other.value7)) {
			return false;
		}
		return true;
	}
}