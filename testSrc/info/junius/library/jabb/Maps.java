// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

import java.util.Map;

/**
 * Created: 26 Aug 2015, 8:46:32 pm
 * 
 * @author Andreas Junius
 */
@ObjectID ( 27 )
public class Maps {

	private Map<String, Long>		value0	= null;

	// unknown object, will be treated as null value and null type
	private Map<Integer, Object>	value1	= null;

	// known, annotated object
	private Map<Integer, Element>	value2	= null;

	/**
	 * @return the value0
	 */
	public Map<String, Long> getValue0 () {
		return this.value0;
	}

	/**
	 * @param value0
	 *            the value0 to set
	 */
	@MemberID ( 0 )
	@Generic ( "Map<String, Long>" )
	public void setValue0 ( Map<String, Long> value0 ) {
		this.value0 = value0;
	}

	/**
	 * @return the value1
	 */
	public Map<Integer, Object> getValue1 () {
		return this.value1;
	}

	/**
	 * @param value1
	 *            the value1 to set
	 */
	@MemberID ( 1 )
	@Generic ( "Map<Integer, Object>" )
	public void setValue1 ( Map<Integer, Object> value1 ) {
		this.value1 = value1;
	}

	/**
	 * @return the value2
	 */
	public Map<Integer, Element> getValue2 () {
		return this.value2;
	}

	/**
	 * @param value2
	 *            the value2 to set
	 */
	@MemberID ( 2 )
	@Generic ( "Map<Integer, info.junius.library.jabb.Element>" )
	public void setValue2 ( Map<Integer, Element> value2 ) {
		this.value2 = value2;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return String.format("Maps [value0=%s, value1=%s, value2=%s]", this.value0, this.value1, this.value2);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( (this.value0 == null) ? 0 : this.value0.hashCode());
		result = prime * result + ( (this.value1 == null) ? 0 : this.value1.hashCode());
		result = prime * result + ( (this.value2 == null) ? 0 : this.value2.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals ( Object obj ) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Maps other = (Maps)obj;
		if (this.value0 == null) {
			if (other.value0 != null) {
				return false;
			}
		} else if (!this.value0.equals(other.value0)) {
			return false;
		}
		if (this.value1 == null) {
			if (other.value1 != null) {
				return false;
			}
		} else if (!this.value1.equals(other.value1)) {
			return false;
		}
		if (this.value2 == null) {
			if (other.value2 != null) {
				return false;
			}
		} else if (!this.value2.equals(other.value2)) {
			return false;
		}
		return true;
	}
}