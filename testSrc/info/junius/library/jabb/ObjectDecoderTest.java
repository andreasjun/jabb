// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import org.junit.Test;
import info.junius.library.jabb.EnumTypes.Color;
import info.junius.library.jabb.constants.Type;
import info.junius.library.jabb.dt.Converter;
import info.junius.library.jabb.dt.RawBinary;
import info.junius.library.jabb.impl.DefaultObjectDecoder;
import info.junius.library.jabb.impl.DefaultObjectEncoder;
import info.junius.library.jabb.util.EncoderFactory;

/**
 * Tests decoding capabilities. Preconditions: properly working encoder; equals and hashcode correct implemented on
 * target types.
 * Created: 6 Feb 2016, 2:30:44 pm
 * 
 * @author Andreas Junius
 */
public class ObjectDecoderTest {

	/** encoder factory */
	private static final EncoderFactory FACTORY = new EncoderFactory();

	/**
	 * A null value as input will return null, no matter what target type
	 */
	@Test
	public void testNull () {
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.NULL.getFlag());
		rawBinary.setValue(Converter.NULL);
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		for (Type type : Type.values()) {
			Object returnValue = this.doReflectiveCall(decoder, rawBinary, type.getClass());
			assertNull("Value null test " + type, returnValue);
		}
	}

	/**
	 * A null value as input will return null, no matter what target type, even in case of an enum
	 */
	@Test
	public void testEnumNull () throws ParseException {
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.NULL.getFlag());
		rawBinary.setValue(Converter.NULL);
		// target type input
		Class<?> clazz = Enum.class;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object returnValue = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertNull("Enum null test", returnValue);
	}

	@Test
	public void testEnum () throws ParseException {
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.ENUM.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(Color.GREEN).encode(Color.GREEN));
		// target type input
		Class<?> clazz = Color.class;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object returnValue = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("Enum test", Color.GREEN, returnValue);
	}

	/*
	 * NULL, BYTE, SHORT, INTEGER, LONG, FLOAT, DOUBLE, BOOLEAN, CHARACTER, STRING, DATE, TIMESTAMP
	 * BYTE_ARRAY, SHORT_ARRAY, INT_ARRAY, LONG_ARRAY, FLOAT_ARRAY, DOUBLE_ARRAY, BOOLEAN_ARRAY, CHAR_ARRAY
	 */
	@Test
	public void testByte () throws ParseException {
		byte expected = 42;
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.BYTE.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = Byte.TYPE;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("primitive byte", expected, actual);
		assertEquals("primitive byte, type test", Byte.class, actual.getClass());
	}

	@Test
	public void testShort () throws ParseException {
		short expected = 42;
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.SHORT.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = Short.TYPE;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("primitive short", expected, actual);
		assertEquals("primitive short, type test", Short.class, actual.getClass());
	}

	@Test
	public void testInteger () throws ParseException {
		int expected = 42;
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.INTEGER.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = Integer.TYPE;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("primitive int", expected, actual);
		assertEquals("primitive int, type test", Integer.class, actual.getClass());
	}

	@Test
	public void testLong () throws ParseException {
		long expected = 42;
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.LONG.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = Long.TYPE;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("primitive long", expected, actual);
		assertEquals("primitive long, type test", Long.class, actual.getClass());
	}

	@Test
	public void testFloat () throws ParseException {
		float expected = 42F;
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.FLOAT.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = Float.TYPE;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("primitive float", expected, actual);
		assertEquals("primitive float, type test", Float.class, actual.getClass());
	}

	@Test
	public void testDouble () throws ParseException {
		double expected = 42D;
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.DOUBLE.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = Double.TYPE;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("primitive double", expected, actual);
		assertEquals("primitive double, type test", Double.class, actual.getClass());
	}

	@Test
	public void testBoolean () throws ParseException {
		boolean expected = true;
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.BOOLEAN.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = Boolean.TYPE;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("primitive boolean", expected, actual);
		assertEquals("primitive boolean, type test", Boolean.class, actual.getClass());
	}

	@Test
	public void testChar () throws ParseException {
		char expected = 'a';
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.CHARACTER.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = Character.TYPE;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("primitive char", expected, actual);
		assertEquals("primitive char, type test", Character.class, actual.getClass());
	}

	/*
	 * NULL, BYTE, SHORT, INTEGER, LONG, FLOAT, DOUBLE, BOOLEAN, CHARACTER, STRING, DATE, TIMESTAMP
	 * BYTE_ARRAY, SHORT_ARRAY, INT_ARRAY, LONG_ARRAY, FLOAT_ARRAY, DOUBLE_ARRAY, BOOLEAN_ARRAY, CHAR_ARRAY
	 */
	@Test
	public void testString () throws ParseException {
		String expected = "hello world!";
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.STRING.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = String.class;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("String", expected, actual);
		assertEquals("String, type test", String.class, actual.getClass());
	}

	@Test
	public void testDate () throws ParseException {
		Date expected = Date.from(Instant.now());
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.DATE.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = Date.class;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("Date", expected, actual);
		assertEquals("Date, type test", Date.class, actual.getClass());
	}

	@Test
	public void testTimestamp () throws ParseException {
		Timestamp expected = Timestamp.from(Instant.now());
		// raw binary input
		RawBinary rawBinary = new RawBinary();
		rawBinary.setId((byte)0);
		rawBinary.setType(Type.TIMESTAMP.getFlag());
		rawBinary.setValue(FACTORY.getEncoder(expected).encode(expected));
		// target type input
		Class<?> clazz = Timestamp.class;
		// the decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		Object actual = this.doReflectiveCall(decoder, rawBinary, clazz);
		assertEquals("Timestamp, type test", Timestamp.class, actual.getClass());
		assertEquals("Timestamp", expected, actual);
	}

	/**
	 * Empty JABB object
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testEmptyElement () throws ParseException {
		// create and assemble object
		StringArgTypes expectation = new StringArgTypes();
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		StringArgTypes result = decoder.decode(octets, StringArgTypes.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * String arg types
	 * 
	 * @throws URISyntaxException
	 * @throws MalformedURLException
	 * @throws ParseException
	 */
	@Test
	public void testStringArgTypes () throws URISyntaxException, MalformedURLException, ParseException {
		// create and assemble object
		StringArgTypes expectation = new StringArgTypes();
		expectation.setFile(new File("home/andy/test.txt"));
		expectation.setUri(new URI("file:///home/andy/test.txt"));
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		StringArgTypes result = decoder.decode(octets, StringArgTypes.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Single annotated element
	 */
	@Test
	public void testElement () throws ParseException {
		// create instance for encoding
		Element expectation = new Element("Foobar", 11);
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		Element result = decoder.decode(octets, Element.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Annotated object holding primitives
	 */
	@Test
	public void testPrimitives () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Primitives expectation = new Primitives();
		expectation.setValue0((byte)1);
		expectation.setValue1((short)1);
		expectation.setValue2(1);
		expectation.setValue3(1L);
		expectation.setValue4(1.0F);
		expectation.setValue5(1.0D);
		expectation.setValue6((char)0);
		expectation.setValue7(true);
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		Primitives result = decoder.decode(octets, Primitives.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Annotated object holding primitive wrappers
	 */
	@Test
	public void testWrapper () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Wrapper expectation = new Wrapper();
		expectation.setValue0(Byte.valueOf((byte)1));
		expectation.setValue1(Short.valueOf((short)1));
		expectation.setValue2(Integer.valueOf(1));
		expectation.setValue3(Long.valueOf(1L));
		expectation.setValue4(Float.valueOf(1.0F));
		expectation.setValue5(Double.valueOf(1.0D));
		expectation.setValue6(Character.valueOf((char)0));
		expectation.setValue7(Boolean.TRUE);
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		Wrapper result = decoder.decode(octets, Wrapper.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Annotated object holding arrays of primitive values
	 */
	@Test
	public void testPrimitiveArrays () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		PrimArrays expectation = new PrimArrays();
		expectation.setValue0(new byte[] {	1,
											2 });
		expectation.setValue1(new short[] {	1,
											2 });
		expectation.setValue2(new int[] {	1,
											2 });
		expectation.setValue3(new long[] {	1L,
											2L });
		expectation.setValue4(new float[] {	1.0F,
											2.0F });
		expectation.setValue5(new double[] {	1.0,
												2.0 });
		expectation.setValue6(new char[] {	'a',
											'b' });
		expectation.setValue7(new boolean[] {	true,
												false });
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		PrimArrays result = decoder.decode(octets, PrimArrays.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Annotated object holding arrays of objects
	 */
	@Test
	public void testObjectArrays () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		ObjArrays expectation = new ObjArrays();
		expectation.setValue0(new Byte[] {	1,
											2 });
		expectation.setValue1(new Short[] {	1,
											2 });
		expectation.setValue2(new Integer[] {	1,
												2 });
		expectation.setValue3(new Long[] {	1L,
											2L });
		expectation.setValue4(new Float[] {	1.0F,
											2.0F });
		expectation.setValue5(new Double[] {	1.0,
												2.0 });
		expectation.setValue6(new Character[] {	'a',
												'b' });
		expectation.setValue7(new Boolean[] {	Boolean.TRUE,
												Boolean.FALSE });
		expectation.setValue8(new Object[] {	new Object(),
												new Object() });
		expectation.setValue9(new Element[] {	new Element("one", 1),
												new Element("two", 2) });
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		ObjArrays result = decoder.decode(octets, ObjArrays.class);
		// the decoder won't be able to decode the unknown object, it'll be an array containing null values
		expectation.setValue8(new Object[] {	null,
												null });
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Annotated object holding collections
	 */
	@Test
	public void testCollections () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		ObjCollections expectation = new ObjCollections();
		// different types of collections
		List<String> list = Arrays.asList(new String[] {	"hello",
															"world" });
		Set<String> set = new HashSet<>();
		set.addAll(list);
		Queue<String> queue = new PriorityQueue<>(list);
		// add them
		expectation.setValue0(list);
		expectation.setValue1(set);
		expectation.setValue2(queue);
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		ObjCollections result = decoder.decode(octets, ObjCollections.class);
		// check
		assertNotNull("null check", result);
		assertNotNull("null check, list", result.getValue0());
		assertNotNull("null check, set", result.getValue1());
		assertNotNull("null check, queue", result.getValue2());
		// compare elements
		assertEquals("List", expectation.getValue0(), result.getValue0());
		assertEquals("Set", expectation.getValue1(), result.getValue1());
		// queue doesn't have hashcode and equals implementations
		for (String value : expectation.getValue2()) {
			if (!result.getValue2().contains(value)) {
				fail("Queue not equal");
			}
		}
		assertNotNull("null check", result);
	}

	/**
	 * Annotated object holding maps
	 */
	@Test
	public void testMaps () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		Maps expectation = new Maps();
		// different types of maps
		Map<String, Long> value0 = new HashMap<>();
		// unknown object, treat as null value and type
		Map<Integer, Object> value1 = new HashMap<>();
		// known, annotated object
		Map<Integer, Element> value2 = new HashMap<>();
		value0.put("keyOne", 1L);
		value1.put(Integer.valueOf(1), new Object());
		value2.put(Integer.valueOf(1), new Element("hello", 1));
		// add them
		expectation.setValue0(value0);
		expectation.setValue1(value1);
		expectation.setValue2(value2);
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		Maps result = decoder.decode(octets, Maps.class);
		// the unknown object has to be set to null
		value1.put(Integer.valueOf(1), null);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Object implementing an interface and extending an abstract class
	 */
	@Test
	public void testInheritance () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// build object
		TypeInterface expectation = new TypeImpl();
		expectation.setValue0("Hello");
		expectation.setValue1(42);
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		TypeInterface result = decoder.decode(octets, TypeInterface.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Object that has enum type members
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testEnumTypes () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// build object
		EnumTypes expectation = new EnumTypes();
		expectation.setColor(Color.GREEN);
		expectation.setBackground(Color.RED);
		expectation.setNix(null);
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		EnumTypes result = decoder.decode(octets, EnumTypes.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Annotated object holding a tree structure (requires impl. of object and array)
	 */
	@Test
	public void testTreeStructure () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		TreeNode expectation = new TreeNode();
		TreeNode b = new TreeNode();
		TreeNode c = new TreeNode();
		TreeNode d = new TreeNode();
		TreeNode e = new TreeNode();
		TreeNode f = new TreeNode();
		expectation.setChildren(new TreeNode[] {	b,
													f });
		b.setChildren(new TreeNode[] {	c,
										d });
		d.setChildren(new TreeNode[] { e });
		expectation.setValue(0);
		b.setValue(1);
		c.setValue(2);
		d.setValue(3);
		e.setValue(4);
		f.setValue(5);
		expectation.setParent(null);
		b.setParent(expectation);
		c.setParent(b);
		d.setParent(b);
		e.setParent(d);
		f.setParent(expectation);
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		TreeNode result = decoder.decode(octets, TreeNode.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	// more tricky stuff here to come ....
	// Some edge-cases aren't supported:
	//
	// -n-dimensional Arrays
	// -nested collections
	// -nested maps
	// -maps containing maps or collections as values (keys always have to be immutable objects - collections aren't)
	//
	// Support for these might be added later.
	// System.out.println(expectation);
	// System.out.println(result);
	/**
	 * Special case 1, multidimensional arrays of primitives
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testMultiDimensionalArrays () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		PrimNdimArrays expectation = new PrimNdimArrays();
		expectation.setValue0(new byte[][][] { new byte[][] {	new byte[] {	1,
																			2,
																			3 },
																new byte[] {	4,
																				5,
																				6 } } });
		expectation.setValue1(new short[][] {	new short[] {	1,
															2 },
												new short[] {	3,
																4,
																5 } });
		expectation.setValue2(new int[][] {	new int[] {	1,
														2 },
											new int[] {	3,
														4,
														5 } });
		expectation.setValue3(new long[][] {	new long[] {	1L,
															2L },
												new long[] {	3L,
																4L,
																5L } });
		expectation.setValue4(new float[][] {	new float[] {	1.0F,
															2.0F },
												new float[] {	3.0F,
																4.0F,
																5.0F } });
		expectation.setValue5(new double[][] {	new double[] {	1.0D,
																2.0D },
												new double[] {	3.0D,
																4.0D,
																5.0D } });
		expectation.setValue6(new char[][] {	new char[] {	'a',
															'b' },
												new char[] {	'c',
																'd',
																'e' } });
		expectation.setValue7(new boolean[][] {	new boolean[] {	Boolean.TRUE,
																Boolean.FALSE },
												new boolean[] {	Boolean.FALSE,
																Boolean.TRUE } });
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		PrimNdimArrays result = decoder.decode(octets, PrimNdimArrays.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Special case 2, multidimensional arrays of objects (wrappers)
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testMultiDimensionalObjectArrays () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		PrimNdimObjectArrays expectation = new PrimNdimObjectArrays();
		expectation.setValue0(new Byte[][][] { new Byte[][] {	new Byte[] {	1,
																			2,
																			3 },
																new Byte[] {	4,
																				5,
																				6 } } });
		expectation.setValue1(new Short[][] {	new Short[] {	1,
															2 },
												new Short[] {	3,
																4,
																5 } });
		expectation.setValue2(new Integer[][] {	new Integer[] {	1,
																2 },
												new Integer[] {	3,
																4,
																5 } });
		expectation.setValue3(new Long[][] {	new Long[] {	1L,
															2L },
												new Long[] {	3L,
																4L,
																5L } });
		expectation.setValue4(new Float[][] {	new Float[] {	1.0F,
															2.0F },
												new Float[] {	3.0F,
																4.0F,
																5.0F } });
		expectation.setValue5(new Double[][] {	new Double[] {	1.0D,
																2.0D },
												new Double[] {	3.0D,
																4.0D,
																5.0D } });
		expectation.setValue6(new Character[][] {	new Character[] {	'a',
																	'b' },
													new Character[] {	'c',
																		'd',
																		'e' } });
		expectation.setValue7(new Boolean[][] {	new Boolean[] {	Boolean.TRUE,
																Boolean.FALSE },
												new Boolean[] {	Boolean.FALSE,
																Boolean.TRUE } });
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		PrimNdimObjectArrays result = decoder.decode(octets, PrimNdimObjectArrays.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Special case 3, collection types that contain other collections as values.
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testNestedCollections () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		NestedCollections expectation = new NestedCollections();
		// list of lists
		List<List<String>> value0 = new ArrayList<>();
		value0.add(Arrays.asList("this", "is", "really", "cool"));
		value0.add(Arrays.asList("this", "is", "also", "not", "too", "bad"));
		value0.add(Arrays.asList("hello", "world", "!"));
		List<List<Set<String>>> value1 = new ArrayList<>();
		List<Set<String>> v0 = new ArrayList<>();
		v0.add(new HashSet<>(Arrays.asList("foo", "bar", "zoom")));
		v0.add(new HashSet<>(Arrays.asList("racka", "racka", "zap")));
		v0.add(new HashSet<>(Arrays.asList("Hallo", "Welt", "!")));
		List<Set<String>> v1 = new ArrayList<>();
		v1.add(new HashSet<>(Arrays.asList("oans", "zwoa", "gsuffa!")));
		v1.add(new HashSet<>(Arrays.asList("aber", "so", "gehts", "net")));
		value1.add(v0);
		value1.add(v1);
		// assign
		expectation.setValue0(value0);
		expectation.setValue1(value1);
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(expectation);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		NestedCollections result = decoder.decode(octets, NestedCollections.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", expectation, result);
	}

	/**
	 * Special case 4, map types that contain other maps or collections as values. Collections or maps as keys are not
	 * supported because mutible types are not suitable for keys - although Java allows that kind of key.
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testNestedMaps () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		NestedMaps object = new NestedMaps();
		// create the two maps
		Map<Integer, List<String>> value0 = new HashMap<>();
		Map<Integer, Map<Integer, String>> value1 = new HashMap<>();
		// put values into the first one (a list)
		value0.put(1, Arrays.asList("eins", "zwei", "drei"));
		value0.put(2, Arrays.asList("vier", "fuenf", "sechs"));
		// and put values into the second one (a nested map)
		Map<Integer, String> v1 = new HashMap<>();
		Map<Integer, String> v2 = new HashMap<>();
		v1.put(1, "first inner map - one");
		v1.put(2, "first inner map - two");
		v2.put(1, "second inner map - one");
		v2.put(2, "second inner map - two");
		value1.put(1, v1);
		value1.put(2, v2);
		// assign
		object.setValue0(value0);
		object.setValue1(value1);
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(object);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		NestedMaps result = decoder.decode(octets, NestedMaps.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", object, result);
	}

	/**
	 * Special case 5, @Generic annotation on subclass of declaring JABB type.
	 * 
	 * @throws ParseException
	 */
	@Test
	public void testGenericFinder () throws ParseException {
		// get encoder
		ObjectEncoder encoder = new DefaultObjectEncoder();
		// create and assemble object
		GenericFinderTestType object = new ConcreteGenericFinderTestType();
		((ConcreteGenericFinderTestType)object).setTheCollection(Arrays.asList("one", "two", "three"));
		((ConcreteGenericFinderTestType)object).setAnotherCollection(Arrays.asList("four", "five", "six"));
		// encode (assuming here that this works already)
		byte[] octets = encoder.encode(object);
		// get decoder for same type
		ObjectDecoder decoder = new DefaultObjectDecoder();
		// decode
		GenericFinderTestType result = decoder.decode(octets, GenericFinderTestType.class);
		// check
		assertNotNull("null check", result);
		assertEquals("equality check", object, result);
	}

	/**
	 * Calls the parse-method using reflection (this method gets tested although it is declared protected)
	 * 
	 * @param decoder
	 * @param rawBinary
	 * @param clazz
	 * @return return value of the call
	 */
	private Object doReflectiveCall ( ObjectDecoder decoder, RawBinary rawBinary, Class<?> clazz ) {
		// method under test is protected
		Object result = null;
		try {
			Method method = DefaultObjectDecoder.class
							.getDeclaredMethod("parse", RawBinary.class, Class.class, Generic.class, Integer.TYPE);
			method.setAccessible(true);
			result = method.invoke(decoder,
							new Object[] {	rawBinary,
											clazz,
											null,
											0 });
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
			throw new RuntimeException("Method call failed", e);
		}
		return result;
	}
}