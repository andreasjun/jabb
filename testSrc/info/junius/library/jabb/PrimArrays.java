// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

import java.util.Arrays;

/**
 * Created: 26 Aug 2015, 8:46:02 pm
 * 
 * @author Andreas Junius
 */
@ObjectID ( 19 )
public class PrimArrays {

	private byte[]		value0	= null;

	private short[]		value1	= null;

	private int[]		value2	= null;

	private long[]		value3	= null;

	private float[]		value4	= null;

	private double[]	value5	= null;

	private char[]		value6	= null;

	private boolean[]	value7	= null;

	/**
	 * @return the value0
	 */
	@MemberID ( 0 )
	public byte[] getValue0 () {
		return this.value0;
	}

	/**
	 * @param value0
	 *            the value0 to set
	 */
	public void setValue0 ( byte[] value0 ) {
		this.value0 = value0;
	}

	/**
	 * @return the value1
	 */
	@MemberID ( 1 )
	public short[] getValue1 () {
		return this.value1;
	}

	/**
	 * @param value1
	 *            the value1 to set
	 */
	public void setValue1 ( short[] value1 ) {
		this.value1 = value1;
	}

	/**
	 * @return the value2
	 */
	@MemberID ( 2 )
	public int[] getValue2 () {
		return this.value2;
	}

	/**
	 * @param value2
	 *            the value2 to set
	 */
	public void setValue2 ( int[] value2 ) {
		this.value2 = value2;
	}

	/**
	 * @return the value3
	 */
	@MemberID ( 3 )
	public long[] getValue3 () {
		return this.value3;
	}

	/**
	 * @param value3
	 *            the value3 to set
	 */
	public void setValue3 ( long[] value3 ) {
		this.value3 = value3;
	}

	/**
	 * @return the value4
	 */
	@MemberID ( 4 )
	public float[] getValue4 () {
		return this.value4;
	}

	/**
	 * @param value4
	 *            the value4 to set
	 */
	public void setValue4 ( float[] value4 ) {
		this.value4 = value4;
	}

	/**
	 * @return the value5
	 */
	@MemberID ( 5 )
	public double[] getValue5 () {
		return this.value5;
	}

	/**
	 * @param value5
	 *            the value5 to set
	 */
	public void setValue5 ( double[] value5 ) {
		this.value5 = value5;
	}

	/**
	 * @return the value6
	 */
	@MemberID ( 6 )
	public char[] getValue6 () {
		return this.value6;
	}

	/**
	 * @param value6
	 *            the value6 to set
	 */
	public void setValue6 ( char[] value6 ) {
		this.value6 = value6;
	}

	/**
	 * @return the value7
	 */
	@MemberID ( 7 )
	public boolean[] getValue7 () {
		return this.value7;
	}

	/**
	 * @param value7
	 *            the value7 to set
	 */
	public void setValue7 ( boolean[] value7 ) {
		this.value7 = value7;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return String.format("PrimArrays [value0=%s, value1=%s, value2=%s, value3=%s, value4=%s, value5=%s, value6=%s, value7=%s]", Arrays.toString(this.value0), Arrays.toString(this.value1), Arrays.toString(this.value2), Arrays.toString(this.value3), Arrays.toString(this.value4), Arrays.toString(this.value5), Arrays.toString(this.value6), Arrays.toString(this.value7));
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(this.value0);
		result = prime * result + Arrays.hashCode(this.value1);
		result = prime * result + Arrays.hashCode(this.value2);
		result = prime * result + Arrays.hashCode(this.value3);
		result = prime * result + Arrays.hashCode(this.value4);
		result = prime * result + Arrays.hashCode(this.value5);
		result = prime * result + Arrays.hashCode(this.value6);
		result = prime * result + Arrays.hashCode(this.value7);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals ( Object obj ) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PrimArrays other = (PrimArrays)obj;
		if (!Arrays.equals(this.value0, other.value0)) {
			return false;
		}
		if (!Arrays.equals(this.value1, other.value1)) {
			return false;
		}
		if (!Arrays.equals(this.value2, other.value2)) {
			return false;
		}
		if (!Arrays.equals(this.value3, other.value3)) {
			return false;
		}
		if (!Arrays.equals(this.value4, other.value4)) {
			return false;
		}
		if (!Arrays.equals(this.value5, other.value5)) {
			return false;
		}
		if (!Arrays.equals(this.value6, other.value6)) {
			return false;
		}
		if (!Arrays.equals(this.value7, other.value7)) {
			return false;
		}
		return true;
	}
}
