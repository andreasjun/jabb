// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * Created: 26 Aug 2015, 8:46:11 pm
 * 
 * @author Andreas Junius
 */
@ObjectID ( 23 )
public class ObjCollections {

	private List<String>	value0	= null;

	private Set<String>		value1	= null;

	/*
	 * Queue implementations generally do not define element-based versions of methods equals and hashCode but instead
	 * inherit the identity based versions from class Object, because element-based equality is not always well-defined
	 * for queues with the same elements but different ordering properties.
	 */
	private Queue<String>	value2	= null;

	/**
	 * @return the value0
	 */
	@MemberID ( 0 )
	public List<String> getValue0 () {
		return this.value0;
	}

	/**
	 * @param value0
	 *            the value0 to set
	 */
	@Generic ( "List<String>" )
	public void setValue0 ( List<String> value0 ) {
		this.value0 = value0;
	}

	/**
	 * @return the value1
	 */
	public Set<String> getValue1 () {
		return this.value1;
	}

	/**
	 * @param value1
	 *            the value1 to set
	 */
	@MemberID ( 1 )
	@Generic ( "Set<String>" )
	public void setValue1 ( Set<String> value1 ) {
		this.value1 = value1;
	}

	/**
	 * @return the value2
	 */
	@Generic ( "Queue<String>" )
	public Queue<String> getValue2 () {
		return this.value2;
	}

	/**
	 * @param value2
	 *            the value2 to set
	 */
	@MemberID ( 2 )
	public void setValue2 ( Queue<String> value2 ) {
		this.value2 = value2;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return String.format("ObjCollections [value0=%s, value1=%s, value2=%s]", this.value0, this.value1, this.value2);
	}
}
