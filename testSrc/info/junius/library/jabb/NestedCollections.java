// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.library.jabb;

import java.util.List;
import java.util.Set;

/**
 * Created: 14 Feb 2016, 11:46:25 am
 * 
 * @author Andreas Junius
 */
@ObjectID ( 120 )
public class NestedCollections {

	/** list containing other list */
	private List<List<String>>		value0	= null;

	/** list holding a list of lists */
	private List<List<Set<String>>>	value1	= null;

	/**
	 * @return the value0
	 */
	public List<List<String>> getValue0 () {
		return this.value0;
	}

	/**
	 * @param value0 the value0 to set
	 */
	@MemberID ( 0 )
	@Generic ( "List<List<String>>" )
	public void setValue0 ( List<List<String>> value0 ) {
		this.value0 = value0;
	}

	/**
	 * @return the value1
	 */
	public List<List<Set<String>>> getValue1 () {
		return this.value1;
	}

	/**
	 * @param value1 the value1 to set
	 */
	@MemberID ( 1 )
	@Generic ( "List<List<Set<String>>>" )
	public void setValue1 ( List<List<Set<String>>> value1 ) {
		this.value1 = value1;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( (this.value0 == null) ? 0 : this.value0.hashCode());
		result = prime * result + ( (this.value1 == null) ? 0 : this.value1.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals ( Object obj ) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (! (obj instanceof NestedCollections)) {
			return false;
		}
		NestedCollections other = (NestedCollections)obj;
		if (this.value0 == null) {
			if (other.value0 != null) {
				return false;
			}
		} else if (!this.value0.equals(other.value0)) {
			return false;
		}
		if (this.value1 == null) {
			if (other.value1 != null) {
				return false;
			}
		} else if (!this.value1.equals(other.value1)) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString () {
		return String.format("NestedCollections [value0=%s, value1=%s]", this.value0, this.value1);
	}
}