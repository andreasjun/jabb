// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.jabb.example.basictypes;

import java.net.MalformedURLException;
import java.util.Arrays;
import info.junius.library.jabb.JabbFactory;
import info.junius.library.jabb.ObjectDecoder;
import info.junius.library.jabb.ObjectEncoder;
import info.junius.library.jabb.ParseException;

/**
 * Example 2: Only more complex objects need to be JABB objects. Basic types such as String, Boolean or Integer can be
 * encoded without using annotations. This applies to primitive types and arrays of primitive types as well. The
 * objectID
 * will always be 0 in these cases.
 */
public class RunExample {

	public static void main ( String[] args ) throws ParseException, MalformedURLException {
		// get a factory
		JabbFactory factory = new JabbFactory();
		// get an encoder
		ObjectEncoder encoder = factory.getEncoder();
		// get a decoder
		ObjectDecoder decoder = factory.getDecoder();
		// ----------------------------------------------
		// STRING
		String hello = "Hallo Welt!";
		// encode
		byte[] bytes = encoder.encode(hello);
		// decode
		String hello2 = decoder.decode(bytes, String.class);
		// use results
		System.out.println("STRING");
		System.out.println("bytes " + Arrays.toString(bytes));
		System.out.println("original value " + hello);
		System.out.println("Byte representation, length " + bytes.length + ", content " + Arrays.toString(bytes));
		System.out.println("deserialised value " + hello2);
		System.out.println("equals: " + hello.equals(hello2));
		// ----------------------------------------------
		// Integer
		bytes = encoder.encode(Integer.valueOf(42));
		Integer value2 = decoder.decode(bytes, Integer.class);
		System.out.println("INTEGER");
		System.out.println("bytes " + Arrays.toString(bytes));
		System.out.println("original value " + Integer.valueOf(42));
		System.out.println("Byte representation, length " + bytes.length + ", content " + Arrays.toString(bytes));
		System.out.println("deserialised value " + value2);
		System.out.println("equals: " + Integer.valueOf(42).equals(value2));
		// ----------------------------------------------
		// int
		bytes = encoder.encode(42);
		int value3 = decoder.decode(bytes, Integer.class);
		System.out.println("int");
		System.out.println("bytes " + Arrays.toString(bytes));
		System.out.println("original value " + 42);
		System.out.println("Byte representation, length " + bytes.length + ", content " + Arrays.toString(bytes));
		System.out.println("deserialised value " + value3);
		System.out.println("equals: " + (42 == value3));
		// ----------------------------------------------
		// int[]
		int[] myArray = new int[] { 454, 6575, -4857 };
		bytes = encoder.encode(myArray);
		int[] value4 = decoder.decode(bytes, int[].class);
		System.out.println("int[]");
		System.out.println("bytes " + Arrays.toString(bytes));
		System.out.println("original value " + Arrays.toString(myArray));
		System.out.println("Byte representation, length " + bytes.length + ", content " + Arrays.toString(bytes));
		System.out.println("deserialised value " + Arrays.toString(value4));
		System.out.println("equals: " + Arrays.equals(value4, myArray));
	}
}