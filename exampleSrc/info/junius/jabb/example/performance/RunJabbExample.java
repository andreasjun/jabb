// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.jabb.example.performance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import info.junius.library.jabb.JabbFactory;
import info.junius.library.jabb.ObjectDecoder;
import info.junius.library.jabb.ObjectEncoder;
import info.junius.library.jabb.ParseException;

/**
 * Example 8: uses the bookstore example to get an idea of JABB's performance compared to JAXB
 * Encoding using JABB is a bit slower on my machine, decoding slightly faster than using JAXB
 * The serialized data is a lot less than with JAXB
 */
public class RunJabbExample {

	public static void main ( String[] args ) throws ParseException {
		// get a factory
		JabbFactory factory = new JabbFactory();
		// get an encoder
		ObjectEncoder encoder = factory.getEncoder();
		// get a decoder
		ObjectDecoder decoder = factory.getDecoder();
		// ----------------------------------------------
		// create the bookstore and books
		Book book1 = new Book();
		book1.setAuthor("Joshua Bloch");
		book1.setName("Effective Java");
		book1.setIsbn("978-0321356680");
		book1.setPublisher("Addison-Wesley");
		Book book2 = new Book();
		book2.setAuthor("Raoul-Gabriel Urma");
		book2.setName("Java 8 in Action: Lambdas, Streams, and Functional-Style Programming");
		book2.setIsbn("978-1617291999");
		book2.setPublisher("Manning Pubn");
		Book book3 = new Book();
		book3.setAuthor("Herbert Schildt");
		book3.setName("Java - The Complete Reference");
		book3.setIsbn("978-0071808552");
		book3.setPublisher("Mcgraw-Hill Education Ltd");
		List<Book> bookList = new ArrayList<>();
		bookList.add(book1);
		bookList.add(book2);
		bookList.add(book3);
		Bookstore bookstore = new Bookstore();
		bookstore.setName("Hugendubel");
		bookstore.setLocation("Muenchen");
		bookstore.setBookList(bookList);
		long encoding = 0;
		long decoding = 0;
		// encode
		long startTime = System.currentTimeMillis();
		byte[] bytes = encoder.encode(bookstore);
		long finished = System.currentTimeMillis();
		encoding = finished - startTime;
		// decode
		startTime = System.currentTimeMillis();
		Bookstore bookstore2 = decoder.decode(bytes, Bookstore.class);
		finished = System.currentTimeMillis();
		decoding = finished - startTime;
		// print results
		System.out.println("bytes " + Arrays.toString(bytes));
		System.out.println("original value     " + bookstore);
		System.out.println("deserialised value " + bookstore2);
		System.out.println("runtime encoding " + encoding + " millseconds");
		System.out.println("runtime decoding " + decoding + " millseconds");
		System.out.println("number of bytes  " + bytes.length);
	}
}