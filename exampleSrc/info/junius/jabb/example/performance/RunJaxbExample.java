// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.jabb.example.performance;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * Example 8: uses the bookstore example to get an idea of JAXB's performance compared to JABB
 */
public class RunJaxbExample {

	public static void main ( String[] args ) throws JAXBException, IOException {
		// initialise
		JAXBContext context = JAXBContext.newInstance(Bookstore.class);
		// create the bookstore and books
		Book book1 = new Book();
		book1.setAuthor("Joshua Bloch");
		book1.setName("Effective Java");
		book1.setIsbn("978-0321356680");
		book1.setPublisher("Addison-Wesley");
		Book book2 = new Book();
		book2.setAuthor("Raoul-Gabriel Urma");
		book2.setName("Java 8 in Action: Lambdas, Streams, and Functional-Style Programming");
		book2.setIsbn("978-1617291999");
		book2.setPublisher("Manning Pubn");
		Book book3 = new Book();
		book3.setAuthor("Herbert Schildt");
		book3.setName("Java - The Complete Reference");
		book3.setIsbn("978-0071808552");
		book3.setPublisher("Mcgraw-Hill Education Ltd");
		List<Book> bookList = new ArrayList<>();
		bookList.add(book1);
		bookList.add(book2);
		bookList.add(book3);
		Bookstore bookstore = new Bookstore();
		bookstore.setName("Hugendubel");
		bookstore.setLocation("Muenchen");
		bookstore.setBookList(bookList);
		long encoding = 0;
		long decoding = 0;
		// encode
		long startTime = System.currentTimeMillis();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Writer writer = new OutputStreamWriter(baos, StandardCharsets.UTF_8);
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_ENCODING, StandardCharsets.UTF_8.toString());
		marshaller.marshal(bookstore, writer);
		writer.flush();
		writer.close();
		String xml = baos.toString(StandardCharsets.UTF_8.toString());
		long finished = System.currentTimeMillis();
		encoding = finished - startTime;
		// decode
		startTime = System.currentTimeMillis();
		InputStream inputStream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
		Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		Bookstore bookstore2 = (Bookstore)unmarshaller.unmarshal(reader);
		reader.close();
		finished = System.currentTimeMillis();
		decoding = finished - startTime;
		// print results
		System.out.println("xml " + xml);
		System.out.println("original value     " + bookstore);
		System.out.println("deserialised value " + bookstore2);
		System.out.println("runtime encoding " + encoding + " millseconds");
		System.out.println("runtime decoding " + decoding + " millseconds");
		System.out.println("number of bytes  " + xml.getBytes(StandardCharsets.UTF_8).length);
	}
}