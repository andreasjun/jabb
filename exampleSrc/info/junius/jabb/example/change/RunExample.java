// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.jabb.example.change;

import info.junius.library.jabb.JabbFactory;
import info.junius.library.jabb.ObjectDecoder;
import info.junius.library.jabb.ObjectEncoder;
import info.junius.library.jabb.ParseException;

/**
 * Example 8: deserialising into an updated object from a stream created with a previous version.
 */
public class RunExample {

	public static void main ( String[] args ) throws ParseException {
		// get a factory
		JabbFactory factory = new JabbFactory();
		// get an encoder
		ObjectEncoder encoder = factory.getEncoder();
		// get a decoder
		ObjectDecoder decoder = factory.getDecoder();
		// create a JABB object
		Address address = new Address();
		address.setState("SA");
		address.setCity("Adelaide");
		address.setZipCode("5000");
		address.setStreet(null);
		address.setNumber(0);
		// encode
		byte[] bytes = encoder.encode(address);
		// decode to the changed type
		ChangedAddress address2 = decoder.decode(bytes, ChangedAddress.class);
		System.out.println("Input: " + address);
		System.out.println("Output:" + address2);
		// prints:
		// Input: Address [city=Adelaide, zipCode=5000, state=SA, street=null, number=0]
		// Output:ChangedAddress [city=Adelaide, zipCode=5000, street=null, number=0, phone=null]
		// the value for state in object 1 got discarded
		// the value for phone in object 2 has been set to its default value
	}
}