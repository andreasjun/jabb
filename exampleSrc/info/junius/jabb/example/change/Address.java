// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.jabb.example.change;

import info.junius.library.jabb.MemberID;
import info.junius.library.jabb.ObjectID;

/**
 * JABB type Address
 */
@ObjectID ( 123 )
public class Address {

	private String	city	= null;

	private String	zipCode	= null;

	private String	state	= null;

	private String	street	= null;

	private int		number	= 0;

	public String getCity () {
		return this.city;
	}

	@MemberID ( 1 )
	public void setCity ( String city ) {
		this.city = city;
	}

	public String getZipCode () {
		return this.zipCode;
	}

	@MemberID ( 2 )
	public void setZipCode ( String zipCode ) {
		this.zipCode = zipCode;
	}

	public String getState () {
		return this.state;
	}

	@MemberID ( 3 )
	public void setState ( String state ) {
		this.state = state;
	}

	public String getStreet () {
		return this.street;
	}

	@MemberID ( 4 )
	public void setStreet ( String street ) {
		this.street = street;
	}

	public int getNumber () {
		return this.number;
	}

	@MemberID ( 5 )
	public void setNumber ( int number ) {
		this.number = number;
	}

	@Override
	public String toString () {
		return String.format("Address [city=%s, zipCode=%s, state=%s, street=%s, number=%s]", this.city, this.zipCode, this.state, this.street, this.number);
	}
}