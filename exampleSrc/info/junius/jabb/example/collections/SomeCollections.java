// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.jabb.example.collections;

import java.util.Collection;
import java.util.Map;
import info.junius.library.jabb.Generic;
import info.junius.library.jabb.MemberID;
import info.junius.library.jabb.ObjectID;

/**
 * A JABB object that contains collections.
 */
@ObjectID ( 15 )   // the ID will be the first byte of the serialized object
public class SomeCollections {

	private Collection<String>	listOfStrings	= null;

	private Map<Long, String>	mapOfStrings	= null;

	public Collection<String> getListOfStrings () {
		return this.listOfStrings;
	}

	@MemberID ( 1 ) // member ID to identify a field
	@Generic ( "Collection<String>" )   // maintain generic information
	public void setListOfStrings ( Collection<String> listOfStrings ) {
		this.listOfStrings = listOfStrings;
	}

	public Map<Long, String> getMapOfStrings () {
		return this.mapOfStrings;
	}

	@MemberID ( 2 )
	@Generic ( "Map<Long, String>" )   // fully qualified names are required for custom objects, i.e. everything that's
									   // not in java.util or java.lang
	public void setMapOfStrings ( Map<Long, String> mapOfStrings ) {
		this.mapOfStrings = mapOfStrings;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return String.format("SomeCollections [listOfStrings=%s, mapOfStrings=%s]", this.listOfStrings, this.mapOfStrings);
	}
}