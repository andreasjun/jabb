// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.jabb.example.collections;

import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import info.junius.library.jabb.JabbFactory;
import info.junius.library.jabb.ObjectDecoder;
import info.junius.library.jabb.ObjectEncoder;
import info.junius.library.jabb.ParseException;

/**
 * Example 4: Collections are normally used with generics. The generic information gets lost at runtime and need to be
 * retained
 * using an annotation. See: <code>info.junius.jabb.example.collections.SomeCollections</code>
 */
public class RunExample {

	public static void main ( String[] args ) throws ParseException, MalformedURLException {
		// get a factory
		JabbFactory factory = new JabbFactory();
		// get an encoder
		ObjectEncoder encoder = factory.getEncoder();
		// get a decoder
		ObjectDecoder decoder = factory.getDecoder();
		// ----------------------------------------------
		// STRING
		SomeCollections myCollections = new SomeCollections();
		Collection<String> myCollection = Arrays.asList(new String[] { "one", "two", "three" });
		Map<Long, String> myMap = new HashMap<>();
		myMap.put(1L, "one");
		myMap.put(2L, "two");
		myMap.put(3L, "three");
		myCollections.setListOfStrings(myCollection);
		myCollections.setMapOfStrings(myMap);
		// encode
		byte[] bytes = encoder.encode(myCollections);
		// decode
		SomeCollections myCollections2 = decoder.decode(bytes, SomeCollections.class);
		// print results
		System.out.println("bytes " + Arrays.toString(bytes));
		System.out.println("original value     " + myCollections);
		System.out.println("deserialised value " + myCollections2);
	}
}