// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.jabb.example.getstarted;

import info.junius.library.jabb.MemberID;
import info.junius.library.jabb.ObjectID;

@ObjectID ( 12 )   // the ID will be the first byte of the serialized object
public class Address {

	private int		number		= 0;

	private String	street		= null;

	private String	town		= null;

	private int		postcode	= 0;

	public int getNumber () {
		return this.number;
	}

	@MemberID ( 1 )    // the member ID helps the framework to map bytes to attributes
	public void setNumber ( int number ) {
		this.number = number;
	}

	@MemberID ( 2 )    // this annotation can be on either getter or setter
	public String getStreet () {
		return this.street;
	}

	public void setStreet ( String street ) {
		this.street = street;
	}

	public String getTown () {
		return this.town;
	}

	@MemberID ( 3 )    // the member ID has to be unique within a class
	public void setTown ( String town ) {
		this.town = town;
	}

	public int getPostcode () {
		return this.postcode;
	}

	// methods without @MemberID annotation won't be serialized
	public void setPostcode ( int postcode ) {
		this.postcode = postcode;
	}

	@Override
	public String toString () {
		return String.format("Address [number=%s, street=%s, town=%s, postcode=%s]", this.number, this.street, this.town, this.postcode);
	}
}