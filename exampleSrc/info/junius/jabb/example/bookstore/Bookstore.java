// @formatter:off
/******************************************************************************************
 *                             JABB - Java Binary Binding                                 *
 * Copyright (c) 2015, 2016, Andreas Junius, andreas@junius.info, www.javanerd.com.au     *
 *                                All rights reserved.                                    *
 *                                                                                        *
 * Redistribution and use in source and binary forms, with or without modification, are   *
 * permitted provided that the following conditions are met:                              *
 *                                                                                        * 
 * 1. Redistributions of source code must retain the above copyright notice, this list    *
 * of conditions and the following disclaimer.                                            *
 *                                                                                        * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list *
 * of conditions and the following disclaimer in the documentation and/or other materials *
 * provided with the distribution.                                                        *
 *                                                                                        * 
 * 3. Neither the name of the copyright holder nor the names of its contributors may be   *
 * used to endorse or promote products derived from this software without specific prior  *
 * written permission.                                                                    *
 *                                                                                        *  
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY    *
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES   * 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT    *
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,         *
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR     *
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN       *
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN     *
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF         *
 * SUCH DAMAGE.                                                                           *
*******************************************************************************************/
// @formatter:on
package info.junius.jabb.example.bookstore;

import java.util.List;
import info.junius.library.jabb.Generic;
import info.junius.library.jabb.MemberID;
import info.junius.library.jabb.ObjectID;

@ObjectID ( -128 )
public class Bookstore {

	private List<Book>	bookList	= null;

	private String		name		= null;

	private String		location	= null;

	public List<Book> getBookList () {
		return this.bookList;
	}

	@MemberID ( 1 )
	@Generic ( "List<info.junius.jabb.example.bookstore.Book>" )  // custom class, use fully qualified name for the
																  // generic information
	public void setBookList ( List<Book> bookList ) {
		this.bookList = bookList;
	}

	public String getName () {
		return this.name;
	}

	@MemberID ( 2 )
	public void setName ( String name ) {
		this.name = name;
	}

	public String getLocation () {
		return this.location;
	}

	@MemberID ( 3 )
	public void setLocation ( String location ) {
		this.location = location;
	}

	@Override
	public String toString () {
		return String.format("Bookstore [bookList=%s, name=%s, location=%s]", this.bookList, this.name, this.location);
	}
}