Java Architecture for Binary Binding (JABB)

The JABB library converts Java objects to a binary representation and vice versa based on annotations, a.k.a serialization. It is a mechanism very similar to those that serialize from and to XML or JSON, and it is supposed to be much simpler than ASN.1 and one of the encodings associated with it (BER, DER, etc.). The process gets guided by annotations that uniquely identify each object and each of its members. This allows to change objects later on and still be able to read from bytes streams that got created with an earlier version. Its name is "Java Architecture for Binary Binding" (JABB) and the main package is info.junius.library.jabb.*

Features:

	-easy to use
	-platform independend
	-fast
	-lightweight (the signed jar is less than 100KB in size)
	-no code generation required
	-creates compact byte streams
	-accepts interfaces

Supported are:

    -primitives
    -objects
    -enums
    -collections
    -maps
    -n-dimensional Arrays
    -nested collections
    -maps containing maps or collections as values
		
The encoding rules are as follows:

	ID/TYPE/LENGTH/VALUE where value can be any number of tuples for nested objects.
	
	ID is an arbitrary, unique identifier set via annotation
	type is a platform independend identifier
    length is the length of the value as a byte array of variable length created using DER encoding rules
	value represents the serialized object
		
Security
	
	Binary streams get deserialized only to known types, using only public methods; other than Java native serialization that serializes every value of an object. Injection of malicious values into private or protected fields is therefore not possible.

How to use

 * Annotate a POJO
 * Get an encoder or a decoder using the JabbFactory
 * encode or decode
  
Example

Annotate a POJO, an Address class:

	import info.junius.library.jabb.MemberID;
	import info.junius.library.jabb.ObjectID;

	@ObjectID ( 123 )	// this id can be used to identify an object by its first byte
	public class Address {

		private String	city	= null;
		private String	zipCode	= null;

		public String getCity () {
			return this.city;
		}

		@MemberID ( 1 )	// this id uniquely identifies a member variable.
		public void setCity ( String city ) {
			this.city = city;
		}

		public String getZipCode () {
			return this.zipCode;
		}

		@MemberID ( 2 )
		public void setZipCode ( String zipCode ) {
			this.zipCode = zipCode;
		}
	}

Get an encoder or a decoder using the JabbFactory:

	// get a factory
	JabbFactory factory = new JabbFactory();
	// get an encoder
	ObjectEncoder encoder = factory.getEncoder();

Encode:
	byte[] binaryData = encoder.encode(myAddress);

And:

	// get a decoder
	ObjectDecoder decoder = factory.getDecoder();
	
Decode:
	Address myAddress = decoder.decode(binaryData, Address.class);	
	
	
Type Encoding Constants
	
	NULL 			 0
	BYTE 			 1
	SHORT 			 2
	INTEGER 		 3
	LONG 			 4
	FLOAT 			 5
	DOUBLE 			 6
	BOOLEAN 		 7
	CHARACTER 		 8
	OBJECT 			 9
	STRING 			10
	DATE 			11
	TIMESTAMP 		12
	ENUM 			14
	BYTE_ARRAY 		21
	SHORT_ARRAY 	22
	INT_ARRAY 		23
	LONG_ARRAY 		24
	FLOAT_ARRAY 	25
	DOUBLE_ARRAY 	26
	BOOLEAN_ARRAY 	27
	CHAR_ARRAY 		28
	COLLECTION 		30
	ARRAY 			40
	MAP 			50	
	
More Examples in info.junius.jabb.example.*

Examples
	1 getstarted		a simple example
	2 basictypes		converting String, Integer and other fundamental types
	3 stringtypes		types that have a String arg constructor and matching toString implementation
	4 collections		how to use collections (generics)
	5 interfaces		how to use annotated interfaces
	6 bookstore			more complex example
	7 performance		comparison JAXB vs. JABB
	8 changing types	how to handle code changes
	
	
	